/**
 * Created by xa on 15-11-20.
 */
'use strict';
function omaControl() {
  debug('testbox - omaControl');
}

omaControl.prototype._init = function() {
  //do nothing
  if(navigator.OmaService) {
    //navigator.OmaService.servicestart();
    debug("testbox: navigator.OmaService is not NULL");
  } else {
    debug("testbox: navigator.OmaService is NULL");
  }
}

omaControl.prototype._uninit = function() {
  //do nothing
}

omaControl.prototype._servicestart = function() {
  if(navigator.OmaService) {
    navigator.OmaService.servicestart();
  }
}

omaControl.prototype._serviceend = function() {
  navigator.OmaService.serviceend();
}

omaControl.prototype._postevent = function() {
  var sessiontype = $('oma-Control-input').value;
  dump("testbox: postevent:"+ sessiontype);
  navigator.OmaService.postevent(sessiontype);
}

omaControl.prototype._startOma = function(){
  console.log("testbox: startOma");
  try{
    var array=["1", "6", "7", "0xc4", "0xb4", "0x84", "0x8d", "0x9e", "0xaf", "0x87",
      "0xcb", "0x92", "0xcf", "0x1f", "0xc5", "0x8d", "0x93", "0x55", "0x89", "0xa8",
      "0x3c", "0xef", "0x94", "0x12", "0xd4", "0xcd", "0x02", "0xd8", "0", "0", "0", "0",
      "0x4a", "0x06", "0x6d", "0x61", "0x73", "0x74", "0x65", "0x72"];
    navigator.OmaService.receiveWapPush(array, array.length, 10, {});
  }catch(e){
    console.log(e);
  }
}

omaControl.prototype.handleEvent = function(evt) {
  dump('testbox-------evt.key: ' + evt.key);
  if(evt.key) {
    switch(evt.key) {
      case 'Enter':
      case 'BrowserForward':
        switch(evt.target.id){
          case 'oma-Control-startsvr':
            omaControl.prototype._servicestart();
            break;
          case 'oma-Control-endsvr':
            omaControl.prototype._serviceend();
            break;
          case 'eventText':
            break;
          case 'oma-Control-postEvt':
            omaControl.prototype._postevent();
            break;
          case 'oma-Control-startoma':
            omaControl.prototype._startOma();
        }
        break;
    }
  }
}

window.addEventListener('load', omaControl.prototype._init.bind(omaControl));
window.addEventListener('keydown', omaControl.prototype.handleEvent.bind(omaControl));
