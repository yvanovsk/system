
/** @license MIT License (c) copyright 2010-2013 B Cavalier & J Hann */

/**
 * curl (cujo resource loader)
 * An AMD-compliant javascript module and resource loader
 *
 * curl is part of the cujo.js family of libraries (http://cujojs.com/)
 *
 * Licensed under the MIT License at:
 * 		http://www.opensource.org/licenses/mit-license.php
 *
 */
(function (global) {
// don't restore this until the config routine is refactored
	var
		version = '0.8.11',
		curlName = 'curl',
		defineName = 'define',
		bootScriptAttr = 'data-curl-run',
		bootScript,
		userCfg,
		prevCurl,
		prevDefine,
		doc = global.document,
		head = doc && (doc['head'] || doc.getElementsByTagName('head')[0]),
		// to keep IE from crying, we need to put scripts before any
		// <base> elements, but after any <meta>. this should do it:
		insertBeforeEl = head && head.getElementsByTagName('base')[0] || null,
		// constants / flags
		msgUsingExports = {},
		msgFactoryExecuted = {},
		// this is the list of scripts that IE is loading. one of these will
		// be the "interactive" script. too bad IE doesn't send a readystatechange
		// event to tell us exactly which one.
		activeScripts = {},
		// readyStates for IE6-9
		readyStates = 'addEventListener' in global ? {} : { 'loaded': 1, 'complete': 1 },
		// these are always handy :)
		cleanPrototype = {},
		toString = cleanPrototype.toString,
		undef,
		// local cache of resource definitions (lightweight promises)
		cache = {},
		// local url cache
		urlCache = {},
		// preload are files that must be loaded before any others
		preload = false,
		// net to catch anonymous define calls' arguments (non-IE browsers)
		argsNet,
		// RegExp's used later, pre-compiled here
		dontAddExtRx = /\?|\.js\b/,
		absUrlRx = /^\/|^[^:]+:\/\/|^[A-Za-z]:[\\/]/,
		findDotsRx = /(\.)(\.?)(?:$|\/([^\.\/]+.*)?)/g,
		removeCommentsRx = /\/\*[\s\S]*?\*\/|\/\/.*?[\n\r]/g,
		findRValueRequiresRx = /require\s*\(\s*(["'])(.*?[^\\])\1\s*\)|[^\\]?(["'])/g,
		splitCommaSepRx = /\s*,\s*/,
		cjsGetters,
		core;

	function noop () {}

	function isType (obj, type) {
		return toString.call(obj).indexOf('[object ' + type) == 0;
	}

	function normalizePkgDescriptor (descriptor, isPkg) {
		var main;

		descriptor.path = removeEndSlash(descriptor['path'] || descriptor['location'] || '');
		if (isPkg) {
			main = descriptor['main'] || './main';
			if (!isRelUrl(main)) main = './' + main;
			// trailing slashes trick reduceLeadingDots to see them as base ids
			descriptor.main = reduceLeadingDots(main, descriptor.name + '/');
		}
		descriptor.config = descriptor['config'];

		return descriptor;
	}

	function isRelUrl (it) {
		return it.charAt(0) == '.';
	}

	function isAbsUrl (it) {
		return absUrlRx.test(it);
	}

	function joinPath (path, file) {
		return removeEndSlash(path) + '/' + file;
	}

	function removeEndSlash (path) {
		return path && path.charAt(path.length - 1) == '/' ? path.substr(0, path.length - 1) : path;
	}

	function reduceLeadingDots (childId, baseId) {
		// this algorithm is similar to dojo's compactPath, which interprets
		// module ids of "." and ".." as meaning "grab the module whose name is
		// the same as my folder or parent folder".  These special module ids
		// are not included in the AMD spec but seem to work in node.js, too.
		var removeLevels, normId, levels, isRelative, diff;

		removeLevels = 1;
		normId = childId;

		// remove leading dots and count levels
		if (isRelUrl(normId)) {
			isRelative = true;
			normId = normId.replace(findDotsRx, function (m, dot, doubleDot, remainder) {
				if (doubleDot) removeLevels++;
				return remainder || '';
			});
		}

		if (isRelative) {
			levels = baseId.split('/');
			diff = levels.length - removeLevels;
			if (diff < 0) {
				// this is an attempt to navigate above parent module.
				// maybe dev wants a url or something. punt and return url;
				return childId;
			}
			levels.splice(diff, removeLevels);
			// normId || [] prevents concat from adding extra "/" when
			// normId is reduced to a blank string
			return levels.concat(normId || []).join('/');
		}
		else {
			return normId;
		}
	}

	function pluginParts (id) {
		var delPos = id.indexOf('!');
		return {
			resourceId: id.substr(delPos + 1),
			// resourceId can be zero length
			pluginId: delPos >= 0 && id.substr(0, delPos)
		};
	}

	function Begetter () {}

	function beget (parent, mixin) {
		Begetter.prototype = parent || cleanPrototype;
		var child = new Begetter();
		Begetter.prototype = cleanPrototype;
		for (var p in mixin) child[p] = mixin[p];
		return child;
	}

	function Promise () {

		var self, thens, complete;

		self = this;
		thens = [];

		function then (resolved, rejected, progressed) {
			// capture calls to callbacks
			thens.push([resolved, rejected, progressed]);
		}

		function notify (which, arg) {
			// complete all callbacks
			var aThen, cb, i = 0;
			while ((aThen = thens[i++])) {
				cb = aThen[which];
				if (cb) cb(arg);
			}
		}

		complete = function promiseComplete (success, arg) {
			// switch over to sync then()
			then = success ?
				function (resolved, rejected) { resolved && resolved(arg); } :
				function (resolved, rejected) { rejected && rejected(arg); };
			// we no longer throw during multiple calls to resolve or reject
			// since we don't really provide useful information anyways.
			complete = noop;
			// complete all callbacks
			notify(success ? 0 : 1, arg);
			// no more notifications
			notify = noop;
			// release memory
			thens = undef;
		};

		this.then = function (resolved, rejected, progressed) {
			then(resolved, rejected, progressed);
			return self;
		};
		this.resolve = function (val) {
			self.resolved = val;
			complete(true, val);
		};
		this.reject = function (ex) {
			self.rejected = ex;
			complete(false, ex);
		};
		this.progress = function (msg) {
			notify(2, msg);
		}

	}

	function isPromise (o) {
		return o instanceof Promise || o instanceof CurlApi;
	}

	function when (promiseOrValue, callback, errback, progback) {
		// we can't just sniff for then(). if we do, resources that have a
		// then() method will make dependencies wait!
		if (isPromise(promiseOrValue)) {
			return promiseOrValue.then(callback, errback, progback);
		}
		else {
			return callback(promiseOrValue);
		}
	}

	/**
	 * Returns a function that when executed, executes a lambda function,
	 * but only executes it the number of times stated by howMany.
	 * When done executing, it executes the completed function. Each callback
	 * function receives the same parameters that are supplied to the
	 * returned function each time it executes.  In other words, they
	 * are passed through.
	 * @private
	 * @param howMany {Number} must be greater than zero
	 * @param lambda {Function} executed each time
	 * @param completed {Function} only executes once when the counter
	 *   reaches zero
	 * @returns {Function}
	 */
	function countdown (howMany, lambda, completed) {
		var result;
		return function () {
			if (--howMany >= 0 && lambda) result = lambda.apply(undef, arguments);
			// we want ==, not <=, since some callers expect call-once functionality
			if (howMany == 0 && completed) completed(result);
			return result;
		}
	}

	core = {

		/**
		 * * reduceLeadingDots of id against parentId
		 *		- if there are too many dots (path goes beyond parent), it's a url
		 *			- return reduceLeadingDots of id against baseUrl + parentId;
		 *	* if id is a url (starts with dots or slash or protocol)
		 *		- pathInfo = { config: userCfg, url: url }
		 *	* if not a url, id-to-id transform here.
		 *		- main module expansion
		 *		- plugin prefix expansion
		 *		- coordinate main module expansion with plugin expansion
		 *			- main module expansion happens first
		 *		- future: other transforms?
		 * @param id
		 * @param parentId
		 * @param cfg
		 * @return {*}
		 */
		toAbsId: function (id, parentId, cfg) {
			var absId, pluginId, parts;

			absId = reduceLeadingDots(id, parentId);

			// if this is still a relative path, it must be a url
			// so just punt, otherwise...
			if (isRelUrl(absId)) return absId;

			// plugin id split
			parts = pluginParts(absId);
			pluginId = parts.pluginId;
			absId = pluginId || parts.resourceId;

			// main id expansion
			if (absId in cfg.pathMap) {
				absId = cfg.pathMap[absId].main || absId;
			}

			// plugin id expansion
			if (pluginId) {
				if (pluginId.indexOf('/') < 0 && !(pluginId in cfg.pathMap)) {
					absId = joinPath(cfg.pluginPath, pluginId);
				}
				absId = absId + '!' + parts.resourceId;
			}

			return absId;
		},

		createContext: function (cfg, baseId, depNames, isPreload) {

			var def;

			def = new Promise();
			def.id = baseId || ''; // '' == global
			def.isPreload = isPreload;
			def.depNames = depNames;
			def.config = cfg;

			// functions that dependencies will use:

			function toAbsId (childId, checkPlugins) {
				var absId, parts, plugin;

				absId = core.toAbsId(childId, def.id, cfg);
				if (!checkPlugins) return absId;

				parts = pluginParts(absId);
				if (!parts.pluginId) return absId;

				plugin = cache[parts.pluginId];
				// check if plugin supports the normalize method
				if ('normalize' in plugin) {
					// note: dojo/has may return falsey values (0, actually)
					parts.resourceId = plugin['normalize'](parts.resourceId, toAbsId, def.config) || '';
				}
				else {
					parts.resourceId = toAbsId(parts.resourceId);
				}
				return parts.pluginId + '!' + parts.resourceId;
			}

			function toUrl (n) {
				// the AMD spec states that we should not append an extension
				// in this function since it could already be appended.
				// we need to use toAbsId in case this is a module id.
				return core.resolvePathInfo(toAbsId(n, true), cfg).url;
			}

			function localRequire (ids, callback, errback) {
				var cb, rvid, childDef, earlyExport;

				// this is public, so send pure function
				// also fixes issue #41
				cb = callback && function () { callback.apply(undef, arguments[0]); };

				// RValue require (CommonJS)
				if (isType(ids, 'String')) {
					if (cb) {
						throw new Error('require(id, callback) not allowed');
					}
					// return resource
					rvid = toAbsId(ids, true);
					childDef = cache[rvid];
					if (!(rvid in cache)) {
						// this should only happen when devs attempt their own
						// manual wrapping of cjs modules or get confused with
						// the callback syntax:
						throw new Error('Module not resolved: '  + rvid);
					}
					earlyExport = isPromise(childDef) && childDef.exports;
					return earlyExport || childDef;
				}
				else {
					when(core.getDeps(core.createContext(cfg, def.id, ids, isPreload)), cb, errback);
				}
			}

			def.require = localRequire;
			localRequire['toUrl'] = toUrl;
			def.toAbsId = toAbsId;

			return def;
		},

		createResourceDef: function (cfg, id, isPreload) {
			var def, origResolve, execute;

			def = core.createContext(cfg, id, undef, isPreload);
			origResolve = def.resolve;

			// using countdown to only execute definition function once
			execute = countdown(1, function (deps) {
				def.deps = deps;
				try {
					return core.executeDefFunc(def);
				}
				catch (ex) {
					def.reject(ex);
				}
			});

			// intercept resolve function to execute definition function
			// before resolving
			def.resolve = function resolve (deps) {
				when(isPreload || preload, function () {
					origResolve((cache[def.id] = urlCache[def.url] = execute(deps)));
				});
			};

			// track exports
			def.exportsReady = function executeFactory (deps) {
				when(isPreload || preload, function () {
					// only resolve early if we also use exports (to avoid
					// circular dependencies). def.exports will have already
					// been set by the getDeps loop before we get here.
					if (def.exports) {
						execute(deps);
						def.progress(msgFactoryExecuted);
					}
				});
			};

			return def;
		},

		createPluginDef: function (cfg, id, resId, isPreload) {
			var def;

			// use resource id for local require and toAbsId
			def = core.createContext(cfg, resId, undef, isPreload);

			return def;
		},

		getCjsRequire: function (def) {
			return def.require;
		},

		getCjsExports: function (def) {
			return def.exports || (def.exports = {});
		},

		getCjsModule: function (def) {
			var module = def.module;
			if (!module) {
				module = def.module = {
					'id': def.id,
					'uri': core.getDefUrl(def),
					'exports': core.getCjsExports(def),
					'config': function () { return def.config; }
				};
				module.exports = module['exports']; // oh closure compiler!
			}
			return module;
		},

		getDefUrl: function (def) {
			// note: this is used by cjs module.uri
			return def.url || (def.url = core.checkToAddJsExt(def.require['toUrl'](def.id), def.config));
		},

		/**
		 * Sets the curl() and define() APIs.
		 * @param [cfg] {Object|Null} set of config params. If missing or null,
		 *   this function will set the default API!
		 */
		setApi: function (cfg) {
			/*
			scenarios:
			1. global config sets apiName: "require"
				- first call to config sets api
				- second and later calls are ignored
				- prevCurl cannot exist
			2. no global config, first call to config() sets api
				- first call to config has no api info
				- second call to config sets api
				- third and later calls must be ignored
			3. global config that doesn't set api, first call does though
				- same as #2
			4. api info is never set
				- how to know when to stop ignoring?

			objectives:
			1. fail before mistakenly overwriting global[curlName]
			2. allow rename/relocate of curl() and define()
			3. restore curl() if we overwrote it
			 */

			var apiName, defName, apiObj, defObj,
				failMsg, okToOverwrite;

			apiName = curlName;
			defName = defineName;
			apiObj = defObj = global;
			failMsg = ' already exists';

			// if we're not setting defaults
			if (cfg) {
				// is it ok to overwrite existing api functions?
				okToOverwrite = cfg['overwriteApi'] || cfg.overwriteApi;
				// allow dev to rename/relocate curl() to another object
				apiName = cfg['apiName'] || cfg.apiName || apiName;
				apiObj = cfg['apiContext'] || cfg.apiContext || apiObj;
				// define() too
				defName = cfg['defineName'] || cfg.defineName || defName;
				defObj = cfg['defineContext'] || cfg.defineContext || defObj;

				// curl() already existed, restore it if this is not a
				// setDefaults pass. dev must be a good citizen and set
				// apiName/apiContext (see below).
				if (prevCurl && isType(prevCurl, 'Function')) {
					// restore previous curl()
					global[curlName] = prevCurl;
				}
				prevCurl = null; // don't check ever again
				// ditto for define()
				if (prevDefine && isType(prevDefine, 'Function')) {
					// restore previous curl()
					global[defineName] = prevDefine;
				}
				prevDefine = null; // don't check ever again

				// check if we're mistakenly overwriting either api
				// if we're configuring, and there's a curl(), and it's not
				// ours -- and we're not explicitly overwriting -- throw!
				// Note: if we're setting defaults, we *must* overwrite curl
				// so that dev can configure it.  This is no different than
				// noConflict()-type methods.
				if (!okToOverwrite) {
					if (apiObj[apiName] && apiObj[apiName] != _curl) {
						throw new Error(apiName + failMsg);
					}
					// check if we're overwriting amd api
					if (defObj[defName] && defObj[defName] != define) {
						throw new Error(defName + failMsg);
					}
				}

			}

			// set curl api
			apiObj[apiName] = _curl;

			// set AMD public api: define()
			defObj[defName] = define;

		},

		config: function (cfg) {
			var prevCfg, newCfg, pluginCfgs, p;

			// convert from closure-safe names
			if ('baseUrl' in cfg) cfg.baseUrl = cfg['baseUrl'];
			if ('main' in cfg) cfg.main = cfg['main'];
			if ('preloads' in cfg) cfg.preloads = cfg['preloads'];
			if ('pluginPath' in cfg) cfg.pluginPath = cfg['pluginPath'];
			if ('dontAddFileExt' in cfg || cfg.dontAddFileExt) {
				cfg.dontAddFileExt = new RegExp(cfg['dontAddFileExt'] || cfg.dontAddFileExt);
			}

			prevCfg = userCfg;
			newCfg = beget(prevCfg, cfg);

			// create object to hold path map.
			// each plugin and package will have its own pathMap, too.
			newCfg.pathMap = beget(prevCfg.pathMap);
			pluginCfgs = cfg['plugins'] || {};
			newCfg.plugins = beget(prevCfg.plugins);
			newCfg.paths = beget(prevCfg.paths, cfg.paths);
			newCfg.packages = beget(prevCfg.packages, cfg.packages);

			// temporary arrays of paths. this will be converted to
			// a regexp for fast path parsing.
			newCfg.pathList = [];

			// normalizes path/package info and places info on either
			// the global cfg.pathMap or on a plugin-specific altCfg.pathMap.
			// also populates a pathList on cfg or plugin configs.
			function fixAndPushPaths (coll, isPkg) {
				var id, pluginId, data, parts, currCfg, info;
				for (var name in coll) {
					data = coll[name];
					if (isType(data, 'String')) data = {
						path: coll[name]
					};
					// grab the package id, if specified. default to
					// property name, if missing.
					data.name = data.name || name;
					currCfg = newCfg;
					// check if this is a plugin-specific path
					parts = pluginParts(removeEndSlash(data.name));
					id = parts.resourceId;
					pluginId = parts.pluginId;
					if (pluginId) {
						// plugin-specific path
						currCfg = pluginCfgs[pluginId];
						if (!currCfg) {
							currCfg = pluginCfgs[pluginId] = beget(newCfg);
							currCfg.pathMap = beget(newCfg.pathMap);
							currCfg.pathList = [];
						}
						// remove plugin-specific path from coll
						delete coll[name];
					}
					info = normalizePkgDescriptor(data, isPkg);
					if (info.config) info.config = beget(newCfg, info.config);
					info.specificity = id.split('/').length;
					if (id) {
						currCfg.pathMap[id] = info;
						currCfg.pathList.push(id);
					}
					else {
						// naked plugin name signifies baseUrl for plugin
						// resources. baseUrl could be relative to global
						// baseUrl.
						currCfg.baseUrl = core.resolveUrl(data.path, newCfg);
					}
				}
			}

			// adds the path matching regexp onto the cfg or plugin cfgs.
			function convertPathMatcher (cfg) {
				var pathMap = cfg.pathMap;
				cfg.pathRx = new RegExp('^(' +
					cfg.pathList.sort(function (a, b) { return pathMap[b].specificity - pathMap[a].specificity; } )
						.join('|')
						.replace(/\/|\./g, '\\$&') +
					')(?=\\/|$)'
				);
				delete cfg.pathList;
			}

			// fix all new packages, then paths (in case there are
			// plugin-specific paths for a main module, such as wire!)
			fixAndPushPaths(cfg['packages'], true);
			fixAndPushPaths(cfg['paths'], false);

			// process plugins after packages in case we already perform an
			// id transform on a plugin (i.e. it's a package.main)
			for (p in pluginCfgs) {
				var absId = core.toAbsId(p + '!', '', newCfg);
				newCfg.plugins[absId.substr(0, absId.length - 1)] = pluginCfgs[p];
			}
			pluginCfgs = newCfg.plugins;

			// create search regex for each path map
			for (p in pluginCfgs) {
				// inherit full config
				pluginCfgs[p] = beget(newCfg, pluginCfgs[p]);
				var pathList = pluginCfgs[p].pathList;
				if (pathList) {
					pluginCfgs[p].pathList = pathList.concat(newCfg.pathList);
					convertPathMatcher(pluginCfgs[p]);
				}
			}

			// ugh, this is ugly, but necessary until we refactor this function
			// copy previous pathMap items onto pathList
			for (p in prevCfg.pathMap) {
				if (!newCfg.pathMap.hasOwnProperty(p)) newCfg.pathList.push(p);
			}

			convertPathMatcher(newCfg);

			return newCfg;

		},

		resolvePathInfo: function (absId, cfg) {
			// searches through the configured path mappings and packages
			var pathMap, pathInfo, path, pkgCfg;

			pathMap = cfg.pathMap;

			if (!isAbsUrl(absId)) {
				path = absId.replace(cfg.pathRx, function (match) {
					// TODO: remove fallbacks here since they should never need to happen
					pathInfo = pathMap[match] || {};
					pkgCfg = pathInfo.config;
					return pathInfo.path || '';
				});
			}
			else {
				path = absId;
			}

			return {
				config: pkgCfg || userCfg,
				url: core.resolveUrl(path, cfg)
			};
		},

		resolveUrl: function (path, cfg) {
			var baseUrl = cfg.baseUrl;
			return baseUrl && !isAbsUrl(path) ? joinPath(baseUrl, path) : path;
		},

		checkToAddJsExt: function (url, cfg) {
			// don't add extension if a ? is found in the url (query params)
			// i'd like to move this feature to a moduleLoader
			return url + ((cfg || userCfg).dontAddFileExt.test(url) ? '' : '.js');
		},

		loadScript: function (def, success, failure) {
			// script processing rules learned from RequireJS
			// TODO: pass a validate function into loadScript to check if a success really is a success

			// insert script
			var el = doc.createElement('script');

			// initial script processing
			function process (ev) {
				ev = ev || global.event;
				// detect when it's done loading
				// ev.type == 'load' is for all browsers except IE6-9
				// IE6-9 need to use onreadystatechange and look for
				// el.readyState in {loaded, complete} (yes, we need both)
				if (ev.type == 'load' || readyStates[el.readyState]) {
					delete activeScripts[def.id];
					// release event listeners
					el.onload = el.onreadystatechange = el.onerror = ''; // ie cries if we use undefined
					success();
				}
			}

			function fail (e) {
				// some browsers send an event, others send a string,
				// but none of them send anything useful, so just say we failed:
				failure(new Error('Syntax or http error: ' + def.url));
			}

			// set type first since setting other properties could
			// prevent us from setting this later
			// actually, we don't even need to set this at all
			//el.type = 'text/javascript';
			// using dom0 event handlers instead of wordy w3c/ms
			el.onload = el.onreadystatechange = process;
			el.onerror = fail;
			// js! plugin uses alternate mimetypes
			el.type = def.mimetype || 'text/javascript';
			// TODO: support other charsets?
			el.charset = 'utf-8';
			el.async = !def.order;
			el.src = def.url;

			// loading will start when the script is inserted into the dom.
			// IE will load the script sync if it's in the cache, so
			// indicate the current resource definition if this happens.
			activeScripts[def.id] = el;

			head.insertBefore(el, insertBeforeEl);

			// the js! plugin uses this
			return el;
		},

		extractCjsDeps: function (defFunc) {
			// Note: ignores require() inside strings and comments
			var source, ids = [], currQuote;
			// prefer toSource (FF) since it strips comments
			source = typeof defFunc == 'string' ?
					 defFunc :
					 defFunc.toSource ? defFunc.toSource() : defFunc.toString();
			// remove comments, then look for require() or quotes
			source.replace(removeCommentsRx, '').replace(findRValueRequiresRx, function (m, rq, id, qq) {
				// if we encounter a string in the source, don't look for require()
				if (qq) {
					currQuote = currQuote == qq ? undef : currQuote;
				}
				// if we're not inside a quoted string
				else if (!currQuote) {
					ids.push(id);
				}
				return ''; // uses least RAM/CPU
			});
			return ids;
		},

		fixArgs: function (args) {
			// resolve args
			// valid combinations for define:
			// (string, array, object|function) sax|saf
			// (array, object|function) ax|af
			// (string, object|function) sx|sf
			// (object|function) x|f

			var id, deps, defFunc, defFuncArity, len, cjs;

			len = args.length;

			defFunc = args[len - 1];
			defFuncArity = isType(defFunc, 'Function') ? defFunc.length : -1;

			if (len == 2) {
				if (isType(args[0], 'Array')) {
					deps = args[0];
				}
				else {
					id = args[0];
				}
			}
			else if (len == 3) {
				id = args[0];
				deps = args[1];
			}

			// Hybrid format: assume that a definition function with zero
			// dependencies and non-zero arity is a wrapped CommonJS module
			if (!deps && defFuncArity > 0) {
				cjs = true;
				deps = ['require', 'exports', 'module'].slice(0, defFuncArity).concat(core.extractCjsDeps(defFunc));
			}

			return {
				id: id,
				deps: deps || [],
				res: defFuncArity >= 0 ? defFunc : function () { return defFunc; },
				cjs: cjs
			};
		},

		executeDefFunc: function (def) {
			var resource, moduleThis;
			// the force of AMD is strong so anything returned
			// overrides exports.
			// node.js assumes `this` === `exports` so we do that
			// for all cjs-wrapped modules, just in case.
			// also, use module.exports if that was set
			// (node.js convention).
			// note: if .module exists, .exports exists.
			moduleThis = def.cjs ? def.exports : undef;
			resource = def.res.apply(moduleThis, def.deps);
			if (resource === undef && def.exports) {
				// note: exports will equal module.exports unless
				// module.exports was reassigned inside module.
				resource = def.module
					? (def.exports = def.module['exports'])
					: def.exports;
			}
			return resource;
		},

		defineResource: function (def, args) {

			def.res = args.res;
			def.cjs = args.cjs;
			def.depNames = args.deps;
			core.getDeps(def);

		},

		getDeps: function (parentDef) {

			var i, names, deps, len, dep, completed, name,
				exportCollector, resolveCollector;

			deps = [];
			names = parentDef.depNames;
			len = names.length;

			if (names.length == 0) allResolved();

			function collect (dep, index, alsoExport) {
				deps[index] = dep;
				if (alsoExport) exportCollector(dep, index);
			}

			// reducer-collectors
			exportCollector = countdown(len, collect, allExportsReady);
			resolveCollector = countdown(len, collect, allResolved);

			// initiate the resolution of all dependencies
			// Note: the correct handling of early exports relies on the
			// fact that the exports pseudo-dependency is always listed
			// before other module dependencies.
			for (i = 0; i < len; i++) {
				name = names[i];
				// is this "require", "exports", or "module"?
				if (name in cjsGetters) {
					// a side-effect of cjsGetters is that the cjs
					// property is also set on the def.
					resolveCollector(cjsGetters[name](parentDef), i, true);
					// if we are using the `module` or `exports` cjs variables,
					// signal any waiters/parents that we can export
					// early (see progress callback in getDep below).
					// note: this may fire for `require` as well, if it
					// is listed after `module` or `exports` in the deps list,
					// but that is okay since all waiters will only record
					// it once.
					if (parentDef.exports) {
						parentDef.progress(msgUsingExports);
					}
				}
				// check for blanks. fixes #32.
				// this helps support yepnope.js, has.js, and the has! plugin
				else if (!name) {
					resolveCollector(undef, i, true);
				}
				// normal module or plugin resource
				else {
					getDep(name, i);
				}
			}

			return parentDef;

			function getDep (name, index) {
				var resolveOnce, exportOnce, childDef, earlyExport;

				resolveOnce = countdown(1, function (dep) {
					exportOnce(dep);
					resolveCollector(dep, index);
				});
				exportOnce = countdown(1, function (dep) {
					exportCollector(dep, index);
				});

				// get child def / dep
				childDef = core.fetchDep(name, parentDef);

				// check if childDef can export. if it can, then
				// we missed the notification and it will never fire in the
				// when() below.
				earlyExport = isPromise(childDef) && childDef.exports;
				if (earlyExport) {
					exportOnce(earlyExport);
				}

				when(childDef,
					resolveOnce,
					parentDef.reject,
					parentDef.exports && function (msg) {
						// messages are only sent from childDefs that support
						// exports, and we only notify parents that understand
						// exports too.
						if (childDef.exports) {
							if (msg == msgUsingExports) {
								// if we're using exports cjs variable on both sides
								exportOnce(childDef.exports);
							}
							else if (msg == msgFactoryExecuted) {
								resolveOnce(childDef.exports);
							}
						}
					}
				);
			}

			function allResolved () {
				parentDef.resolve(deps);
			}

			function allExportsReady () {
				parentDef.exportsReady && parentDef.exportsReady(deps);
			}

		},

		fetchResDef: function (def) {

			// ensure url is computed
			core.getDefUrl(def);

			core.loadScript(def,

				function () {
					var args = argsNet;
					argsNet = undef; // reset it before we get deps

					// if our resource was not explicitly defined with an id (anonymous)
					// Note: if it did have an id, it will be resolved in the define()
					if (def.useNet !== false) {

						// if !args, nothing was added to the argsNet
						if (!args || args.ex) {
							def.reject(new Error(((args && args.ex) || 'define() missing or duplicated: ' + def.url)));
						}
						else {
							core.defineResource(def, args);
						}
					}

				},

				def.reject

			);

			return def;

		},

		fetchDep: function (depName, parentDef) {
			var toAbsId, isPreload, parentCfg, parts, absId, mainId, loaderId, pluginId,
				resId, pathInfo, def, tempDef, resCfg;

			toAbsId = parentDef.toAbsId;
			isPreload = parentDef.isPreload;
			parentCfg = parentDef.config || userCfg; // is this fallback necessary?

			absId = toAbsId(depName);

			if (absId in cache) {
				// module already exists in cache
				mainId = absId;
			}
			else {
				// check for plugin loaderId
				parts = pluginParts(absId);
				resId = parts.resourceId;
				// get id of first resource to load (which could be a plugin)
				mainId = parts.pluginId || resId;
				pathInfo = core.resolvePathInfo(mainId, parentCfg);
			}

			if (!(absId in cache)) {
				resCfg = core.resolvePathInfo(resId, parentCfg).config;
				if (parts.pluginId) {
					loaderId = mainId;
				}
				else {
					// get custom module loader from package config if not a plugin
					// TODO: move config.moduleLoader to config.loader
					loaderId = resCfg['moduleLoader'] || resCfg.moduleLoader
						|| resCfg['loader'] || resCfg.loader;
					if (loaderId) {
						// TODO: allow transforms to have relative module ids?
						// (we could do this by returning package location from
						// resolvePathInfo. why not return all package info?)
						resId = mainId;
						mainId = loaderId;
						pathInfo = core.resolvePathInfo(loaderId, parentCfg);
					}
				}
			}

			if (mainId in cache) {
				def = cache[mainId];
			}
			else if (pathInfo.url in urlCache) {
				def = cache[mainId] = urlCache[pathInfo.url];
			}
			else {
				def = core.createResourceDef(resCfg, mainId, isPreload);
				// TODO: can this go inside createResourceDef?
				// TODO: can we pass pathInfo.url to createResourceDef instead?
				def.url = core.checkToAddJsExt(pathInfo.url, pathInfo.config);
				cache[mainId] = urlCache[pathInfo.url] = def;
				core.fetchResDef(def);
			}

			// plugin or transformer
			if (mainId == loaderId) {

				// use plugin's config if specified
				if (parts.pluginId && parentCfg.plugins[parts.pluginId]) {
					resCfg = parentCfg.plugins[parts.pluginId];
				}
				// we need to use an anonymous promise until plugin tells
				// us normalized id. then, we need to consolidate the promises
				// below. Note: exports objects will be different between
				// pre-normalized and post-normalized defs! does this matter?
				// don't put this resource def in the cache because if the
				// resId doesn't change, the check if this is a new
				// normalizedDef (below) will think it's already being loaded.
				tempDef = new Promise();

				// wait for plugin resource def
				when(def, function(plugin) {
					var normalizedDef, fullId, dynamic;

					dynamic = plugin['dynamic'];
					// check if plugin supports the normalize method
					if ('normalize' in plugin) {
						// note: dojo/has may return falsey values (0, actually)
						resId = plugin['normalize'](resId, toAbsId, def.config) || '';
					}
					else {
						resId = toAbsId(resId);
					}

					// use the full id (loaderId + id) to id plugin resources
					// so multiple plugins may each process the same resource
					// resId could be blank if the plugin doesn't require any (e.g. "domReady!")
					fullId = loaderId + '!' + resId;
					normalizedDef = cache[fullId];

					// if this is our first time fetching this (normalized) def
					if (!(fullId in cache)) {

						// because we're using resId, plugins, such as wire!,
						// can use paths relative to the resource
						normalizedDef = core.createPluginDef(resCfg, fullId, resId, isPreload);

						// don't cache non-determinate "dynamic" resources
						if (!dynamic) {
							cache[fullId] = normalizedDef;
						}

						// curl's plugins prefer to receive a deferred,
						// but to be compatible with AMD spec, we have to
						// piggy-back on the callback function parameter:
						var loaded = function (res) {
							if (!dynamic) cache[fullId] = res;
							normalizedDef.resolve(res);
						};
						loaded['resolve'] = loaded;
						loaded['reject'] = loaded['error'] = normalizedDef.reject;

						// load the resource!
						plugin.load(resId, normalizedDef.require, loaded, resCfg);

					}

					// chain defs (resolve when plugin.load executes)
					if (tempDef != normalizedDef) {
						when(normalizedDef, tempDef.resolve, tempDef.reject, tempDef.progress);
					}

				}, tempDef.reject);

			}

			// return tempDef if this is a plugin-based resource
			return tempDef || def;
		},

		getCurrentDefName: function () {
			// IE6-9 mark the currently executing thread as "interactive"
			// Note: Opera lies about which scripts are "interactive", so we
			// just have to test for it. Opera provides a true browser test, not
			// a UA sniff, thankfully.
			// learned this trick from James Burke's RequireJS
			var def;
			if (!isType(global.opera, 'Opera')) {
				for (var d in activeScripts) {
					if (activeScripts[d].readyState == 'interactive') {
						def = d;
						break;
					}
				}
			}
			return def;
		},

		findScript: function (predicate) {
			var i = 0, scripts, script;
			scripts = doc && (doc.scripts || doc.getElementsByTagName('script'));
			while (scripts && (script = scripts[i++])) {
				if (predicate(script)) return script;
			}
		},

		extractDataAttrConfig: function () {
			var script, attr = '';
			script = core.findScript(function (script) {
				var run;
				// find data-curl-run attr on script element
				run = script.getAttribute(bootScriptAttr);
				if (run) attr = run;
				return run;
			});
			// removeAttribute is wonky (in IE6?) but this works
			if (script) {
				script.setAttribute(bootScriptAttr, '');
			}
			return attr;
		},

		bootScript: function () {
			var urls = bootScript.split(splitCommaSepRx);
			if (urls.length) {
				load();
			}
			function load () {
				// Note: IE calls success handler if it gets a 400+.
				core.loadScript({ url: urls.shift() }, check, check);
			}
			function check () {
				// check if run.js called curl() or curl.config()
				if (bootScript) {
					if (urls.length) {
						// show an error message
						core.nextTurn(fail);
						// try next
						load();
					}
					else fail('run.js script did not run.');
				}
			}
			function fail (msg) {
				throw new Error(msg || 'Primary run.js failed. Trying fallback.');
			}
		},

		nextTurn: function (task) {
			setTimeout(task, 0);
		}

	};

	// hook-up cjs free variable getters
	cjsGetters = {'require': core.getCjsRequire, 'exports': core.getCjsExports, 'module': core.getCjsModule};

	function _curl (/* various */) {
		var args, promise, cfg;

		// indicate we're no longer waiting for a boot script
		bootScript = '';

		args = [].slice.call(arguments);

		// extract config, if it's specified
		if (isType(args[0], 'Object')) {
			cfg = args.shift();
			promise = _config(cfg);
		}

		return new CurlApi(args[0], args[1], args[2], promise);
	}

	function _config (cfg, callback, errback) {
		var pPromise, main, fallback;

		// indicate we're no longer waiting for a boot script
		bootScript = '';

		if (cfg) {
			core.setApi(cfg);
			userCfg = core.config(cfg);
			// check for preloads
			if ('preloads' in cfg) {
				pPromise = new CurlApi(cfg['preloads'], undef, errback, preload, true);
				// yes, this is hacky and embarrassing. now that we've got that
				// settled... until curl has deferred factory execution, this
				// is the only way to stop preloads from dead-locking when
				// they have dependencies inside a bundle.
				core.nextTurn(function () { preload = pPromise; });
			}
			// check for main module(s). all modules wait for preloads implicitly.
			main = cfg['main'];
			if (main) {
				return new CurlApi(main, callback, errback);
			}
		}
	}

	// thanks to Joop Ringelberg for helping troubleshoot the API
	function CurlApi (ids, callback, errback, waitFor, isPreload) {
		var then, ctx;

		ctx = core.createContext(userCfg, undef, [].concat(ids), isPreload);

		this['then'] = this.then = then = function (resolved, rejected) {
			when(ctx,
				// return the dependencies as arguments, not an array
				function (deps) {
					if (resolved) resolved.apply(undef, deps);
				},
				// just throw if the dev didn't specify an error handler
				function (ex) {
					if (rejected) rejected(ex); else throw ex;
				}
			);
			return this;
		};

		this['next'] = function (ids, cb, eb) {
			// chain api
			return new CurlApi(ids, cb, eb, ctx);
		};

		this['config'] = _config;

		if (callback || errback) then(callback, errback);

		// ensure next-turn so inline code can execute first
		core.nextTurn(function () {
			when(isPreload || preload, function () {
				when(waitFor, function () { core.getDeps(ctx); }, errback);
			});
		});
	}

	_curl['version'] = version;
	_curl['config'] = _config;

	function _define (args) {

		var id, def, pathInfo;

		id = args.id;

		if (id == undef) {
			if (argsNet !== undef) {
				argsNet = { ex: 'Multiple anonymous defines encountered' };
			}
			else if (!(id = core.getCurrentDefName())/* intentional assignment */) {
				// anonymous define(), defer processing until after script loads
				argsNet = args;
			}
		}
		if (id != undef) {
			// named define(), it is in the cache if we are loading a dependency
			// (could also be a secondary define() appearing in a built file, etc.)
			def = cache[id];
			if (!(id in cache)) {
				// id is an absolute id in this case, so we can get the config.
				pathInfo = core.resolvePathInfo(id, userCfg);
				def = core.createResourceDef(pathInfo.config, id);
				cache[id] = def;
			}
			if (!isPromise(def)) throw new Error('duplicate define: ' + id);
			// check if this resource has already been resolved
			def.useNet = false;
			core.defineResource(def, args);
		}

	}

	function define () {
		// wrap inner _define so it can be replaced without losing define.amd
		var args = core.fixArgs(arguments);
		_define(args);
	}

	// indicate our capabilities:
	define['amd'] = { 'plugins': true, 'jQuery': true, 'curl': version };

	// default configs
	userCfg = {
		baseUrl: '',
		pluginPath: 'curl/plugin',
		dontAddFileExt: dontAddExtRx,
		paths: {},
		packages: {},
		plugins: {},
		pathMap: {},
		pathRx: /$^/
	};

	// handle pre-existing global
	prevCurl = global[curlName];
	prevDefine = global[defineName];

	// only run config if there is something to config (perf saver?)
	if (prevCurl && isType(prevCurl, 'Object')) {
		// remove global curl object
		global[curlName] = undef; // can't use delete in IE 6-8
		// configure curl
		_config(prevCurl);
	}
	else {
		// set default api
		core.setApi();
	}

	// look for "data-curl-run" directive
	bootScript = core.extractDataAttrConfig();
	// wait a bit in case curl.js is bundled into the boot script
	if (bootScript) core.nextTurn(core.bootScript);

	// allow curl to be a dependency
	cache[curlName] = _curl;

	// expose curl core for special plugins and modules
	// Note: core overrides will only work in either of two scenarios:
	// 1. the files are running un-compressed (Google Closure or Uglify)
	// 2. the overriding module was compressed into the same file as curl.js
	// Compiling curl and the overriding module separately won't work.
	cache['curl/_privileged'] = {
		'core': core,
		'cache': cache,
		'config': function () { return userCfg; },
		'_define': _define,
		'_curl': _curl,
		'Promise': Promise
	};

}(this.window || (typeof global != 'undefined' && global) || this));

define("ext/curl", function(){});

define('date_format',['require','exports','module'],function(require, exports, module) {


module.exports = navigator.mozL10n.DateTimeFormat();

});

define('timespan',['require','exports','module'],function(require, exports, module) {


function Timespan(startDate, endDate) {
  this.start = startDate.valueOf();
  this.end = endDate.valueOf();
}
module.exports = Timespan;

Timespan.prototype = {
  isEqual: function(inputSpan) {
    return (
      this.start === inputSpan.start &&
      this.end === inputSpan.end
    );
  },

  /**
   * If given Timespan overlaps this timespan
   * return a new timespan with the overlapping
   * parts removed.
   *
   * See tests for examples...
   */
  trimOverlap: function(span) {
    if (this.contains(span) || span.contains(this)) {
      return null;
    }

    var start = span.start;
    var end = span.end;
    var ourEnd = this.end;
    var ourStart = this.start;

    var overlapsBefore = start >= ourStart && start < ourEnd;
    var overlapsAfter = ourStart >= start && ourStart < end;

    var newStart = span.start;
    var newEnd = span.end;

    if (overlapsBefore) {
      newStart = ourEnd + 1;
    }

    if (overlapsAfter) {
      newEnd = ourStart - 1;
    }

    return new Timespan(newStart, newEnd);
  },

  /**
   * Checks if given time overlaps with
   * range.
   *
   * @param {Date|Numeric|Timespan} start range or one position.
   * @param {Date|Numeric} [end] do a span comparison.
   */
  overlaps: function(start, end) {
    var ourStart = this.start;
    var ourEnd = this.end;

    if (start instanceof Timespan) {
      end = start.end;
      start = start.start;
    } else {
      // start/end expected
      start = (start instanceof Date) ? start.valueOf() : start;
      end = (end instanceof Date) ? end.valueOf() : end;
    }

    return (
        start >= ourStart && start < ourEnd ||
        ourStart >= start && ourStart < end
    );
  },

  /**
   * When given a date checks if
   * date is inside given range.
   *
   *
   * @param {Date} date date or event.
   */
  contains: function(date) {
    var start = this.start;
    var end = this.end;

    if (date instanceof Date) {
      return start <= date && end >= date;
    } else if (date instanceof Timespan) {
      return start <= date.start &&
             end >= date.end;
    } else {
      return this.containsNumeric(date);
    }
  },

  /**
   * Numeric comparison assumes
   * given seconds since epoch.
   *
   * @param {Numeric} timestamp numeric timestamp.
   */
  containsNumeric: function(timestamp) {
    var start = this.start;
    var end = this.end;

    return start <= timestamp &&
           end >= timestamp;
  }
};

});

define('calc',['require','exports','module','date_format','timespan'],function(require, exports) {


var localeFormat = require('date_format').localeFormat;
var Timespan = require('timespan');

const SECOND = 1000;
const MINUTE = (SECOND * 60);
const HOUR = MINUTE * 60;

exports._hourDate = new Date();
exports.startDay = 0;
exports.FLOATING = 'floating';
exports.ALLDAY = 'allday';
exports.SECOND = SECOND;
exports.MINUTE = MINUTE;
exports.HOUR = HOUR;
exports.PAST = 'past';
exports.NEXT_MONTH = 'next-month';
exports.OTHER_MONTH = 'other-month';
exports.PRESENT = 'present';
exports.FUTURE = 'future';

Object.defineProperty(exports, 'today', {
  get: function() {
    return new Date();
  }
});

exports.getTimeL10nLabel = function(timeLabel) {
  return timeLabel + (navigator.mozHour12 ? '12' : '24');
};

exports.daysInWeek = function() {
  // XXX: We need to localize this...
  return 7;
};

/**
 * Calculates day of week when starting day is Monday.
 */
exports.dayOfWeekFromMonday = function(numeric) {
  var day = numeric - 1;
  if (day < 0) {
    return 6;
  }
  return day;
};

/**
 * Calculates day of week from startDay value
 * passed by the locale currently being used
 */
exports.dayOfWeekFromStartDay = function(numeric) {
  var day = numeric - exports.startDay;
  if (day < 0) {
    return 7 + day;
  }
  return day;
};

/**
 * Checks is given date is today.
 *
 * @param {Date} date compare.
 * @return {Boolean} true when today.
 */
exports.isToday = function(date) {
  return exports.isSameDate(date, exports.today);
};

/**
 * Checks if date object only contains date information (not time).
 *
 * Example:
 *
 *    var time = new Date(2012, 0, 1, 1);
 *    this._isOnlyDate(time); // false
 *
 *    var time = new Date(2012, 0, 1);
 *    this._isOnlyDate(time); // true
 *
 * @param {Date} date to verify.
 * @return {Boolean} see above.
 */
exports.isOnlyDate = function(date) {
  if (
    date.getHours() === 0 &&
    date.getMinutes() === 0 &&
    date.getSeconds() === 0
  ) {
    return true;
  }

  return false;
};

/**
 * Calculates the difference between
 * two points in hours.
 *
 * @param {Date|Numeric} start start hour.
 * @param {Date|Numeric} end end hour.
 */
exports.hourDiff = function(start, end) {
  start = (start instanceof Date) ? start.valueOf() : start;
  end = (end instanceof Date) ? end.valueOf() : end;

  start = start / HOUR;
  end = end / HOUR;

  return end - start;
};

/**
 * Creates timespan for given day.
 *
 * @param {Date} date date of span.
 * @param {Boolean} includeTime uses given date
 *                           as the start time of the timespan
 *                           rather then the absolute start of
 *                           the day of the given date.
 */
exports.spanOfDay = function(date, includeTime) {
  if (typeof(includeTime) === 'undefined') {
    date = exports.createDay(date);
  }

  var end = exports.createDay(date);
  end.setDate(end.getDate() + 1);
  return new Timespan(date, end);
};

/**
 * Creates timespan for a given month.
 * Starts at the first week that occurs
 * in the given month. Ends at 42 days after the start day,
 * because the UI SPEC require.
 */
exports.spanOfMonth = function(month) {
  var spanScope = 42;
  month = exports.monthStart(month);
  var startDay = exports.getWeekStartDate(month);
  var endDay = exports.monthEnd(month);
  endDay = exports.getWeekEndDate(endDay);
  var  days = exports.daysBetween(startDay, endDay);
  var diffDays = endDay.getDate() + spanScope - days.length;
  if (days.length < spanScope) {
    endDay = exports.createDay(endDay, diffDays);
  }
  return new Timespan(startDay, endDay);
};

exports.monthEnd = function(date, diff = 0) {
  var endDay = new Date(
    date.getFullYear(),
    date.getMonth() + diff + 1,
    1
  );
  endDay.setMilliseconds(-1);
  return endDay;
};

/**
 * Converts a date to UTC
 */
exports.getUTC = function(date) {
  return new Date(
    date.getUTCFullYear(),
    date.getUTCMonth(),
    date.getUTCDate(),
    date.getUTCHours(),
    date.getUTCMinutes(),
    date.getUTCSeconds(),
    date.getUTCMilliseconds()
  );
};

/**
 * Converts transport time into a JS Date object.
 *
 * @param {Object} transport date in transport format.
 * @return {Date} javascript date converts the transport into
 *                the current time.
 */
exports.dateFromTransport = function(transport) {
  var utc = transport.utc;
  var offset = transport.offset;
  var zone = transport.tzid;

  var date = new Date(
    // offset is expected to be 0 in the floating case
    parseInt(utc) - parseInt(offset)
  );

  if (zone && zone === exports.FLOATING) {
    return exports.getUTC(date);
  }

  return date;
};

/**
 * Converts a date object into a transport value
 * which can be stored in the database or sent
 * to a service.
 *
 * When the tzid value is given an is the string
 * value of "floating" it will convert the local
 * time directly to UTC zero and record no offset.
 * This along with the tzid is understood to be
 * a "floating" time which will occur at that position
 * regardless of the current tzid's offset.
 *
 * @param {Date} date js date object.
 * @param {String} [tzid] optional tzid.
 * @param {Boolean} isDate true when is a "date" representation.
 */
exports.dateToTransport = function(date, tzid, isDate) {
  var result = Object.create(null);

  if (isDate) {
    result.isDate = isDate;
  }

  if (tzid) {
    result.tzid = tzid;
  }

  var utc = Date.UTC(
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
    date.getMilliseconds()
  );

  // remember a "date" is always a floating
  // point in time otherwise we don't use it...
  if (isDate || tzid && tzid === exports.FLOATING) {
    result.utc = utc;
    result.offset = 0;
    result.tzid = exports.FLOATING;
  } else {
    var localUtc = date.valueOf();
    var offset = utc - localUtc;

    result.utc = utc;
    result.offset = offset;
  }

  return result;
};

/**
 * Checks if two date objects occur
 * on the same date (in the same month, year, day).
 * Disregards time.
 *
 * @param {Date} first date.
 * @param {Date} second date.
 * @return {Boolean} true when they are the same date.
 */
exports.isSameDate = function(first, second) {
  return first.getMonth() == second.getMonth() &&
         first.getDate() == second.getDate() &&
         first.getFullYear() == second.getFullYear();
};

/**
 * Returns an identifier for a specific
 * date in time for a given date
 *
 * @param {Date} date to get id for.
 * @return {String} identifier.
 */
exports.getDayId = function(date) {
  return [
    'd',
    date.getFullYear(),
    date.getMonth(),
    date.getDate()
  ].join('-');
};

/**
 * Returns a date object from
 * a string id for a date.
 *
 * @param {String} id identifier for date.
 * @return {Date} date output.
 */
exports.dateFromId = function(id) {
  var parts = id.split('-'),
      date,
      type;

  if (parts.length > 1) {
    type = parts.shift();
    switch (type) {
      case 'd':
        date = new Date(parts[0], parts[1], parts[2]);
        break;
      case 'm':
        date = new Date(parts[0], parts[1]);
        break;
    }
  }

  return date;
};

/**
 * Returns an identifier for a specific
 * month in time for a given date.
 *
 * @return {String} identifier.
 */
exports.getMonthId = function(date) {
  return [
    'm',
    date.getFullYear(),
    date.getMonth()
  ].join('-');
};

exports.createDay = function(date, day, month, year) {
  return new Date(
    year != null ? year : date.getFullYear(),
    month != null ? month : date.getMonth(),
    day != null ? day : date.getDate()
  );
};

exports.endOfDay = function(date) {
  var day = exports.createDay(date, date.getDate() + 1);
  day.setMilliseconds(-1);
  return day;
};

exports.monthStart = function(date, diff = 0) {
  return new Date(date.getFullYear(), date.getMonth() + diff, 1);
};

/**
 * Returns localized day of week.
 *
 * @param {Date|Number} date numeric or date object.
 */
exports.dayOfWeek = function(date) {
  var number = date;

  if (typeof(date) !== 'number') {
    number = date.getDay();
  }
  return exports.dayOfWeekFromStartDay(number);
};

/**
 * Finds localized week start date of given date.
 *
 * @param {Date} date any day the week.
 * @return {Date} first date in the week of given date.
 */
exports.getWeekStartDate = function(date) {
  var currentDay = exports.dayOfWeek(date);
  var startDay = (date.getDate() - currentDay);

  return exports.createDay(date, startDay);
};

exports.getWeekEndDate = function(date) {
  // TODO: There are localization problems
  // with this approach as we assume a 7 day week.
  var start = exports.getWeekStartDate(date);
  start.setDate(start.getDate() + 7);
  start.setMilliseconds(-1);

  return start;
};

exports.getWeeksOfYear = function(date) {
  var l10Id = exports.startDay === 0 ? '%U' : '%W';
  var weeks = localeFormat(date, l10Id);
  return parseInt(weeks) + 1;
};

/**
 * Returns an array of dates objects.
 * Inclusive. First and last are
 * the given instances.
 *
 * @param {Date} start starting day.
 * @param {Date} end ending day.
 * @param {Boolean} includeTime include times start/end ?
 */
exports.daysBetween = function(start, end, includeTime) {
  if (start instanceof Timespan) {
    if (end) {
      includeTime = end;
    }

    end = new Date(start.end);
    start = new Date(start.start);
  }

  if (start > end) {
    var tmp = end;
    end = start;
    start = tmp;
    tmp = null;
  }

  var list = [];
  var last = start.getDate();

  // handle the case where start & end dates
  // are the same date.
  if (exports.isSameDate(start, end)) {
    if (includeTime) {
      list.push(end);
    } else {
      list.push(exports.createDay(start));
    }
    return list;
  }

  while (true) {
    var next = new Date(
      start.getFullYear(),
      start.getMonth(),
      ++last
    );

    if (next > end) {
      throw new Error(
        'sanity fails next is greater then end'
      );
    }

    if (!exports.isSameDate(next, end)) {
      list.push(next);
      continue;
    }

    break;
  }

  if (includeTime) {
    list.unshift(start);
    list.push(end);
  } else {
    list.unshift(exports.createDay(start));
    list.push(exports.createDay(end));
  }

  return list;
},

/**
 * Returns an array of weekdays based on the start date.
 * Will always return the 7 daysof that week regardless of
 * what the start date isbut they will be returned
 * in the order of their localized getDay function.
 *
 * @param {Date} startDate point of origin.
 * @return {Array} a list of dates in order of getDay().
 */
exports.getWeeksDays = function(startDate) {
  //local day position
  var weeksDayStart = exports.getWeekStartDate(startDate);
  var result = [weeksDayStart];

  for (var i = 1; i < 7; i++) {
    result.push(exports.createDay(weeksDayStart, weeksDayStart.getDate() + i));
  }

  return result;
};

/**
 * Checks if date is in the past
 *
 * @param {Date} date to check.
 * @return {Boolean} true when date is in the past.
 */
exports.isPast = function(date) {
  return (date.valueOf() < exports.today.valueOf());
};

/**
 * Checks if date is in the future
 *
 * @param {Date} date to check.
 * @return {Boolean} true when date is in the future.
 */
exports.isFuture = function(date) {
  return !exports.isPast(date);
};

/**
 * Based on the input date
 * will return one of the following states
 *
 *  past, present, future
 *
 * @param {Date} day for compare.
 * @param {Date} month comparison month.
 * @return {String} state.
 */
exports.relativeState = function(day, month) {
  var states;
  //var today = exports.today;

  // 1. the date is today (real time)
  if (exports.isToday(day)) {
    states = exports.PRESENT;
  } else {
    // 2. the date is in the past (real time)
    if (exports.isPast(day)) {
      states = exports.PAST;
    // 3. the date is in the future (real time)
    } else {
      states = exports.FUTURE;
    }
  }

  // 4. the date is not in the current month (relative time)
  if (day.getMonth() !== month.getMonth()) {
    states += ' ' + exports.OTHER_MONTH;
  }

  return states;
};

/**
 * Computes the relative hour (0...23.9999) inside the given day.
 * If `date` is on a different day than `baseDate` it will return `0`.
 * Used by week view to compute the position of the busytimes relative to
 * the top of the view.
 */
exports.relativeOffset = function(baseDate, date) {
  if (exports.isSameDate(baseDate, date)) {
    return date.getHours() + (date.getMinutes() / 60);
  }
  // different day!
  return 0;
};

/**
 * Computes the relative duration between startDate and endDate inside
 * a given baseDate. Returns a number between 0 and 24.
 * Used by MultiDay view to compute the height of the busytimes relative to
 * the length inside the baseDate.
 */
exports.relativeDuration = function(baseDate, startDate, endDate) {
  if (!exports.isSameDate(startDate, endDate)) {
    if (exports.isSameDate(baseDate, startDate)) {
      endDate = exports.endOfDay(baseDate);
    } else if (exports.isSameDate(baseDate, endDate)) {
      startDate = exports.createDay(endDate);
    } else {
      // started before baseDate and ends on a different day
      return 24;
    }
  }
  return exports.hourDiff(startDate, endDate);
};

/**
 * Check if event spans thru the whole day.
 */
exports.isAllDay = function(baseDate, startDate, endDate) {
  // beginning reference point (start of given date)
  var refStart = exports.createDay(baseDate);
  var refEnd = exports.endOfDay(baseDate);

  var startBefore = startDate <= refStart;
  var endsAfter = endDate >= refEnd;

  // yahoo uses same start/end date for recurring all day events!!!
  return (startBefore && endsAfter) || Number(startDate) === Number(endDate);
};

navigator.mozL10n.once(() => {
  exports.startDay = parseInt(navigator.mozL10n.get('firstDayOfTheWeek'), 10);
});

window.addEventListener('localized', function changeStartDay() {
  exports.startDay = parseInt(navigator.mozL10n.get('firstDayOfTheWeek'), 10);
});

});

define('date_l10n',['require','exports','module','date_format'],function(require, exports) {


var dateFormat = require('date_format');

/**
 * Localizes all elements with data-l10n-date-format.
 */
exports.localizeElements = function() {
  var elements = document.querySelectorAll('[data-l10n-date-format]');
  for (var i = 0; i < elements.length; i++) {
    exports.localizeElement(elements[i]);
  }
};

exports.changeElementsHourFormat = function() {
  var isHour12 = navigator.mozHour12;
  var previousFormat = isHour12 ? 24 : 12;
  var currentFormat = isHour12 ? 12 : 24;
  var elements = document.querySelectorAll(
    `[data-l10n-date-format*="${previousFormat}"]`
  );

  Array.prototype.forEach.call(elements, (element) => {
    var format = element.dataset.l10nDateFormat.replace(
      previousFormat,
      currentFormat
    );

    element.dataset.l10nDateFormat = format;
    // Remove leading zero of hour items in day, week view sidebars.
    exports.localizeElement(element, {
      addAmPmClass: format === 'week-hour-format12',
      removeLeadingZero: format.contains('hour-format')
    });
  });
};

/**
 * Localize a single element expected to have data-l10n-date-format.
 *
 * Options:
 *
 *   (Boolean) addAmPmClass
 *   (Boolean) removeLeadingZero
 */
exports.localizeElement = function(element, options) {
  var date = element.dataset.date;
  if (!date) {
    return;
  }

  var l10n = navigator.mozL10n;
  var format = l10n.get(element.dataset.l10nDateFormat);
  if (options && options.addAmPmClass) {
    // developer.mozilla.org/docs/Mozilla/Localization/Localization_best_practices#Avoid_unnecessary_complexity_in_strings
    format = format.replace(/\s*%p\s*/, '<span class="ampm">%p</span>');
  }

  var text = dateFormat.localeFormat(new Date(date), format);
  if (options && options.removeLeadingZero) {
    text = text.replace(/^0/, '');
  }

  element.textContent = text;
};

});

define('models/account',['require','exports','module'],function(require, exports, module) {


function Account(options) {
  var key;

  if (typeof(options) === 'undefined') {
    options = {};
  }

  for (key in options) {
    if (options.hasOwnProperty(key)) {
      this[key] = options[key];
    }
  }
}
module.exports = Account;

Account.prototype = {

  /**
   * Type of provider this
   * account requires.
   */
  providerType: null,

  /**
   * ID for this model always set
   * by the store when hydrating
   */
  id: null,

  /**
   * Which preset this model came from.
   */
  preset: null,

  /**
   * Domain for account
   */
  domain: '',

  /**
   * url/path for account
   */
  entrypoint: '',

  /**
   * Location where calendars can be found.
   * May be the same as entrypoint.
   */
  calendarHome: '',

  /**
   * username for authentication
   */
  user: '',

  /**
   * password for authentication
   */
  password: '',

  get fullUrl() {
    return this.domain + this.entrypoint;
  },

  set fullUrl(value) {
    var protocolIdx = value.indexOf('://');

    this.domain = value;
    this.entrypoint = '/';

    if (protocolIdx !== -1) {
      protocolIdx += 3;
      // next chunk
      var domainChunk = value.substr(protocolIdx);
      var pathIdx = domainChunk.indexOf('/');


      if (pathIdx !== -1) {
        pathIdx = pathIdx + protocolIdx;

        this.entrypoint = value.substr(pathIdx);
        this.domain = value.substr(0, pathIdx);
      }

    }
  },

  /**
   * Data only version of this object.
   * Used for both passing data between
   * threads (workers) and persisting data
   * in indexeddb.
   */
  toJSON: function() {
    var output = {};
    var fields = [
      'entrypoint',
      'calendarHome',
      'domain',
      'password',
      'user',
      'providerType',
      'preset',
      'oauth',
      'error'
    ];

    fields.forEach(function(key) {
      output[key] = this[key];
    }, this);

    if (this._id || this._id === 0) {
      output._id = this._id;
    }

    return output;
  }

};

});

define('presets',{
  "google": {
    "providerType": "Caldav",
    "group": "remote",
    "authenticationType": "oauth2",
    "apiCredentials": {
      "tokenUrl": "https://accounts.google.com/o/oauth2/token",
      "authorizationUrl": "https://accounts.google.com/o/oauth2/auth",
      "user_info": {
        "url": "https://www.googleapis.com/oauth2/v3/userinfo",
        "field": "email"
      },
      "client_secret": "jQTKlOhF-RclGaGJot3HIcVf",
      "client_id": "605300196874-1ki833poa7uqabmh3hq6u1onlqlsi54h.apps.googleusercontent.com",
      "scope": "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/userinfo.email",
      "redirect_uri": "https://oauth.gaiamobile.org/authenticated"
    },
    "options": {
      "domain": "https://apidata.googleusercontent.com",
      "entrypoint": "/caldav/v2/",
      "providerType": "Caldav"
    }
  },
  "yahoo": {
    "providerType": "Caldav",
    "group": "remote",
    "options": {
      "domain": "https://caldav.calendar.yahoo.com",
      "entrypoint": "/",
      "providerType": "Caldav",
      "user": "@yahoo.com",
      "usernameType": "email"
    }
  },
  "caldav": {
    "providerType": "Caldav",
    "group": "remote",
    "options": {
      "domain": "",
      "entrypoint": "",
      "providerType": "Caldav"
    }
  },
  "local": {
    "singleUse": true,
    "providerType": "Local",
    "group": "local",
    "options": {
      "providerType": "Local"
    }
  }
});
/**
 * @fileoverview Utilities for converting async functions which use
 *     node-style callbacks to also cater promises callers.
 */
define('promise',['require','exports','module'],function(require, exports) {


function denodeify(fn) {
  // This is our new, denodeified function. You can interact with it using
  // node-style callbacks or promises.
  return function() {
    // Original arguments to fn.
    var args = Array.slice(arguments);
    var callback = args[args.length - 1];
    if (typeof callback === 'function') {
      // If consumer is trying to interact with node-style callback, let them.
      return fn.apply(this, args);
    }

    // We need the defer style promise api here since we don't want to
    // accidentily step on functions that return things like DOMRequests...
    var deferred = defer();
    args.push((err, result) => {
      if (err) {
        return deferred.reject(err);
      }

      deferred.resolve(result);
    });

    // Return the promise <=> the function doesn't return an object.
    var returnValue = fn.apply(this, args);
    return typeof returnValue === 'object' ? returnValue : deferred.promise;
  };
}
exports.denodeify = denodeify;

function denodeifyAll(object, methods) {
  methods.forEach((method) => {
    object[method] = exports.denodeify(object[method]);
  });
}
exports.denodeifyAll = denodeifyAll;

function defer() {
  var deferred = {};
  var promise = new Promise((resolve, reject) => {
    deferred.resolve = resolve;
    deferred.reject = reject;
  });

  deferred.promise = promise;
  return deferred;
}

});

define('next_tick',['require','exports','module'],function(require, exports, module) {


var resolved = Promise.resolve();

/**
 * Very similar to node's nextTick.
 * Much faster than setTimeout or window.postMessage
 */
module.exports = function(callback) {
  resolved.then(callback);
};

});

define('provider/abstract',['require','exports','module','promise','next_tick'],function(require, exports, module) {


var denodeifyAll = require('promise').denodeifyAll;
var nextTick = require('next_tick');

function Abstract(options) {
  var key;
  for (key in options) {
    if (options.hasOwnProperty(key)) {
      this[key] = options[key];
    }
  }


  denodeifyAll(this, [
    'eventCapabilities',
    'getAccount',
    'findCalendars',
    'syncEvents',
    'ensureRecurrencesExpanded',
    'createEvent',
    'updateEvent',
    'deleteEvent'
  ]);
}
module.exports = Abstract;

Abstract.prototype = {
  // orange (used by local calendar)
  defaultColor: '#F97C17',

  /**
   * Does this provider require credentials.
   */
  useCredentials: false,

  /**
   * Does this provider require a url.
   */
  useUrl: false,

  /**
   * Can provider sync with remote server?
   */
  canSync: false,

  /**
   * Can expand recurring events?
   */
  canExpandRecurringEvents: false,

  /**
   *  - domain: (String)
   *  - password: (String)
   *  - user: (String)
   *
   * @param {Object} account user credentials.
   * @param {Function} callback node style (err, result).
   */
  getAccount: function(account, callback) {},

  /**
   * Attempts to find all calendars
   * for a given account.
   *
   *
   * account: (same as getAccount)
   *
   * @param {Object} account user credentials.
   * @param {Function} callback node style (err, result).
   */
  findCalendars: function() {},

  /**
   * Sync remote and local events.
   *
   */
  syncEvents: function(account, calendar, callback) {},

  /**
   * Ensures recurring events are expanded up to the given date.
   *
   * Its very important to correctly return the second callback arg
   * in the subclasses callback. When requiredExpansion is returned as true a
   * second call will likely be made to ensureRecurrencesExpanded to verify
   * there are no more pending events for the date (controller handles this).
   *
   * @param {Date} date to expand recurring events to.
   * @param {Function} callback [err, requiredExpansion].
   *  first argument is error, second indicates if any expansion was done.
   */
  ensureRecurrencesExpanded: function(date, callback) {},

  /**
   * Update an event
   *
   * @param {Object} event record from event store.
   *
   * @param {Object} [busytime] optional busytime instance
   *                 when a busytime is passed the edit is treated
   *                 as an "exception" and will only edit the one recurrence
   *                 related to the busytime. This may result in the creation
   *                 of a new "event" related to the busytime.
   */
  updateEvent: function(event, busytime, callback) {},

  /**
   * Delete event
   *
   * @param {Object} event record from the event store.
   * @param {Object} [busytime] optional busytime instance
   *                 when given it will only remove this occurence/exception
   *                 of the event rather then the entire sequence of events.
   */
  deleteEvent: function(event, busytime, callback) {},

  /**
   * Create an event
   */
  createEvent: function(event, callback) {},

  /**
   * Returns an object with three keys used to
   * determine the capabilities of a given calendar.
   *
   * - canCreate (Boolean)
   * - canUpdate (Boolean)
   * - canDelete (Boolean)
   *
   * @param {Object} calendar full calendar details.
   */
  calendarCapabilities: function(calendar) {
    return {
      canCreateEvent: true,
      canUpdateEvent: true,
      canDeleteEvent: true
    };
  },

  /**
   * Returns the capabilities of a single event.
   *
   * @param {Object} event object.
   * @param {Function} callback [err, caps].
   */
  eventCapabilities: function(event, callback) {
    var caps = this.calendarCapabilities();

    nextTick(function() {
      callback(null, {
        canCreate: caps.canCreateEvent,
        canUpdate: caps.canUpdateEvent,
        canDelete: caps.canDeleteEvent
      });
    });
  }
};

});

//     uuid.js
//
//     Copyright (c) 2010-2012 Robert Kieffer
//     MIT License - http://opensource.org/licenses/mit-license.php

(function() {
  var _global = this;

  // Unique ID creation requires a high quality random # generator.  We feature
  // detect to determine the best RNG source, normalizing to a function that
  // returns 128-bits of randomness, since that's what's usually required
  var _rng;

  // Node.js crypto-based RNG - http://nodejs.org/docs/v0.6.2/api/crypto.html
  //
  // Moderately fast, high quality
  if (typeof(_global.require) == 'function' && typeof(module) != 'undefined' && module.exports) {
    try {
      var _rb = _global.require('crypto').randomBytes;
      _rng = _rb && function() {return _rb(16);};
    } catch(e) {}
  }

  if (!_rng && _global.crypto && crypto.getRandomValues) {
    // WHATWG crypto-based RNG - http://wiki.whatwg.org/wiki/Crypto
    //
    // Moderately fast, high quality
    var _rnds8 = new Uint8Array(16);
    _rng = function whatwgRNG() {
      crypto.getRandomValues(_rnds8);
      return _rnds8;
    };
  }

  if (!_rng) {
    // Math.random()-based (RNG)
    //
    // If all else fails, use Math.random().  It's fast, but is of unspecified
    // quality.
    var  _rnds = new Array(16);
    _rng = function() {
      for (var i = 0, r; i < 16; i++) {
        if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
        _rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
      }

      return _rnds;
    };
  }

  // Buffer class to use
  var BufferClass = typeof(_global.Buffer) == 'function' ? _global.Buffer : Array;

  // Maps for number <-> hex string conversion
  var _byteToHex = [];
  var _hexToByte = {};
  for (var i = 0; i < 256; i++) {
    _byteToHex[i] = (i + 0x100).toString(16).substr(1);
    _hexToByte[_byteToHex[i]] = i;
  }

  // **`parse()` - Parse a UUID into it's component bytes**
  function parse(s, buf, offset) {
    var i = (buf && offset) || 0, ii = 0;

    buf = buf || [];
    s.toLowerCase().replace(/[0-9a-f]{2}/g, function(oct) {
      if (ii < 16) { // Don't overflow!
        buf[i + ii++] = _hexToByte[oct];
      }
    });

    // Zero out remaining bytes if string was short
    while (ii < 16) {
      buf[i + ii++] = 0;
    }

    return buf;
  }

  // **`unparse()` - Convert UUID byte array (ala parse()) into a string**
  function unparse(buf, offset) {
    var i = offset || 0, bth = _byteToHex;
    return  bth[buf[i++]] + bth[buf[i++]] +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] +
            bth[buf[i++]] + bth[buf[i++]] +
            bth[buf[i++]] + bth[buf[i++]];
  }

  // **`v1()` - Generate time-based UUID**
  //
  // Inspired by https://github.com/LiosK/UUID.js
  // and http://docs.python.org/library/uuid.html

  // random #'s we need to init node and clockseq
  var _seedBytes = _rng();

  // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
  var _nodeId = [
    _seedBytes[0] | 0x01,
    _seedBytes[1], _seedBytes[2], _seedBytes[3], _seedBytes[4], _seedBytes[5]
  ];

  // Per 4.2.2, randomize (14 bit) clockseq
  var _clockseq = (_seedBytes[6] << 8 | _seedBytes[7]) & 0x3fff;

  // Previous uuid creation time
  var _lastMSecs = 0, _lastNSecs = 0;

  // See https://github.com/broofa/node-uuid for API details
  function v1(options, buf, offset) {
    var i = buf && offset || 0;
    var b = buf || [];

    options = options || {};

    var clockseq = options.clockseq != null ? options.clockseq : _clockseq;

    // UUID timestamps are 100 nano-second units since the Gregorian epoch,
    // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
    // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
    // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
    var msecs = options.msecs != null ? options.msecs : new Date().getTime();

    // Per 4.2.1.2, use count of uuid's generated during the current clock
    // cycle to simulate higher resolution clock
    var nsecs = options.nsecs != null ? options.nsecs : _lastNSecs + 1;

    // Time since last uuid creation (in msecs)
    var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

    // Per 4.2.1.2, Bump clockseq on clock regression
    if (dt < 0 && options.clockseq == null) {
      clockseq = clockseq + 1 & 0x3fff;
    }

    // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
    // time interval
    if ((dt < 0 || msecs > _lastMSecs) && options.nsecs == null) {
      nsecs = 0;
    }

    // Per 4.2.1.2 Throw error if too many uuids are requested
    if (nsecs >= 10000) {
      throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
    }

    _lastMSecs = msecs;
    _lastNSecs = nsecs;
    _clockseq = clockseq;

    // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
    msecs += 12219292800000;

    // `time_low`
    var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
    b[i++] = tl >>> 24 & 0xff;
    b[i++] = tl >>> 16 & 0xff;
    b[i++] = tl >>> 8 & 0xff;
    b[i++] = tl & 0xff;

    // `time_mid`
    var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
    b[i++] = tmh >>> 8 & 0xff;
    b[i++] = tmh & 0xff;

    // `time_high_and_version`
    b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
    b[i++] = tmh >>> 16 & 0xff;

    // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
    b[i++] = clockseq >>> 8 | 0x80;

    // `clock_seq_low`
    b[i++] = clockseq & 0xff;

    // `node`
    var node = options.node || _nodeId;
    for (var n = 0; n < 6; n++) {
      b[i + n] = node[n];
    }

    return buf ? buf : unparse(b);
  }

  // **`v4()` - Generate random UUID**

  // See https://github.com/broofa/node-uuid for API details
  function v4(options, buf, offset) {
    // Deprecated - 'format' argument, as supported in v1.2
    var i = buf && offset || 0;

    if (typeof(options) == 'string') {
      buf = options == 'binary' ? new BufferClass(16) : null;
      options = null;
    }
    options = options || {};

    var rnds = options.random || (options.rng || _rng)();

    // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
    rnds[6] = (rnds[6] & 0x0f) | 0x40;
    rnds[8] = (rnds[8] & 0x3f) | 0x80;

    // Copy bytes to buffer, if provided
    if (buf) {
      for (var ii = 0; ii < 16; ii++) {
        buf[i + ii] = rnds[ii];
      }
    }

    return buf || unparse(rnds);
  }

  // Export public API
  var uuid = v4;
  uuid.v1 = v1;
  uuid.v4 = v4;
  uuid.parse = parse;
  uuid.unparse = unparse;
  uuid.BufferClass = BufferClass;

  if (typeof define === 'function' && define.amd) {
    // Publish as AMD module
    define('ext/uuid',[],function() {return uuid;});
  } else if (typeof(module) != 'undefined' && module.exports) {
    // Publish as node.js module
    module.exports = uuid;
  } else {
    // Publish as global (in browsers)
    var _previousRoot = _global.uuid;

    // **`noConflict()` - (browser only) to reset global 'uuid' var**
    uuid.noConflict = function() {
      _global.uuid = _previousRoot;
      return uuid;
    };

    _global.uuid = uuid;
  }
}).call(this);

/**
 * EventMutations are a simple wrapper for a
 * set of idb transactions that span multiple
 * stores and calling out to the time controller
 * at appropriate times so it can cache the information.
 *
 *
 * Examples:
 *
 *
 *    // create an event
 *    var mutation = Calendar.EventMutations.create({
 *      // this class does not handle/process events
 *      // only persisting the records. Busytimes will
 *      // automatically be recreated.
 *      event: event
 *    });
 *
 *    // add an optional component
 *    // mutation.icalComponent = component;
 *
 *    mutation.commit(function(err) {
 *      if (err) {
 *        // handle it
 *      }
 *
 *      // success event/busytime/etc.. has been created
 *    });
 *
 *
 *    // update an event:
 *    // update shares an identical api but will
 *    // destroy/recreate associated busytimes with event.
 *    var mutation = Calendar.EventMutations.update({
 *      event: event
 *    });
 *
 *    mutation.commit(function() {
 *
 *    });
 *
 *
 */
define('event_mutations',['require','exports','module','calc','ext/uuid'],function(require, exports) {


var Calc = require('calc');
var uuid = require('ext/uuid');

/**
 * Create a single instance busytime for the given event object.
 *
 * @param {Object} event to create busytime for.
 */
function createBusytime(event) {
  return {
    _id: event._id + '-' + uuid.v4(),
    eventId: event._id,
    calendarId: event.calendarId,
    start: event.remote.start,
    end: event.remote.end
  };
}

function Create(options) {
  if (options) {
    for (var key in options) {
      if (options.hasOwnProperty(key)) {
        this[key] = options[key];
      }
    }
  }
}

Create.prototype = {
  commit: function(callback) {
    var app = exports.app;
    var alarmStore = app.store('Alarm');
    var eventStore = app.store('Event');
    var busytimeStore = app.store('Busytime');
    var componentStore = app.store('IcalComponent');

    var trans = eventStore.db.transaction(
      eventStore._dependentStores,
      'readwrite'
    );

    trans.oncomplete = function commitComplete() {
      callback(null);
    };

    trans.onerror = function commitError(e) {
      callback(e.target.error);
    };

    eventStore.persist(this.event, trans);

    if (!this.busytime) {
      this.busytime = createBusytime(this.event);
    }

    busytimeStore.persist(this.busytime, trans);

    if (this.icalComponent) {
      componentStore.persist(this.icalComponent, trans);
    }

    var alarms = this.event.remote.alarms;
    if (alarms && alarms.length) {
      var i = 0;
      var len = alarms.length;
      var now = Date.now();

      var alarmTrans = alarmStore.db.transaction(
        ['alarms'],
        'readwrite'
      );

      for (; i < len; i++) {

        var alarm = {
          startDate: {
            offset: this.busytime.start.offset,
            utc: this.busytime.start.utc + (alarms[i].trigger * 1000),
            tzid: this.busytime.start.tzid
          },
          eventId: this.busytime.eventId,
          busytimeId: this.busytime._id
        };

        var alarmDate = Calc.dateFromTransport(this.busytime.end).valueOf();
        if (alarmDate < now) {
          continue;
        }

        alarmStore.persist(alarm, alarmTrans);
      }
    }
  }

};

function Update() {
  Create.apply(this, arguments);
}

Update.prototype = {
  commit: function(callback) {
    var app = exports.app;
    var busytimeStore = app.store('Busytime');

    var self = this;

    // required so UI knows to refresh even in the
    // case where the start/end times are the same.
    busytimeStore.removeEvent(this.event._id, function(err) {
      if (err) {
        callback(err);
        return;
      }

      Create.prototype.commit.call(self, callback);
    });
  }
};

/**
 * Will be injected...
 */
exports.app = null;

exports.create = function createMutation(option) {
  return new Create(option);
};

exports.update = function updateMutation(option) {
  return new Update(option);
};

});

/* exported NotificationHelper */
(function(window) {
  

  window.NotificationHelper = {
    getIconURI: function nh_getIconURI(app, entryPoint) {
      var icons = app.manifest.icons;

      if (entryPoint) {
        icons = app.manifest.entry_points[entryPoint].icons;
      }

      if (!icons) {
        return null;
      }

      var sizes = Object.keys(icons).map(function parse(str) {
        return parseInt(str, 10);
      });
      sizes.sort(function(x, y) { return y - x; });

      var HVGA = document.documentElement.clientWidth < 480;
      var index = sizes[HVGA ? sizes.length - 1 : 0];
      return app.installOrigin + icons[index];
    },

    // titleL10n and options.bodyL10n may be:
    // a string -> l10nId
    // an object -> {id: l10nId, args: l10nArgs}
    // an object -> {raw: string}
    send: function nh_send(titleL10n, options) {
      return new Promise(function(resolve, reject) {
        navigator.mozL10n.once(function() {
          var title = getL10n(titleL10n);

          if (options.bodyL10n) {
            options.body = getL10n(options.bodyL10n);
          }

          options.dir = navigator.mozL10n.language.direction;
          options.lang = navigator.mozL10n.language.code;

          var notification = new window.Notification(title, options);

          if (options.closeOnClick !== false) {
            notification.addEventListener('click', function nh_click() {
              notification.removeEventListener('click', nh_click);
              notification.close();
            });
          }

          resolve(notification);
        });
      });
    },
  };

  function getL10n(l10nAttrs) {
    if (typeof l10nAttrs === 'string') {
      return navigator.mozL10n.get(l10nAttrs);
    }
    if (l10nAttrs.raw) {
      return l10nAttrs.raw;
    }
    return navigator.mozL10n.get(l10nAttrs.id, l10nAttrs.args);
  }
})(this);

define("shared/notification_helper", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.NotificationHelper;
    };
}(this)));

define('debug',['require','exports','module'],function(require, exports, module) {


module.exports = function(name) {
  return function() {
    var args = Array.prototype.slice.call(arguments).map(JSON.stringify);
    args.unshift('[calendar] ');
    args.unshift(name);
    // console.log.apply(console, args);
  };
};

});



(function(window) {

  // Placeholder for storing statically generated performance timestamps,
  // similar to window.performance
  window.mozPerformance = {
    timing: {}
  };

  function dispatch(name) {
    if (!window.mozPerfHasListener) {
      return;
    }

    var now = window.performance.now();
    var epoch = Date.now();

    setTimeout(function() {
      var detail = {
        name: name,
        timestamp: now,
        epoch: epoch
      };
      var event = new CustomEvent('x-moz-perf', { detail: detail });

      window.dispatchEvent(event);
    });
  }

  [
    'moz-chrome-dom-loaded',
    'moz-chrome-interactive',
    'moz-app-visually-complete',
    'moz-content-interactive',
    'moz-app-loaded'
  ].forEach(function(eventName) {
      window.addEventListener(eventName, function mozPerfLoadHandler() {
        dispatch(eventName);
      }, false);
    });

  window.PerformanceTestingHelper = {
    dispatch: dispatch
  };

})(window);

define("shared/performance_testing_helper", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.PerformanceTestingHelper;
    };
}(this)));

define('performance',['require','exports','module','shared/performance_testing_helper'],function(require, exports) {


require('shared/performance_testing_helper');

// Helper for the performance testing events. we created
// this dedicated module since we need some "state machine" logic to avoid
// race conditions and the app contains way too many async operations during
// startup and no simple way to listen to these events.

exports._isMonthAgendaInteractive = false;
exports._isMonthReady = false;
exports._isVisuallyActive = false;
exports._isPendingReady = false;

// TODO: It would be nice if this had an events interface so I could
//     simply do performance.once('moz-app-loaded', () => ...) and
//     I would be called immediately if we were already loaded or
//     when we're loaded otherwise.
var dispatched = {};

/**
 * Performance testing events. See <https://bugzil.la/996038>.
 */
function dispatch(eventType) {
  dispatched[eventType] = true;
  window.dispatchEvent(new CustomEvent(eventType));
}

exports.isComplete = function(eventType) {
  return dispatched[eventType];
};

/**
 * Dispatch 'moz-chrome-dom-loaded' event.
 * Designates that the app's *core* chrome or navigation interface
 * exists in the DOM and is marked as ready to be displayed.
 */
exports.domLoaded = function() {
  window.performance.mark('navigationLoaded');
  // PERFORMANCE EVENT (1): moz-chrome-dom-loaded
  dispatch('moz-chrome-dom-loaded');
};

/**
 * Designates that the app's *core* chrome or navigation interface
 * has its events bound and is ready for user interaction.
 */
exports.chromeInteractive = function() {
  window.performance.mark('navigationInteractive');
  // PERFORMANCE EVENT (2): moz-chrome-interactive
  dispatch('moz-chrome-interactive');
};

/**
 * Should be called when the MonthsDayView
 * rendered all the busytimes for that day.
 */
exports.monthsDayReady = function() {
  if (exports._isMonthAgendaInteractive) {
    return;
  }

  exports._isMonthAgendaInteractive = true;
  dispatchVisuallyCompleteAndInteractive();
};

/**
 * Should be called when the month is "ready" (rendered + event listeners)
 * including the busy times indicator.
 */
exports.monthReady = function() {
  if (exports._isMonthReady) {
    return;
  }

  exports._isMonthReady = true;
  dispatchVisuallyCompleteAndInteractive();
};

/**
 * app-visually-complete and content-interactive will happen after the
 * MonthChild#activate + rendering the busy counts for the current month +
 * DayBased#_loadRecords (inherited by MonthsDayView)
 */
function dispatchVisuallyCompleteAndInteractive() {
  if (exports._isVisuallyActive ||
      !exports._isMonthAgendaInteractive ||
      !exports._isMonthReady) {
    return;
  }

  exports._isVisuallyActive = true;

  // PERFORMANCE EVENT (3): moz-app-visually-complete
  // Designates that the app is visually loaded (e.g.: all of the
  // "above-the-fold" content exists in the DOM and is marked as
  // ready to be displayed).
  window.performance.mark('visuallyLoaded');
  dispatch('moz-app-visually-complete');

  // PERFORMANCE EVENT (4): moz-content-interactive
  // Designates that the app has its events bound for the minimum
  // set of functionality to allow the user to interact with the
  // "above-the-fold" content.
  window.performance.mark('contentInteractive');
  dispatch('moz-content-interactive');

  dispatchAppLoad();
}

/**
 * Register that pending manager completed first batch of operations.
 */
exports.pendingReady = function() {
  if (exports._isPendingReady) {
    return;
  }

  exports._isPendingReady = true;
  dispatchAppLoad();
};

/**
 * App is only considered "loaded" after the MonthView and MonthDayAgenda
 * are "ready" and the first pending operations batch is completed (loading
 * events from DB and recurring events expansion).
 */
function dispatchAppLoad() {
  if (!exports._isPendingReady) {
    // to avoid race conditions (in case this is called before month view
    // is built), should not happen, but maybe in the future when IDB gets
    // faster this might be possible.
    return;
  }

  // PERFORMANCE EVENT (5): moz-app-loaded
  // Designates that the app is *completely* loaded and all relevant
  // "below-the-fold" content exists in the DOM, is marked visible,
  // has its events bound and is ready for user interaction. All
  // required startup background processing should be complete.
  window.performance.mark('fullyLoaded');
  dispatch('moz-app-loaded');
}

});


;(function(){

  /**
   * Perform initial dispatch.
   */

  var dispatch = true;

  /**
   * Base path.
   */

  var base = '';

  /**
   * Running flag.
   */

  var running;

  /**
   * Register `path` with callback `fn()`,
   * or route `path`, or `page.start()`.
   *
   *   page(fn);
   *   page('*', fn);
   *   page('/user/:id', load, user);
   *   page('/user/' + user.id, { some: 'thing' });
   *   page('/user/' + user.id);
   *   page();
   *
   * @param {String|Function} path
   * @param {Function} fn...
   * @api public
   */

  function page(path, fn) {
    // <callback>
    if ('function' == typeof path) {
      return page('*', path);
    }

    // route <path> to <callback ...>
    if ('function' == typeof fn) {
      var route = new Route(path);
      for (var i = 1; i < arguments.length; ++i) {
        page.callbacks.push(route.middleware(arguments[i]));
      }
    // show <path> with [state]
    } else if ('string' == typeof path) {
      page.show(path, fn);
    // start [options]
    } else {
      page.start(path);
    }
  }

  /**
   * Callback functions.
   */

  page.callbacks = [];

  /**
   * Get or set basepath to `path`.
   *
   * @param {String} path
   * @api public
   */

  page.base = function(path){
    if (0 == arguments.length) return base;
    base = path;
  };

  /**
   * Bind with the given `options`.
   *
   * Options:
   *
   *    - `click` bind to click events [true]
   *    - `popstate` bind to popstate [true]
   *    - `dispatch` perform initial dispatch [true]
   *
   * @param {Object} options
   * @api public
   */

  page.start = function(options){
    options = options || {};
    if (running) return;
    running = true;
    if (false === options.dispatch) dispatch = false;
    if (false !== options.popstate) window.addEventListener('popstate', onpopstate, false);
    if (false !== options.click) window.addEventListener('click', onclick, false);
    if (!dispatch) return;
    page.replace(location.pathname + location.search, null, true, dispatch);
  };

  /**
   * Unbind click and popstate event handlers.
   *
   * @api public
   */

  page.stop = function(){
    running = false;
    removeEventListener('click', onclick, false);
    removeEventListener('popstate', onpopstate, false);
  };

  /**
   * Show `path` with optional `state` object.
   *
   * @param {String} path
   * @param {Object} state
   * @return {Context}
   * @api public
   */

  page.show = function(path, state){
    var ctx = new Context(path, state);
    page.dispatch(ctx);
    if (!ctx.unhandled) ctx.pushState();
    return ctx;
  };

  /**
   * Replace `path` with optional `state` object.
   *
   * @param {String} path
   * @param {Object} state
   * @return {Context}
   * @api public
   */

  page.replace = function(path, state, init, dispatch){
    var ctx = new Context(path, state);
    ctx.init = init;
    if (null == dispatch) dispatch = true;
    if (dispatch) page.dispatch(ctx);
    ctx.save();
    return ctx;
  };

  /**
   * Dispatch the given `ctx`.
   *
   * @param {Object} ctx
   * @api private
   */

  page.dispatch = function(ctx){
    var i = 0;

    function next() {
      var fn = page.callbacks[i++];
      if (!fn) return unhandled(ctx);
      fn(ctx, next);
    }

    next();
  };

  /**
   * Unhandled `ctx`. When it's not the initial
   * popstate then redirect. If you wish to handle
   * 404s on your own use `page('*', callback)`.
   *
   * @param {Context} ctx
   * @api private
   */

  function unhandled(ctx) {
    if (window.location.pathname + window.location.search == ctx.canonicalPath) return;
    page.stop();
    ctx.unhandled = true;
    window.location = ctx.canonicalPath;
  }

  /**
   * Initialize a new "request" `Context`
   * with the given `path` and optional initial `state`.
   *
   * @param {String} path
   * @param {Object} state
   * @api public
   */

  function Context(path, state) {
    if ('/' == path[0] && 0 != path.indexOf(base)) path = base + path;
    var i = path.indexOf('?');
    this.canonicalPath = path;
    this.path = path.replace(base, '') || '/';
    this.title = document.title;
    this.state = state || {};
    this.state.path = path;
    this.querystring = ~i ? path.slice(i + 1) : '';
    this.pathname = ~i ? path.slice(0, i) : path;
    this.params = [];
  }

  /**
   * Expose `Context`.
   */

  page.Context = Context;

  /**
   * Push state.
   *
   * @api private
   */

  Context.prototype.pushState = function(){
    history.pushState(this.state, this.title, this.canonicalPath);
  };

  /**
   * Save the context state.
   *
   * @api public
   */

  Context.prototype.save = function(){
    history.replaceState(this.state, this.title, this.canonicalPath);
  };

  /**
   * Initialize `Route` with the given HTTP `path`,
   * and an array of `callbacks` and `options`.
   *
   * Options:
   *
   *   - `sensitive`    enable case-sensitive routes
   *   - `strict`       enable strict matching for trailing slashes
   *
   * @param {String} path
   * @param {Object} options.
   * @api private
   */

  function Route(path, options) {
    options = options || {};
    this.path = path;
    this.method = 'GET';
    this.regexp = pathtoRegexp(path
      , this.keys = []
      , options.sensitive
      , options.strict);
  }

  /**
   * Expose `Route`.
   */

  page.Route = Route;

  /**
   * Return route middleware with
   * the given callback `fn()`.
   *
   * @param {Function} fn
   * @return {Function}
   * @api public
   */

  Route.prototype.middleware = function(fn){
    var self = this;
    return function(ctx, next){
      if (self.match(ctx.path, ctx.params)) return fn(ctx, next);
      next();
    }
  };

  /**
   * Check if this route matches `path`, if so
   * populate `params`.
   *
   * @param {String} path
   * @param {Array} params
   * @return {Boolean}
   * @api private
   */

  Route.prototype.match = function(path, params){
    var keys = this.keys
      , qsIndex = path.indexOf('?')
      , pathname = ~qsIndex ? path.slice(0, qsIndex) : path
      , m = this.regexp.exec(pathname);
  
    if (!m) return false;

    for (var i = 1, len = m.length; i < len; ++i) {
      var key = keys[i - 1];

      var val = 'string' == typeof m[i]
        ? decodeURIComponent(m[i])
        : m[i];

      if (key) {
        params[key.name] = undefined !== params[key.name]
          ? params[key.name]
          : val;
      } else {
        params.push(val);
      }
    }

    return true;
  };

  /**
   * Normalize the given path string,
   * returning a regular expression.
   *
   * An empty array should be passed,
   * which will contain the placeholder
   * key names. For example "/user/:id" will
   * then contain ["id"].
   *
   * @param  {String|RegExp|Array} path
   * @param  {Array} keys
   * @param  {Boolean} sensitive
   * @param  {Boolean} strict
   * @return {RegExp}
   * @api private
   */

  function pathtoRegexp(path, keys, sensitive, strict) {
    if (path instanceof RegExp) return path;
    if (path instanceof Array) path = '(' + path.join('|') + ')';
    path = path
      .concat(strict ? '' : '/?')
      .replace(/\/\(/g, '(?:/')
      .replace(/\+/g, '__plus__')
      .replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?/g, function(_, slash, format, key, capture, optional){
        keys.push({ name: key, optional: !! optional });
        slash = slash || '';
        return ''
          + (optional ? '' : slash)
          + '(?:'
          + (optional ? slash : '')
          + (format || '') + (capture || (format && '([^/.]+?)' || '([^/]+?)')) + ')'
          + (optional || '');
      })
      .replace(/([\/.])/g, '\\$1')
      .replace(/__plus__/g, '(.+)')
      .replace(/\*/g, '(.*)');
    return new RegExp('^' + path + '$', sensitive ? '' : 'i');
  };

  /**
   * Handle "populate" events.
   */

  function onpopstate(e) {
    if (e.state) {
      var path = e.state.path;
      page.replace(path, e.state);
    }
  }

  /**
   * Handle "click" events.
   */

  function onclick(e) {
    if (1 != which(e)) return;
    if (e.defaultPrevented) return;
    var el = e.target;
    while (el && 'A' != el.nodeName) el = el.parentNode;
    if (!el || 'A' != el.nodeName) return;
    var href = el.href;
    var path = el.pathname + el.search;
    if (el.hash || '#' == el.getAttribute('href')) return;
    if (!sameOrigin(href)) return;
    var orig = path;
    path = path.replace(base, '');
    if (base && orig == path) return;
    e.preventDefault();
    page.show(orig);
  }

  /**
   * Event button.
   */

  function which(e) {
    e = e || window.event;
    return null == e.which
      ? e.button
      : e.which;
  }

  /**
   * Check if `href` is the same origin.
   */

  function sameOrigin(href) {
    var origin = location.protocol + '//' + location.hostname;
    if (location.port) origin += ':' + location.port;
    return 0 == href.indexOf(origin);
  }

  /**
   * Expose `page`.
   */

  if ('undefined' == typeof module) {
    window.page = page;
  } else {
    module.exports = page;
  }

})();

define("ext/page", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.page;
    };
}(this)));

/* jshint loopfunc: true */
define('router',['require','exports','module','ext/page'],function(require, exports, module) {


var COPY_METHODS = ['start', 'stop', 'show'];

var page = require('ext/page');

function Router() {
  var i = 0;
  var len = COPY_METHODS.length;

  this.page = page;
  this._activeObjects = [];

  for (; i < len; i++) {
    this[COPY_METHODS[i]] = page[COPY_METHODS[i]].bind(page);
  }

  this._lastState = this._lastState.bind(this);
}

Router.prototype = {

  go: function(path, context) {
    this.show(path, context);
  },

  /**
   * Tells router to manage the object.
   * This will call the 'onactive'
   * method if present on the object.
   *
   * When the route is changed all 'manged'
   * object will be cleared and 'oninactive'
   * will be fired.
   */
  mangeObject: function() {
    var args = Array.prototype.slice.call(arguments);
    var object = args.shift();

    this._activeObjects.push(object);
    // intentionally using 'in'
    if ('onactive' in object) {
      object.onactive.apply(object, args);
    }
  },

  /**
   * Clears active objects, calls oninactive
   * on object if available.
   */
  clearObjects: function() {
    var item;
    while ((item = this._activeObjects.pop())) {
      // intentionally using 'in'
      if ('oninactive' in item) {
        item.oninactive();
      }
    }
  },

  /**
   * This method serves two purposes.
   *
   * 1. to safely end the loop by _not_ calling next.
   * 2. to store the last location.
   *
   * This function is added to the end of every rule.
   */
  _lastState: function(ctx) {
    this.last = ctx;
  },

  resetState: function() {
    if (!this.currentPath) {
      this.currentPath = '/month/';
    }

    this.show(this.currentPath);
  },

  /**
   * Adds a route that represents a state of the page.
   * The theory is that a page can only enter
   * one state at a time (basically yield control to some
   * view or other object).
   *
   * Modifiers can be used to alter the behaviour
   * of a given state (without exiting it)
   *
   * @param {String} path path as defined by page.js.
   * @param {String|Array} one or multiple view identifiers.
   * @param {Object} options (clear, path).
   */
  state: function(path, views, options) {

    options = options || {};
    if (!Array.isArray(views)) {
      views = [views];
    }

    var self = this;
    var viewObjs = [];

    function loadAllViews(ctx, next) {
      var len = views.length;
      var numViews = len;
      var i;

      // Reset our views
      viewObjs = [];

      /*jshint loopfunc: true */
      for (i = 0; i < numViews; i++) {
        self.app.view(views[i], function(view) {
          viewObjs.push(view);
          len--;

          if (!len) {
            next();
          }
        });
      }
    }

    function setPath(ctx, next) {
      // Only set the dataset path after the view has loaded
      // its resources. Otherwise, there is some flash and
      // jank while styles start to apply and the view is only
      // partially loaded.
      if (options.path !== false) {
        document.body.dataset.path = ctx.canonicalPath;
        // Need to trigger the DOM to accept the new style
        // right away. Otherwise, once manageObject is called,
        // any styles/animations it triggers may be delayed
        // or dropped as the browser coalesces style changes
        // into one visible change. Example is the settings
        // drawer animation getting chopped so it is not smooth.
        document.body.clientWidth;
      }
      next();
    }

    function handleViews(ctx, next) {

      // Clear views if needed
      if (options.clear !== false) {
        self.clearObjects();
      }

      // Activate objects
      for (var i = 0; i < viewObjs.length; i++) {
        self.mangeObject(viewObjs[i], ctx);
      }

      // Set the current path
      if (options.appPath !== false) {
        self.currentPath = ctx.canonicalPath;
        var evt = new CustomEvent("page-changed", {
          detail: {
            page: self.currentPath
          },
          bubbles: true,
          cancelable: false
        });
        window.dispatchEvent(evt);
      }

      next();
    }

    this.page(path, loadAllViews, setPath, handleViews, this._lastState);
  },

  /**
   * Adds a modifier route
   * Modifiers are essentially views, without the currentPath updating
   */
  modifier: function(path, view, options) {
    options = options || {};
    options.appPath = false;
    options.clear = false;
    this.state(path, view, options);
  }
};

// router is singleton to simplify dependency graph, specially since it's
// needed by notifications and it could get into weird race conditions
module.exports = new Router();

});

/* global Notification */
define('notification',['require','exports','module','shared/notification_helper','debug','performance','router'],function(require, exports, module) {


var NotificationHelper = require('shared/notification_helper');
var debug = require('debug')('notification');
var performance = require('performance');
var router = require('router');

var cachedSelf;

exports.sendNotification = function(title, body, url) {
  return getSelf().then(app => {
    if (!app) {
      // This is perhaps a test environment?
      debug('mozApps.getSelf gave us lemons!');
      return Promise.resolve();
    }

    var icon = NotificationHelper.getIconURI(app);
    icon += '?';
    icon += url;
    var notification = new Notification(title, {
      body: body,
      icon: icon,
      // we use the URL as the ID so we display a single notification for each
      // busytime (it will override previous notifications)
      tag: url
    });
    return new Promise((resolve, reject) => {
      notification.onshow = resolve;
      notification.onerror = reject;
      notification.onclick = function() {
        launch(url);
      };
    });
  });
};

/**
 * Bug 987458 - Multipe requests to mozApps.getSelf will fail if fired
 *     in close succession. Therefore we must make sure to only ever fire
 *     a single request to getSelf.
 */
function getSelf() {
  if (!cachedSelf) {
    cachedSelf = new Promise((resolve, reject) => {
      var request = navigator.mozApps.getSelf();

      request.onsuccess = (event) => {
        resolve(event.target.result);
      };

      request.onerror = () => {
        reject(new Error('mozApps.getSelf failed!'));
      };
    });
  }

  return cachedSelf;
}

/**
 * Start the calendar app and open the url.
 */
function launch(url) {
  // we close all the notifications for the same busytime when we launch the
  // app; we do it like this to make sure we use the same codepath for cases
  // where notification was handled by mozSetMessageHandler or by the
  // Notification instance onclick listener (Bug 1132336)
  closeNotifications(url);

  if (performance.isComplete('moz-app-loaded')) {
    return foreground(url);
  }

  // If we're not fully loaded, wait for that to happen to foreground
  // ourselves and navigate to the target url so the user
  // experiences less flickering.
  window.addEventListener('moz-app-loaded', function onMozAppLoaded() {
    window.removeEventListener('moz-app-loaded', onMozAppLoaded);
    return foreground(url);
  });
}
exports.launch = launch;

// Bring ourselves to the foreground at some url.
function foreground(url) {
  return getSelf().then(app => {
    router.go(url);
    return app && app.launch();
  });
}

function closeNotifications(url) {
  Notification.get({ tag: url }).then(notifications => {
    notifications.forEach(n => n.close());
  });
}

exports.closeNotifications = closeNotifications;

});

define('provider/local',['require','exports','module','./abstract','event_mutations','ext/uuid','notification'],function(require, exports, module) {


var Abstract = require('./abstract');
var mutations = require('event_mutations');
var uuid = require('ext/uuid');
var notification = require('notification');

var LOCAL_CALENDAR_ID = 'local-first';

function Local() {
  Abstract.apply(this, arguments);

  // TODO: Get rid of this when app global is gone.
  mutations.app = this.app;
  this.events = this.app.store('Event');
  this.busytimes = this.app.store('Busytime');
  this.alarms = this.app.store('Alarm');
}
module.exports = Local;

Local.calendarId = LOCAL_CALENDAR_ID;

/**
 * Returns the details for the default calendars.
 */
Local.defaultCalendar = function() {
  // XXX: Make async
  var l10nId = 'calendar-local';
  var name;

  if ('mozL10n' in window.navigator) {
    name = window.navigator.mozL10n.get(l10nId);
    if (name === l10nId) {
      name = null;
    }
  }

  if (!name) {
    name = 'Offline';
  }

  return {
    // XXX localize this name somewhere
    name: name,
    id: LOCAL_CALENDAR_ID,
    color: Local.prototype.defaultColor
  };

};

Local.prototype = {
  __proto__: Abstract.prototype,

  canExpandRecurringEvents: false,

  getAccount: function(account, callback) {
    callback(null, {});
  },

  findCalendars: function(account, callback) {
    var list = {};
    list[LOCAL_CALENDAR_ID] = Local.defaultCalendar();
    callback(null, list);
  },

  syncEvents: function(account, calendar, cb) {
    cb(null);
  },

  /**
   * @return {Calendar.EventMutations.Create} mutation object.
   */
  createEvent: function(event, busytime, callback) {
    // most providers come with their own built in
    // id system when creating a local event we need to generate
    // our own UUID.
    if (!event.remote.id) {
      // TOOD: uuid is provided by ext/uuid.js
      //       if/when platform supports a safe
      //       random number generator (values never conflict)
      //       we can use that instead of uuid.js
      event.remote.id = uuid();
    }

    var create = mutations.create({ event: event });
    create.commit(function(err) {
      if (err) {
        return callback(err);
      }

      callback(null, create.busytime, create.event);
    });

    return create;
  },

  deleteEvent: function(event, busytime, callback) {
    if (typeof(busytime) === 'function') {
      callback = busytime;
      busytime = null;
    }
    this.app.store('Event').remove(event._id, callback);

    if (busytime) {
      this.app.store('Busytime').remove(busytime._id, function(err) {
        if (err) {
          return;
        }

        var url = '/alarm-display/' + busytime._id;
        notification.closeNotifications(url);
      });
    }
  },

  /**
   * @return {Calendar.EventMutations.Update} mutation object.
   */
  updateEvent: function(event, busytime, callback) {
    if (typeof(busytime) === 'function') {
      callback = busytime;
      busytime = null;
    }

    var update = mutations.update({ event: event });
    update.commit(function(err) {
      if (err) {
        return callback(err);
      }

      callback(null, update.busytime, update.event);
    });

    if (busytime) {
      var url = '/alarm-display/' + busytime._id;
      notification.closeNotifications(url);
    }

    return update;
  }
};

});

// this code is from test-agent might use native dom events
// or something else in the future to replace this.
define('responder',['require','exports','module'],function(require, exports, module) {


/**
 * @param {Object} list of events to add onto responder.
 */
function Responder(events) {
  this._$events = Object.create(null);

  // Buffer for emitWhenListener that hangs onto events dispatched for topics
  // no one is currently listening for. Inspired by email's function
  // of the same name.
  this.buffer = {};

  if (typeof(events) !== 'undefined') {
    this.addEventListener(events);
  }
}
module.exports = Responder;

/**
 * Stringifies request to websocket
 *
 *
 * @param {String} command command name.
 * @param {Object} data object to be sent over the wire.
 * @return {String} json object.
 */
Responder.stringify = function stringify(command, data) {
  return JSON.stringify([command, data]);
};

/**
 * Parses request from WebSocket.
 *
 * @param {String} json json string to translate.
 * @return {Object} ex: { event: 'test', data: {} }.
 */
Responder.parse = function parse(json) {
  var data;
  try {
    data = (json.forEach) ? json : JSON.parse(json);
  } catch (e) {
    throw new Error('Could not parse json: "' + json + '"');
  }

  return data;
};

Responder.prototype = {
  parse: Responder.parse,
  stringify: Responder.stringify,

  /**
   * Events on this instance
   *
   * @type Object
   */
  events: null,

  /**
   * Recieves json string event and dispatches an event.
   *
   * @param {String|Object} json data object to respond to.
   * @param {String} json.event event to emit.
   * @param {Object} json.data data to emit with event.
   * @param {Object} [params] option number of params to pass to emit.
   * @return {Object} result of WebSocketCommon.parse.
   */
  respond: function respond(json) {
    var event = Responder.parse(json);
    var args = Array.prototype.slice.call(arguments).slice(1);
    this.emit.apply(this, event.concat(args));
    return event;
  },

  /**
   * Adds an event listener to this object.
   *
   * @param {String} type event name.
   * @param {Function} callback event callback.
   */
  addEventListener: function addEventListener(type, callback) {
    var event;

    if (typeof(callback) === 'undefined' && typeof(type) === 'object') {
      for (event in type) {
        if (type.hasOwnProperty(event)) {
          this.addEventListener(event, type[event]);
        }
      }

      return this;
    }

    if (!(type in this._$events)) {
      this._$events[type] = [];
    }

    this._$events[type].push(callback);
    return this.flushTopicBuffer(type);
  },

  /**
   * Adds an event listener which will
   * only fire once and then remove itself.
   *
   *
   * @param {String} type event name.
   * @param {Function} callback fired when event is emitted.
   */
  once: function once(type, callback) {
    var self = this;
    function onceCb() {
      /*jshint validthis:true */
      self.removeEventListener(type, onceCb);
      callback.apply(this, arguments);
    }

    this.addEventListener(type, onceCb);
    return this.flushTopicBuffer(type);
  },

  flushTopicBuffer: function flushTopicBuffer(topic) {
    if (!(topic in this.buffer)) {
      // Nothing to flush.
      return this;
    }

    this.buffer[topic].forEach(args => {
      args.unshift(topic);
      this.emit.apply(this, args);
    });

    return this;
  },

  /**
   * Emits an event.
   *
   * Accepts any number of additional arguments to pass unto
   * event listener.
   *
   * @param {String} eventName name of the event to emit.
   * @param {Object} [arguments] additional arguments to pass.
   */
  emit: function emit() {
    var args = Array.prototype.slice.call(arguments),
        event = args.shift(),
        eventList,
        self = this;

    if (event in this._$events) {
      eventList = this._$events[event];

      eventList.forEach(function(callback) {
        if (typeof(callback) === 'object' && callback.handleEvent) {
          callback.handleEvent({ type: event, data: args });
        } else {
          callback.apply(self, args);
        }
      });
    }

    return this;
  },

  emitWhenListener: function emitWhenListener() {
    var args = Array.prototype.slice.call(arguments);
    var event = args.shift();

    if (event in this._$events && this._$events[event].length) {
      // Someone is already listening for this event, so this is just
      // a regular old emit.
      return this.emit.apply(this, arguments);
    }

    if (!(event in this.buffer)) {
      this.buffer[event] = [];
    }

    // Now just push the call info onto the topic buffer.
    this.buffer[event].push(args);
    return this;
  },

  /**
   * Removes all event listeners for a given event type
   *
   *
   * @param {String} event event type to remove.
   */
  removeAllEventListeners: function removeAllEventListeners(name) {
    if (name in this._$events) {
      //reuse array
      this._$events[name].length = 0;
    }

    return this;
  },

  /**
   * Removes a single event listener from a given event type
   * and callback function.
   *
   *
   * @param {String} eventName event name.
   * @param {Function} callback same instance of event handler.
   */
  removeEventListener: function removeEventListener(name, callback) {
    var i, length, events;

    if (!(name in this._$events)) {
      return false;
    }

    events = this._$events[name];

    for (i = 0, length = events.length; i < length; i++) {
      if (events[i] && events[i] === callback) {
        events.splice(i, 1);
        return true;
      }
    }

    return false;
  }

};

Responder.prototype.on = Responder.prototype.addEventListener;
Responder.prototype.off = Responder.prototype.removeEventListener;

});

define('store/abstract',['require','exports','module','responder','promise','next_tick'],function(require, exports, module) {


var Responder = require('responder');
var denodeifyAll = require('promise').denodeifyAll;
var nextTick = require('next_tick');

/**
 * Creates an abstract store instance.
 * Every store must contain a reference
 * to the database.
 */
function Abstract(db, app) {
  this.db = db;
  this.app = app;
  this._cached = Object.create(null);
  Responder.call(this);

  denodeifyAll(this, [
    'persist',
    'all',
    '_allCached',
    'removeByIndex',
    'get',
    'remove',
    'count'
  ]);
}
module.exports = Abstract;

Abstract.prototype = {
  __proto__: Responder.prototype,

  _store: null,

  /**
   * Stores that will need to be removed
   * when a record is removed from this store.
   *
   * @type {Array}
   */
  _dependentStores: null,

  _createModel: function(object, id) {
    if (typeof(id) !== 'undefined') {
      object._id = id;
    }

    return object;
  },

  _addToCache: function(object) {
    this._cached[object._id] = object;
  },

  _removeFromCache: function(id) {
    if (id in this._cached) {
      delete this._cached[id];
    }
  },

  _transactionCallback: function(trans, callback) {
    if (callback) {
      trans.addEventListener('error', function(e) {
        callback(e);
      });

      trans.addEventListener('complete', function() {
        callback(null);
      });
    }
  },

  /**
   * Adds an account to the database.
   *
   * @param {Object} object reference to account object to store.
   * @param {IDBTransaction} trans transaction.
   * @param {Function} callback node style callback.
   */
  persist: function(object, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = undefined;
    }

    if (!trans) {
      trans = this.db.transaction(
        this._dependentStores || this._store,
        'readwrite'
      );
    }

    var self = this;
    var store = trans.objectStore(this._store);
    var data = this._objectData(object);
    var id;
    var model;

    var putReq;
    var reqType = this._detectPersistType(object);

    // determine type of event
    if (reqType === 'update') {
      putReq = store.put(data);
    } else {
      this._assignId(data);
      putReq = store.add(data);
    }

    trans.addEventListener('error', function(event) {
      if (callback) {
        callback(event);
      }
    });

    this._addDependents(object, trans);

    // when we have the id we can add the model to the cache.
    if (data._id) {
      id = data._id;
      model = self._createModel(object, id);
      self._addToCache(model);
    }

    trans.addEventListener('complete', function(data) {
      if (!model) {
        id = putReq.result;
        model = self._createModel(object, id);
        self._addToCache(model);
      }

      if (callback) {
        callback(null, id, model);
      }

      self.emit(reqType, id, model);
      self.emit('persist', id, model);
    });
  },

  _allCached: function(callback) {
    var list = this._cached;
    nextTick(function() {
      callback(null, list);
    });
  },

  /**
   * Loads all records in the database
   * for this store.
   *
   * Using this function will fill
   * the cache with all records in the store.
   * As such this should only be used once
   * during the app life-cycle.
   */
  all: function(callback) {
    if (this._allCallbacks) {
      this._allCallbacks.push(callback);
      return;
    }

    // list of pending callbacks
    this._allCallbacks = [callback];

    var self = this;
    var trans = this.db.transaction(this._store);
    var store = trans.objectStore(this._store);

    function process(data) {
      return self._addToCache(self._createModel(data));
    }

    function fireQueue(err, value) {
      var callback;
      while ((callback = self._allCallbacks.shift())) {
        callback(err, value);
      }
    }

    store.mozGetAll().onsuccess = function(event) {
      event.target.result.forEach(process);
    };

    trans.onerror = function(event) {
      fireQueue(event.target.error.name);
    };

    trans.oncomplete = function() {
      fireQueue(null, self._cached);
      self.all = self._allCached;
    };
  },

  _addDependents: function() {},
  _removeDependents: function(trans) {},

  _detectPersistType: function(object) {
    return ('_id' in object) ? 'update' : 'add';
  },

  _parseId: function(id) {
    return id;
  },

  _assignId: function(obj) {
  },

  /**
   * Removes all records a index value and removes
   * them from the cache. 'remove' events are *not* emitted
   * when removing in this manner for performance reasons.
   *
   * TODO: the test for this method still lives in the event store tests
   *       where this code began should refactor those tests to be general
   *       and live in the abstract tests.
   *
   * @param {String} indexName name of store index.
   * @param {Numeric} indexValue value in index.
   * @param {IDBTransation} [trans] optional transaction to reuse.
   * @param {Function} [callback] optional callback to use.
   *                   When called without a transaction chances
   *                   are you should pass a callback.
   */
  removeByIndex: function(indexName, indexValue, trans, callback) {
    var self = this;
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = undefined;
    }

    if (!trans) {
      trans = this.db.transaction(
        this._dependentStores || this._store,
        'readwrite'
      );
    }
    if (callback) {

      trans.addEventListener('complete', function() {
        callback(null);
      });

      trans.addEventListener('error', function(event) {
        callback(event);
      });
    }


    var index = trans.objectStore(this._store).index(indexName);
    var req = index.openCursor(
      IDBKeyRange.only(indexValue)
    );

    req.onsuccess = function(event) {
      var cursor = event.target.result;
      if (cursor) {
        // remove deps first intentionally to mimic, removes normal behaviour
        self._removeDependents(cursor.primaryKey, trans);
        self._removeFromCache(cursor.primaryKey);
        cursor.delete();
        cursor.continue();
      }
    };

    return req;
  },

  /**
   * Finds a single record.
   *
   * Does not go through any cache or emit any events.
   *
   * @param {String} id id of record.
   * @param {IDBTransaction} [trans] optional transaction.
   * @param {Function} callback node style [err, record].
   */
  get: function(id, trans, callback) {
    var self = this;

    if (typeof(trans) === 'function') {
      callback = trans;
      trans = null;
    }

    if (!trans) {
      trans = this.db.transaction(this._store);
    }

    var store = trans.objectStore(this._store);
    var req = store.get(this._parseId(id));

    req.onsuccess = function() {
      var model;

      if (req.result) {
        model = self._createModel(req.result);
      }

      callback(null, model);
    };

    req.onerror = function(event) {
      callback(event);
    };
  },

  /**
   * Removes a object from the store.
   *
   * @param {String} id record reference.
   * @param {IDBTransaction} trans transaction.
   * @param {Function} callback node style callback.
   */
  remove: function(id, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = undefined;
    }

    if (!trans) {
      trans = this.db.transaction(
        this._dependentStores || this._store,
        'readwrite'
      );
    }

    var self = this;
    var store = trans.objectStore(this._store);
    id = this._parseId(id);

    store.delete(id);

    this._removeDependents(id, trans);
    self.emit('preRemove', id);

    trans.addEventListener('error', function(event) {
      if (callback) {
        callback(event);
      }
    });

    trans.addEventListener('complete', function() {
      if (callback) {
        callback(null, id);
      }

      self.emit('remove', id);

      // intentionally after the callbacks...
      self._removeFromCache(id);
    });
  },

  /**
   * Find number of records in store.
   *
   * @param {Function} callback node style err/count.
   */
  count: function(callback) {
    var trans = this.db.transaction(this._store);
    var store = trans.objectStore(this._store);

    var req = store.count();

    req.onsuccess = function() {
      callback(null, req.result);
    };

    req.onerror = function(e) {
      callback(e);
    };
  },

  _objectData: function(data) {
    if ('toJSON' in data) {
      return data.toJSON();
    }
    return data;
  }
};

});

define('extend',['require','exports','module'],function(require, exports, module) {


module.exports = function(target, input) {
  for (var key in input) {
    if (hasOwnProperty.call(input, key)) {
      target[key] = input[key];
    }
  }

  return target;
};

});

define('probably_parse_int',['require','exports','module'],function(require, exports, module) {


var NUMERIC = /^[0-9]+$/;

/**
 * @param {number|string} id Some id.
 */
module.exports = function(id) {
  // by an unfortunate decision we have both
  // string ids and number ids.. based on the
  // input we run parseInt
  if (typeof id === 'string' && id.match(NUMERIC)) {
    return parseInt(id, 10);
  }

  return id;
};

});

define('error',['require','exports','module'],function(require, exports, module) {


/**
 * These errors are _not_ exceptions and are designed to be passed not thrown
 * in typical |throw new X| fashion.
 */
function Base(name, detail) {
  this.message = 'oops... why did you throw this?';
  this.name = name;
  this.detail = detail;
}
module.exports = Base;

Base.prototype = Object.create(Error.prototype);

function errorFactory(name, l10nID) {
  var error = function(detail) {
    this.name = name;
    this.detail = detail;
    /**
     * we need to use l10nID's for backwards compatibility
     * (not changing string IDs between releases).
     */
    this.l10nID = l10nID || name;
  };

  error.prototype = Object.create(Base.prototype);
  return error;
}

/* note names should _never_ change */
Base.Authentication = errorFactory('authentication', 'error-unauthenticated');
Base.InvalidServer = errorFactory('invalid-server', 'error-internal-server-error');
Base.ServerFailure = errorFactory('server-failure', 'error-internal-server-error');
Base.CaldavUnknownError = errorFactory('caldav-unknown', 'error-unkown');

});

define('provider/caldav_pull_events',['require','exports','module','calc','debug','ext/uuid'],function(require, exports, module) {


var Calc = require('calc');
var debug = require('debug')('pull events');
var uuid = require('ext/uuid');

/**
 * Event synchronization class for caldav provider.
 *
 * Options:
 *  - app: current calendar app
 *  - account: (Calendar.Models.Account) required
 *  - calendar: (Calendar.Models.Calendar) required
 *
 * Example:
 *
 *    // instance of a service stream
 *    var stream;
 *
 *    var pull = new Calendar.Provider.CaldavPullEvents(stream, {
 *      calendar: calendarModel,
 *      account: accountModel,
 *      app: Calendar.App
 *    });
 *
 *    stream.request(function() {
 *      // stream is complete here the audit of
 *      // events can be made. They are flushed
 *      // to the cache where possible but not actually
 *      // persisted in the database.
 *
 *      // assuming we are ready commit the changes
 *      pull.commit(function(err) {
 *        // all changes have been committed at this point.
 *      });
 *    });
 *
 * @param {Calendar.Responder} stream event emitter usually
 *                                    a service stream.
 * @param {Object} options options for instance (see above).
 */
function PullEvents(stream, options) {
  if (options.calendar) {
    this.calendar = options.calendar;
  } else {
    throw new Error('.calendar option must be given');
  }

  if (options.account) {
    this.account = options.account;
  } else {
    throw new Error('.account option must be given');
  }

  this.app = options.app;

  stream.on('event', this);
  stream.on('component', this);
  stream.on('occurrence', this);
  stream.on('missingEvents', this);

  this.icalQueue = [];
  this.eventQueue = [];
  this.busytimeQueue = [];
  this.alarmQueue = [];

  this._busytimeStore = this.app.store('Busytime');

  // Catch account events to watch for mid-sync removal
  this._accountStore = this.app.store('Account');
  this._accountStore.on('remove', this._onRemoveAccount.bind(this));

  this._aborted = false;
  this._trans = null;
}
module.exports = PullEvents;

PullEvents.prototype = {
  eventQueue: null,
  busytimeQueue: null,

  /**
   * Get db id for busytime.
   *
   * @param {Object} busytime service sent busytime.
   */
  busytimeIdFromRemote: function(busytime) {
    var eventId = this.eventIdFromRemote(busytime, !busytime.isException);

    return busytime.start.utc + '-' +
           busytime.end.utc + '-' +
           eventId;
  },

  /**
   * Get db id for event.
   *
   * @param {Object} event service sent event or '.remote'
   *                       property in db event.
   *
   * @param {Boolean} ignoreException when true will ignore
   *                                  recurrence exceptions.
   *
   * @return {String} database object id.
   */
  eventIdFromRemote: function(event, ignoreException) {
    var id = event.eventId || event.id;

    if (!ignoreException && event.recurrenceId) {
      id += '-' + event.recurrenceId.utc;
    }

    return this.calendar._id + '-' + id;
  },

  /**
   * Format an incoming event.
   *
   * @param {Object} event service sent event.
   */
  formatEvent: function(event) {
    // get id or parent id we ignore the exception
    // rules here so children (exceptions) can lookup
    // their parents id.
    var id = this.eventIdFromRemote(event, true);

    var result = Object.create(null);
    result.calendarId = this.calendar._id;
    result.remote = event;

    if (event.recurrenceId) {
      result.parentId = id;
      // don't ignore the exceptions
      result._id = this.eventIdFromRemote(event);
    } else {
      result._id = id;
    }

    return result;
  },

  /**
   * Formats and tags busytime sent from service.
   *
   * @param {Object} time service sent busytime.
   */
  formatBusytime: function(time) {
    var eventId = this.eventIdFromRemote(time, !time.isException);
    var id = eventId + '-' + uuid.v4();
    var calendarId = this.calendar._id;

    time._id = id;
    time.calendarId = calendarId;
    time.eventId = eventId;

    if (time.alarms) {
      var i = 0;
      var len = time.alarms.length;
      var alarm;

      for (; i < len; i++) {
        alarm = time.alarms[i];
        alarm.eventId = eventId;
        alarm.busytimeId = id;
      }
    }

    return this._busytimeStore.initRecord(time);
  },

  handleOccurrenceSync: function(item) {
    var alarms;

    if ('alarms' in item) {
      alarms = item.alarms;
      delete item.alarms;

      if (alarms.length) {
        var i = 0;
        var len = alarms.length;
        var now = Date.now();

        for (; i < len; i++) {
          var alarm = {
            startDate: {},
            eventId: item.eventId,
            busytimeId: item._id
          };

          // Copy the start object
          for (var j in item.start) {
            alarm.startDate[j] = item.start[j];
          }
          alarm.startDate.utc += (alarms[i].trigger * 1000);

          var alarmDate = Calc.dateFromTransport(item.end);
          if (alarmDate.valueOf() < now) {
            continue;
          }
          this.alarmQueue.push(alarm);
        }
      }
    }

    this.busytimeQueue.push(item);
  },

  handleComponentSync: function(component) {
    component.eventId = this.eventIdFromRemote(component);
    component.calendarId = this.calendar._id;

    if (!component.lastRecurrenceId) {
      delete component.lastRecurrenceId;
    }

    this.icalQueue.push(component);
  },

  handleEventSync: function(event) {
    var exceptions = event.remote.exceptions;
    delete event.remote.exceptions;

    var id = event._id;

    // clear any busytimes that could possibly be
    // related to this event as we will be adding new
    // ones as part of the sync.
    this._busytimeStore.removeEvent(id);

    this.eventQueue.push(event);

    var component = event.remote.icalComponent;
    delete event.remote.icalComponent;

    // don't save components for exceptions.
    // the parent has the ical data.
    if (!event.remote.recurrenceId) {
      this.icalQueue.push({
        data: component,
        eventId: event._id
      });
    }

    if (exceptions) {
      for (var i = 0; i < exceptions.length; i++) {
        this.handleEventSync(this.formatEvent(exceptions[i]));
      }
    }
  },

  /**
   * Account removal event handler. Aborts the rest of sync processing, if
   * the account deleted is the subject of the current sync.
   *
   * @param {String} database object id.
   */
  _onRemoveAccount: function(id) {
    if (id === this.account._id) {
      // This is our account, so abort the sync.
      this.abort();
    }
  },

  /**
   * Abort the sync. After this, further events will be ignored and commit()
   * will do nothing.
   */
  abort: function() {
    if (this._aborted) {
      // Bail, if already aborted.
      return;
    }
    // Flag that the sync should be aborted.
    this._aborted = true;
    if (this._trans) {
      // Attempt to abort the in-progress commit transaction
      this._trans.abort();
    }
  },

  handleEvent: function(event) {
    if (this._aborted) {
      // Ignore all events, if the sync has been aborted.
      return;
    }

    var data = event.data;

    switch (event.type) {
      case 'missingEvents':
        this.removeList = data[0];
        break;
      case 'occurrence':
        var occur = this.formatBusytime(data[0]);
        this.handleOccurrenceSync(occur);
        break;
      case 'component':
        this.handleComponentSync(data[0]);
        break;
      case 'event':
        var e = this.formatEvent(data[0]);
        this.handleEventSync(e);
        break;
    }
  },

  /**
   * Commit all pending records.
   *
   *
   * @param {IDBTransaction} [trans] optional transaction.
   * @param {Function} callback fired when transaction completes.
   */
  commit: function(trans, callback) {
    var eventStore = this.app.store('Event');
    var icalComponentStore = this.app.store('IcalComponent');
    var calendarStore = this.app.store('Calendar');
    var busytimeStore = this.app.store('Busytime');
    var alarmStore = this.app.store('Alarm');

    if (typeof(trans) === 'function') {
      callback = trans;
      trans = calendarStore.db.transaction(
        ['calendars', 'events', 'busytimes', 'alarms', 'icalComponents'],
        'readwrite'
      );
    }

    if (this._aborted) {
      // Commit nothing, if sync was aborted.
      return callback && callback(null);
    }

    // Stash a reference to the transaction, in case we still need to abort.
    this._trans = trans;

    var self = this;

    this.eventQueue.forEach(function(event) {
      debug('add event', event);
      eventStore.persist(event, trans);
    });

    this.icalQueue.forEach(function(ical) {
      debug('add component', ical);
      icalComponentStore.persist(ical, trans);
    });

    this.busytimeQueue.forEach(function(busy) {
      debug('add busytime', busy);
      busytimeStore.persist(busy, trans);
    });

    this.alarmQueue.forEach(function(alarm) {
      debug('add alarm', alarm);
      alarmStore.persist(alarm, trans);
    });

    if (this.removeList) {
      this.removeList.forEach(function(id) {
        eventStore.remove(id, trans);
      });
    }

    function handleError(e) {
      if (e && e.type !== 'abort') {
        console.error('Error persisting sync results', e);
      }

      // if we have an event preventDefault so we don't trigger window.onerror
      if (e && e.preventDefault) {
        e.preventDefault();
      }

      self._trans = null;
      callback && callback(e);
    }

    trans.addEventListener('error', handleError);
    trans.addEventListener('abort', handleError);


    trans.addEventListener('complete', function() {
      self._trans = null;
      callback && callback(null);
    });

    return trans;
  }
};

});

define('provider/caldav',['require','exports','module','./abstract','error','calc','./caldav_pull_events','error','error','./local','error','error','event_mutations','next_tick'],function(require, exports, module) {


var Abstract = require('./abstract');
var Authentication = require('error').Authentication;
var Calc = require('calc');
var CaldavPullEvents = require('./caldav_pull_events');
var CalendarError = require('error');
var InvalidServer = require('error').InvalidServer;
var Local = require('./local');
var ServerFailure = require('error').ServerFailure;
var CaldavUnknownError = require('error').CaldavUnknownError;
var mutations = require('event_mutations');
var nextTick = require('next_tick');

var CALDAV_ERROR_MAP = {
  'caldav-authentication': Authentication,
  'caldav-invalid-entrypoint': InvalidServer,
  'caldav-server-failure': ServerFailure,
  'caldav-unknown': CaldavUnknownError
};

function mapError(error, detail) {
  console.error('Error with name:', error.name);
  var calError = CALDAV_ERROR_MAP[error.name];
  if (!calError) {
    calError = new CalendarError(error.name, detail);
  } else {
    calError = new calError(detail);
  }

  return calError;
}

function CaldavProvider() {
  Abstract.apply(this, arguments);

  // TODO: Get rid of this when app global is gone.
  mutations.app = this.app;
  this.service = this.app.serviceController;
  this.accounts = this.app.store('Account');
  this.busytimes = this.app.store('Busytime');
  this.events = this.app.store('Event');
  this.icalComponents = this.app.store('IcalComponent');
}
module.exports = CaldavProvider;

CaldavProvider.prototype = {
  __proto__: Abstract.prototype,
  role: 'caldav',
  useUrl: true,
  useCredentials: true,
  canSync: true,
  canExpandRecurringEvents: true,

  /**
   * Number of dates in the past to sync.
   * This is usually from the first sync date.
   */
  daysToSyncInPast: 30,

  canCreateEvent: true,
  canUpdateEvent: true,
  canDeleteEvent: true,

  hasAccountSettings: true,

  /**
   * Error handling can be complex- this is the centralized location where
   * methods can send their error state and some context (an account).
   *
   *    this._handleServiceError(
   *      err,
   *      { account: account, calendar: calendar }
   *    );
   *
   * @param {Object} rawErr from service.
   * @param {Object} detail for the error.
   */
  _handleServiceError: function(rawErr, detail) {
    var calendarErr = mapError(rawErr, detail);

    // when we receive a permanent error we should mark the account with an
    // error.
    if (
      calendarErr instanceof Authentication ||
      calendarErr instanceof InvalidServer
    ) {
      // there must always be an account
      if (detail.account) {
        // but we only mark it with a permanent error if its persisted.
        if (detail.account._id) {
          this.accounts.markWithError(detail.account, calendarErr);
        }
      } else {
        console.error('Permanent server error without an account!');
      }
    }

    return calendarErr;
  },

  /**
   * Determines the capabilities of a specific calendar.
   *
   * The .remote property should contain a .privilegeSet array
   * with the caldav specific names of privileges.
   * In the case where .privilegeSet is missing all privileges are granted.
   *
   * (see http://tools.ietf.org/html/rfc3744#section-5.4).
   *
   *   - write-content: (PUT) can edit/add events
   *   - unbind: (DELETE) remove events
   *
   *
   * There are aggregate values (write for example) but
   * the spec states the specific permissions must also be expanded
   * so even if they have full write permissions we only check
   * for write-content.
   *
   * @param {Object} calendar object with caldav remote details.
   * @return {Object} object with three properties
   *  (canUpdate, canDelete, canCreate).
   */
  calendarCapabilities: function(calendar) {
    var remote = calendar.remote;

    if (!remote.privilegeSet) {
      return {
        canUpdateEvent: true,
        canDeleteEvent: true,
        canCreateEvent: true
      };
    }

    var privilegeSet = remote.privilegeSet;
    var canWriteConent = privilegeSet.indexOf('write-content') !== -1;

    return {
      canUpdateEvent: canWriteConent,
      canCreateEvent: canWriteConent,
      canDeleteEvent: privilegeSet.indexOf('unbind') !== -1
    };
  },

  /**
   * Returns the capabilities of a single event.
   *
   * @param {Object} event local object.
   * @param {Function} callback [err, caps].
   */
  eventCapabilities: function(event, callback) {
    if (event.remote.isRecurring) {
      // XXX: for now recurring events cannot be edited
      nextTick(function() {
        callback(null, {
          canUpdate: false,
          canDelete: false,
          canCreate: false
        });
      });

    } else {
      var calendarStore = this.app.store('Calendar');

      calendarStore.get(event.calendarId, function(err, calendar) {
        if (err) {
          return callback(err);
        }

        var caps = this.calendarCapabilities(
          calendar
        );

        callback(null, {
          canCreate: caps.canCreateEvent,
          canUpdate: caps.canUpdateEvent,
          canDelete: caps.canDeleteEvent
        });
      }.bind(this));
    }
  },

  getAccount: function(account, callback) {
    if (this.bailWhenOffline(callback)) {
      return;
    }

    var self = this;
    this.service.request(
      'caldav',
      'getAccount',
      account,
      function(err, data) {
        if (err) {
          return callback(
            self._handleServiceError(err, { account: account })
          );
        }
        callback(null, data);
      }
    );
  },

  /**
   * Hook to format remote data if needed.
   */
  formatRemoteCalendar: function(calendar) {
    if (!calendar.color) {
      calendar.color = this.defaultColor;
    }

    return calendar;
  },

  findCalendars: function(account, callback) {
    if (this.bailWhenOffline(callback)) {
      return;
    }

    var self = this;
    function formatCalendars(err, data) {
      if (err) {
        return callback(self._handleServiceError(err, {
          account: account
        }));
      }

      // format calendars if needed
      if (data) {
        for (var key in data) {
          data[key] = self.formatRemoteCalendar(data[key]);
        }
      }

      callback(err, data);
    }

    this.service.request(
      'caldav',
      'findCalendars',
      account.toJSON(),
      formatCalendars
    );
  },

  _syncEvents: function(account, calendar, cached, callback) {

    var startDate;
    // calculate the first date we want to sync
    if (!calendar.firstEventSyncDate) {
      startDate = Calc.createDay(new Date());

      // will be persisted if sync is successful (clone is required)
      calendar.firstEventSyncDate = new Date(
        startDate.valueOf()
      );
    } else {
      startDate = new Date(calendar.firstEventSyncDate.valueOf());
    }

    // start date - the amount of days is the sync range
    startDate.setDate(startDate.getDate() - this.daysToSyncInPast);

    var options = {
      startDate: startDate,
      cached: cached
    };

    var stream = this.service.stream(
      'caldav',
      'streamEvents',
      account.toJSON(),
      calendar.remote,
      options
    );

    var pull = new CaldavPullEvents(stream, {
      app: this.app,
      account: account,
      calendar: calendar
    });

    var calendarStore = this.app.store('Calendar');
    var syncStart = new Date();

    var self = this;
    stream.request(function(err) {
      if (err) {
        return callback(
          self._handleServiceError(err, {
            account: account,
            calendar: calendar
          })
        );
      }

      var trans = pull.commit(function(commitErr) {
        if (commitErr) {
          callback(err);
          return;
        }
        callback(null);
      });

      /**
       * Successfully synchronizing a calendar indicates we can remove this
       * error.
       */
      calendar.error = undefined;

      calendar.lastEventSyncToken = calendar.remote.syncToken;
      calendar.lastEventSyncDate = syncStart;

      calendarStore.persist(calendar, trans);

    });

    return pull;
  },

  /**
   * Builds list of event urls & sync tokens.
   *
   * @param {Calendar.Model.Calendar} calender model instance.
   * @param {Function} callback node style [err, results].
   */
  _cachedEventsFor: function(calendar, callback) {
    var store = this.app.store('Event');

    store.eventsForCalendar(calendar._id, function(err, results) {
      if (err) {
        callback(err);
        return;
      }

      var list = Object.create(null);

      var i = 0;
      var len = results.length;
      var item;

      for (; i < len; i++) {
        item = results[i];
        list[item.remote.url] = {
          syncToken: item.remote.syncToken,
          id: item._id
        };
      }

      callback(null, list);
    });
  },

  /**
   * Sync remote and local events for a calendar.
   */
  syncEvents: function(account, calendar, callback) {
    var self = this;

    if (this.bailWhenOffline(callback)) {
      return;
    }

    if (!calendar._id) {
      throw new Error('calendar must be assigned an _id');
    }

    // Don't attempt to sync when provider cannot
    // or we have matching tokens
    if ((calendar.lastEventSyncToken &&
         calendar.lastEventSyncToken === calendar.remote.syncToken)) {
      return nextTick(callback);
    }

    this._cachedEventsFor(calendar, function(err, results) {
      if (err) {
        callback(err);
        return;
      }

      self._syncEvents(
        account,
        calendar,
        results,
        callback
      );
    });
  },

  /**
   * See abstract for contract details...
   *
   * Finds all ical components that have not been expanded
   * beyond the given point and expands / persists them.
   *
   * @param {Date} maxDate maximum date to expand to.
   * @param {Function} callback [err, didExpand].
   */
  ensureRecurrencesExpanded: function(maxDate, callback) {
    var self = this;
    this.icalComponents.findRecurrencesBefore(maxDate,
                                              function(err, results) {
      if (err) {
        callback(err);
        return;
      }

      if (!results.length) {
        callback(null, false);
        return;
      }

      // CaldavPullRequest is based on a calendar/account combination
      // so we must group all of the outstanding components into
      // their calendars before we can begin expanding them.
      var groups = Object.create(null);
      results.forEach(function(comp) {
        var calendarId = comp.calendarId;
        if (!(calendarId in groups)) {
          groups[calendarId] = [];
        }

        groups[calendarId].push(comp);
      });

      var pullGroups = [];
      var pending = 0;
      var options = {
        maxDate: Calc.dateToTransport(maxDate)
      };

      function next(err, pull) {
        pullGroups.push(pull);
        if (!(--pending)) {
          var trans = self.app.db.transaction(
            ['icalComponents', 'alarms', 'busytimes'],
            'readwrite'
          );

          trans.oncomplete = function() {
            callback(null, true);
          };

          trans.onerror = function(event) {
            callback(event.result.error.name);
          };

          pullGroups.forEach(function(pull) {
            pull.commit(trans);
          });
        }
      }

      for (var calendarId in groups) {
        pending++;
        self._expandComponents(
          calendarId,
          groups[calendarId],
          options,
          next
        );
      }

    });
  },

  _expandComponents: function(calendarId, comps, options, callback) {
    var calStore = this.app.store('Calendar');

    calStore.ownersOf(calendarId, function(err, owners) {
      if (err) {
        return callback(err);
      }

      var calendar = owners.calendar;
      var account = owners.account;

      var stream = this.service.stream(
        'caldav',
        'expandComponents',
        comps,
        options
      );

      var pull = new CaldavPullEvents(
        stream,
        {
          account: account,
          calendar: calendar,
          app: this.app,
          stores: [
            'busytimes', 'alarms', 'icalComponents'
          ]
        }
      );

      stream.request(function(err) {
        if (err) {
          callback(err);
          return;
        }
        callback(null, pull);
      });

    }.bind(this));
  },

  createEvent: function(event, busytime, callback) {
    if (typeof(busytime) === 'function') {
      callback = busytime;
      busytime = null;
    }


    if (this.bailWhenOffline(callback)) {
      return;
    }

    this.events.ownersOf(event, fetchOwners);

    var self = this;
    var calendar;
    var account;
    function fetchOwners(err, owners) {
      calendar = owners.calendar;
      account = owners.account;

      self.service.request(
        'caldav',
        'createEvent',
        account,
        calendar.remote,
        event.remote,
        handleRequest
      );
    }

    function handleRequest(err, remote) {
      if (err) {
        return callback(self._handleServiceError(err, {
          account: account,
          calendar: calendar
        }));
      }

      var event = {
        _id: calendar._id + '-' + remote.id,
        calendarId: calendar._id
      };

      var component = {
        eventId: event._id,
        ical: remote.icalComponent
      };

      delete remote.icalComponent;
      event.remote = remote;

      var create = mutations.create({
        event: event,
        icalComponent: component
      });

      create.commit(function(err) {
        if (err) {
          callback(err);
          return;
        }

        callback(null, create.busytime, create.event);
      });
    }
  },

  updateEvent: function(event, busytime, callback) {
    if (typeof(busytime) === 'function') {
      callback = busytime;
      busytime = null;
    }

    if (this.bailWhenOffline(callback)) {
      return;
    }

    this.events.ownersOf(event, fetchOwners);

    var self = this;
    var calendar;
    var account;

    function fetchOwners(err, owners) {
      calendar = owners.calendar;
      account = owners.account;

      self.icalComponents.get(
        event._id, fetchComponent
      );
    }

    function fetchComponent(err, ical) {
      if (err) {
        callback(err);
        return;
      }

      var details = {
        event: event.remote,
        icalComponent: ical.ical
      };

      self.service.request(
        'caldav',
        'updateEvent',
        account,
        calendar.remote,
        details,
        handleUpdate
      );
    }

    function handleUpdate(err, remote) {
      if (err) {
        callback(self._handleServiceError(err, {
          account: account,
          calendar: calendar
        }));
        return;
      }

      var component = {
        eventId: event._id,
        ical: remote.icalComponent
      };

      delete remote.icalComponent;
      event.remote = remote;

      var update = mutations.update({
        event: event,
        icalComponent: component
      });

      update.commit(function(err) {
        if (err) {
          callback(err);
          return;
        }
        callback(null, update.busytime, update.event);
      });
    }
  },

  deleteEvent: function(event, busytime, callback) {
    if (typeof(busytime) === 'function') {
      callback = busytime;
      busytime = null;
    }

    if (this.bailWhenOffline(callback)) {
      return;
    }

    this.events.ownersOf(event, fetchOwners);

    var calendar;
    var account;
    var self = this;
    function fetchOwners(err, owners) {
      calendar = owners.calendar;
      account = owners.account;

      self.service.request(
        'caldav',
        'deleteEvent',
        account,
        calendar.remote,
        event.remote,
        handleRequest
      );

    }

    function handleRequest(err) {
      if (err) {
        callback(self._handleServiceError(err, {
          account: account,
          calendar: calendar
        }));
        return;
      }
      Local.prototype.deleteEvent.call(self, event, busytime, callback);
    }
  },

  bailWhenOffline: function(callback) {
    if (!this.offlineMessage && 'mozL10n' in window.navigator) {
      this.offlineMessage = window.navigator.mozL10n.get('error-offline');
    }

    var ret = this.app.offline() && callback;
    if (ret) {
      var error = new Error();
      error.name = 'offline';
      error.message = this.offlineMessage;
      callback(error);
    }
    return ret;
  }
};

});

define('provider/caldav_visual_log',['require','exports','module','calc'],function(require, exports, module) {


var Calc = require('calc');

/**
 * This is a more crude version of what asuth does in email.
 * Right now this class is here only for debugging sync issues.
 * We need to add the settings so we can optionally turn this on.
 */
function EventLogger() {
  this.events = Object.create(null);
  this.occurs = Object.create(null);

  this.richLog = {};
}
module.exports = EventLogger;

EventLogger.prototype = {
  displayLog: function(id, string) {
    if (!(id in this.richLog)) {
      this.richLog[id] = [];
    }
    this.richLog[id].push(string);
  },

  addEvent: function(event) {
    var id = event.id;
    this.events[id] = event;
    var log = this.formatEvent(event);
    this.displayLog(id, log);
  },

  addBusytime: function(busy) {
    var id = busy.eventId;
    this.occurs[id] = busy;
    this.displayLog(id, this.formatBusytime(busy));
  },

  formatEvent: function(event) {
    var format = [
      'add event: (' + event.id + ')',
      'title:' + event.title,
      'start:' + (Calc.dateFromTransport(event.start)).toString(),
      'end:' + (Calc.dateFromTransport(event.end)).toString(),
      'isException:' + event.isException
    ];

    return format.join(' || ');
  },

  formatBusytime: function(busy) {
    var event = this.events[busy.eventId];
    var title = 'busytime for event: ' + busy.eventId;

    if (event) {
      title = 'busytime for event: ' + event.title;
    }

    var format = [
      title,
      'start:' + (Calc.dateFromTransport(busy.start)).toString(),
      'end:' + (Calc.dateFromTransport(busy.end)).toString(),
      'isException:' + busy.isException
    ];

    return format.join(' || ');
  }
};

});

define('provider/provider',['require','exports','module','./abstract','./caldav','./caldav_pull_events','./caldav_visual_log','./local'],function(require, exports) {


exports.Abstract = require('./abstract');
exports.Caldav = require('./caldav');
exports.CaldavPullEvents = require('./caldav_pull_events');
exports.CaldavVisualLog = require('./caldav_visual_log');
exports.Local = require('./local');

});

/**
 * TODO(gareth): This thing must die.
 */
define('provider/provider_factory',['require','exports','module','./provider'],function(require, exports) {


var Provider = require('./provider');

var providers = exports.providers = {};

// Will be injected...
exports.app = null;

exports.get = function(name) {
  if (!providers[name]) {
    providers[name] = new Provider[name]({ app: exports.app });
  }

  return providers[name];
};

});

define('store/account',['require','exports','module','models/account','./abstract','debug','promise','extend','next_tick','probably_parse_int','provider/provider_factory'],function(require, exports, module) {


var AccountModel = require('models/account');
var Abstract = require('./abstract');
var debug = require('debug')('store/account');
var denodeifyAll = require('promise').denodeifyAll;
var extend = require('extend');
var nextTick = require('next_tick');
var probablyParseInt = require('probably_parse_int');
var providerFactory = require('provider/provider_factory');

function Account() {
  Abstract.apply(this, arguments);

  denodeifyAll(this, [
    'verifyAndPersist',
    'sync',
    'markWithError',
    'syncableAccounts',
    'availablePresets'
  ]);
}
module.exports = Account;

Account.prototype = {
  __proto__: Abstract.prototype,

  _store: 'accounts',

  _parseId: probablyParseInt,

  /**
   * Checks if a given account is a duplicate of another.
   *
   * @param {Calendar.Model.Account} model to check.
   * @param {Function} callback [err].
   */
  _validateModel: function(model, callback) {
    this.all(function(err, allAccounts) {
      if (err) {
        callback(err);
        return;
      }

      // check if this account is already registered
      for (var index in allAccounts) {
        if (
            allAccounts[index].user === model.user &&
            allAccounts[index].fullUrl === model.fullUrl &&
            allAccounts[index]._id !== model._id
        ) {

          var dupErr = new Error(
            'Cannot add two accounts with the same url / entry point'
          );

          dupErr.name = 'account-exist';
          callback(dupErr);
          return;
        }
      }

      callback();
    });
  },

  verifyAndPersist: function(model, callback) {
    var self = this;
    var provider = providerFactory.get(
      model.providerType
    );

    provider.getAccount(model.toJSON(), function(err, data) {
      if (err) {
        callback(err);
        return;
      }

      model.error = undefined;

      // if this works we always will get a calendar home.
      // This is used to find calendars.
      model.calendarHome = data.calendarHome;

      // server may override properties on demand.
      extend(model, data);

      self._validateModel(model, function(err) {
        if (err) {
          return callback(err);
        }

        self.persist(model, callback);
      });
    });
  },

  /**
   * Because this is a top-level store
   * when we remove an account all records
   * related to it must be removed.
   */
  _dependentStores: [
    'accounts', 'calendars', 'events',
    'busytimes', 'alarms', 'icalComponents'
  ],

  _removeDependents: function(id, trans) {
    var store = this.db.getStore('Calendar');
    store.remotesByAccount(id, trans, function(err, related) {
      if (err) {
        return console.error('Error removing deps for account: ', id);
      }

      var key;
      for (key in related) {
        store.remove(related[key]._id, trans);
      }
    });
  },

  /**
   * Syncs all calendars for account.
   *
   * TODO: Deprecate this method in favor of new provider API's.
   *
   * @param {Calendar.Models.Account} account sync target.
   * @param {Function} callback node style.
   */
  sync: function(account, callback) {
    //TODO: We need to block removal when syncing
    //OR after removal ensure everything created here
    //is purged.

    var self = this;
    var provider = providerFactory.get(account.providerType);
    var calendarStore = this.db.getStore('Calendar');

    var persist = [];

    // remotesByAccount return an object indexed by remote ids
    var calendars;

    // these are remote ids not local ones
    var originalIds;

    function fetchExistingCalendars(err, results) {
      if (err) {
        return callback(err);
      }

      calendars = results;
      originalIds = Object.keys(calendars);

      provider.findCalendars(account, persistCalendars);
    }

    function persistCalendars(err, remoteCals) {
      var key;

      if (err) {
        callback(err);
        return;
      }

      for (key in remoteCals) {
        if (remoteCals.hasOwnProperty(key)) {
          var cal = remoteCals[key];
          var idx = originalIds.indexOf(key);

          if (idx !== -1) {
            // update an existing calendar
            originalIds.splice(idx, 1);

            var original = calendars[key];
            original.remote = cal;
            original.error = undefined;
            persist.push(original);
          } else {
            // create a new calendar
            persist.push(
              calendarStore._createModel({
                remote: new Object(cal),
                accountId: account._id
              })
            );
          }
        }
      }

      // at this point whatever is left in originalIds
      // is considered a removed calendar.

      // update / remove
      if (persist.length || originalIds.length) {
        var trans = self.db.transaction(
          self._dependentStores,
          'readwrite'
        );

        originalIds.forEach(function(id) {
          calendarStore.remove(calendars[id]._id, trans);
        });

        persist.forEach(function(object) {
          calendarStore.persist(object, trans);
        });

        // event listeners must come at the end
        // because persist/remove also listen to
        // transaction complete events.
        trans.addEventListener('error', function(err) {
          callback(err);
        });

        trans.addEventListener('complete', function() {
          callback(null);
        });
      } else {
        // invoke callback nothing to sync
        callback(null);
      }
    }

    calendarStore.remotesByAccount(
      account._id,
      fetchExistingCalendars
    );
  },

  _createModel: function(obj, id) {
    if (!(obj instanceof AccountModel)) {
      obj = new AccountModel(obj);
    }

    if (typeof(id) !== 'undefined') {
      obj._id = id;
    }

    return obj;
  },

  /**
   * Marks given model with an error and sends an error event with the given
   * model
   *
   * This will trigger an 'error' event immediately with the given model.
   * The callback fires _after_ the event. Its entirely possible (under rare
   * conditions) that this operation will fail but the event will fire.
   *
   *
   * @param {Object} account model.
   * @param {Calendar.Error} error to mark model with.
   * @param {IDBTransaction} [trans] optional transaction.
   * @param {Function} [callback] optional called with [err, model].
   */
  markWithError: function(account, error, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = null;
    }

    if (!account._id) {
      throw new Error('given account must be persisted');
    }

    if (!account.error) {
      account.error = {
        name: error.name,
        date: new Date(),
        count: 0
      };
    }

    // increment the error count
    account.error.count++;

    var calendarStore = this.db.getStore('Calendar');
    var self = this;
    function fetchedCalendars(err, calendars) {
      if (!trans) {
        trans = self.db.transaction(
          self._dependentStores,
          'readwrite'
        );
      }

      if (err) {
        console.error('Cannot fetch all calendars', err);
        return self.persist(account, trans, callback);
      }

      for (var id in calendars) {
        calendarStore.markWithError(calendars[id], error, trans);
      }

      self.persist(account, trans);
      self._transactionCallback(trans, callback);

    }

    // find related calendars and mark those too
    calendarStore.remotesByAccount(
      account._id,
      fetchedCalendars
    );
  },

  /**
   * Finds and returns all accounts that can sync (based on their provider).
   *
   *    accountStore.syncableAccounts(function(err, list) {
   *      if (list.length === 0)
   *        // hide sync options
   *    });
   *
   * @param {Function} callback [Error err, Array accountList].
   */
  syncableAccounts: function(callback) {
    debug('Will find syncable accounts...');
    this.all((err, list) => {
      if (err) {
        return callback(err);
      }

      var results = [];
      for (var key in list) {
        var account = list[key];
        var provider = providerFactory.get(account.providerType);
        if (provider.canSync) {
          results.push(account);
        }
      }

      callback(null, results);
    });
  },

  /**
   * Returns a list of available presets filtered by
   * the currently used presets in the database.
   *
   * Expected structure of the presetList is as follows:
   *
   *    {
   *      'presetType': {
   *        // most important field when true if the preset
   *        // is available in the database that preset type
   *        // will be excluded.
   *        singleUse: true
   *        providerType: 'X',
   *        options: {}
   *      }
   *
   *    }
   *
   * @param {Object} presetList see example ^^^.
   * @param {Function} callback [err, ['presetKey', ...]].
   */
  availablePresets: function(presetList, callback) {
    var results = [];
    var singleUse = {};
    var hasSingleUses = false;

    for (var preset in presetList) {
      if (presetList[preset].singleUse) {
        hasSingleUses = true;
        singleUse[preset] = true;
      } else {
        results.push(preset);
      }
    }

    if (!hasSingleUses) {
      return nextTick(function() {
        callback(null, results);
      });
    }

    this.all(function(err, list) {
      if (err) {
        callback(err);
        return;
      }

      for (var id in list) {
        var preset = list[id].preset;
        if (singleUse[preset]) {
          delete singleUse[preset];
        }
      }

      // add un-used presets to the list.
      callback(null, results.concat(Object.keys(singleUse)));
    });
  }
};

});

/**
 * @fileoverview Simple helper function to convert basic platform DOMRequest
 *     into Promise. Deprecate once there is platform support for something
 *     like DOMRequest.then().
 */
define('create_dom_promise',['require','exports','module'],function(require, exports, module) {


module.exports = function createDOMPromise(request) {
  return new Promise((resolve, reject) => {
    request.onsuccess = resolve;
    request.onerror = reject;
  });
};

});

define('message_handler',['require','exports','module','responder','debug','notification'],function(require, exports, module) {


var Responder = require('responder');
var debug = require('debug')('message_handler');
var notification = require('notification');

// Will be injected...
exports.app = null;
var responder = exports.responder = new Responder();

exports.start = function() {
  if (!('mozSetMessageHandler' in navigator)) {
    debug('mozSetMessageHandler is missing!');
    return;
  }

  debug('Will listen for alarm messages...');
  navigator.mozSetMessageHandler('alarm', message => {
    debug('Received alarm message!');
    var data = message.data;
    switch (data.type) {
      case 'sync':
        responder.emitWhenListener('sync');
        break;
      default:
        responder.emitWhenListener('alarm', data);
        break;
    }
  });

  // Handle notifications when the calendar app process is closed.
  debug('Will listen for notification messages...');
  navigator.mozSetMessageHandler('notification', message => {
    debug('Received notification message!');
    if (!message.clicked) {
      return debug('Notification was not clicked?');
    }

    var url = message.imageURL.split('?')[1];
    notification.launch(url);
  });
};

});

define('controllers/notifications',['require','exports','module','calc','date_format','debug','message_handler','notification'],function(require, exports) {


var calc = require('calc');
var dateFormat = require('date_format');
var debug = require('debug')('controllers/notifications');
var messageHandler = require('message_handler');
var notification = require('notification');

function formatDate(date, fmt) {
  var ishour12 = /(h+)/.test(fmt);
    var o = {
    "M+" : date.getMonth()+1,
    "d+" : date.getDate(),
    "h+" : date.getHours()%12 == 0 ? 12 : date.getHours()%12,
    "H+" : date.getHours(),
    "m+" : date.getMinutes(),
    "s+" : date.getSeconds(),
    "q+" : Math.floor((date.getMonth()+3)/3),
    "S" : date.getMilliseconds()
    };
    var week = {
    "0" : "/u65e5",
    "1" : "/u4e00",
    "2" : "/u4e8c",
    "3" : "/u4e09",
    "4" : "/u56db",
    "5" : "/u4e94",
    "6" : "/u516d"
    };
    if(/(y+)/.test(fmt)){
        fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    if(/(E+)/.test(fmt)){
        fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[date.getDay()+""]);
    }
    for(var k in o){
        if(new RegExp("("+ k +")").test(fmt)){
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }
    }
  if(ishour12) {
    fmt = fmt + (date.getHours() >= 12? " PM": " AM");
  }
    return fmt;
}

// Will be injected...
exports.app = null;

exports.observe = function() {
  debug('Will start notifications controller...');
  messageHandler.responder.on('alarm', exports.onAlarm);
};

exports.unobserve = function() {
  messageHandler.responder.off('alarm', exports.onAlarm);
};

exports.onAlarm = function(alarm) {
  debug('Will request cpu wake lock...');
  var lock = navigator.requestWakeLock('cpu');
  debug('Received cpu lock. Will issue notification...');
  return issueNotification(alarm).catch(err => {
    console.error('controllers/notifications', err.toString());
  }).then(() => {
    // release cpu lock with or without errors
    debug('Will release cpu wake lock...');
    lock.unlock();
  });
};

function prettyDateForCalendar(time, useCompactFormat, maxDiff) {
  var _ = navigator.mozL10n.get;

  maxDiff = maxDiff || 86400 * 2; // default = 2 days

  switch (time.constructor) {
    case String: // timestamp
      time = parseInt(time);
      break;
    case Date:
      time = time.getTime();
      break;
  }

  var now = Date.now();
  var secDiff = (now - time) / 1000;
  if (isNaN(secDiff)) {
    return _('incorrectDate');
  }

  if (Math.abs(secDiff) > 60) {
    // round milliseconds up if difference is over 1 minute so the result is
    // closer to what the user would expect (1h59m59s300ms diff should return
    // "in 2 hours" instead of "in an hour")
    // After exit the calendar application, it takes a few seconds to pull up 
    // the alarm event.
    secDiff = secDiff > 0 ? Math.ceil(secDiff) : Math.floor(secDiff - 30);
  }

  var today = new Date();
  today.setHours(0,0,0,0);
  var todayMidnight = today.getTime();
  var yesterdayMidnight = todayMidnight - 86400 * 1000;

  const thisyearTimestamp = 
        (new Date(today.getFullYear().toString())).getTime();
  // ex. 11:59 PM or 23:59
  const timeFormat = navigator.mozHour12 ? '%I:%M %p' : '%H:%M';

  if (time < thisyearTimestamp) {
    // before this year, ex. December 31, 2015 11:59 PM
    return dateFormat.localeFormat(new Date(time), '%B %e, %Y ' + timeFormat);
  } else if (time < yesterdayMidnight || 
             time >= todayMidnight + 86400 * 1000 * 3) {
    // before yesterday and in this year, ex. August 31, 11:59 PM
    return dateFormat.localeFormat(new Date(time), '%B %e, ' + timeFormat);
  } else if (time < todayMidnight) {
    // yesterday
    return _('days-ago-long', {value: 1}) + ', ' + 
           dateFormat.localeFormat(new Date(time), timeFormat);
  } else if (time >= todayMidnight + 86400 * 1000 * 2) {
    return _('days-until-long', {value: 2});
  } else if (time >= todayMidnight + 86400 * 1000) {
    return _('days-until-long', {value: 1});
  } else if (secDiff > 3600 * 4) {
    // today and before 4 hours
    return _('days-ago-long', {value: 0}) + ', ' + 
           dateFormat.localeFormat(new Date(time), timeFormat);
  } else {
    // in 4 hours
    var f = useCompactFormat ? '-short' : '-long';
    var parts = dateFormat.relativeParts(secDiff);

    var affix = secDiff >= 0 ? '-ago' : '-until';
    for (var i in parts) {
      return _(i + affix + f, { value: parts[i]});
    }
  }
}

function issueNotification(alarm) {
  var app = exports.app;
  var eventStore = app.store('Event');
  var busytimeStore = app.store('Busytime');

  var trans = app.db.transaction(['busytimes', 'events']);

  // Find the event and busytime associated with this alarm.
  return Promise.all([
    eventStore.get(alarm.eventId, trans),
    busytimeStore.get(alarm.busytimeId, trans)
  ]).then((values) => {
    var [event, busytime] = values;
    var _ = navigator.mozL10n.get;

    // just a safeguard on the very unlikely case that busytime or event
    // doesn't exist anymore (should be really hard to happen)
    if (!event) {
      throw new Error(`can't find event with ID: ${alarm.eventId}`);
    }
    if (!busytime) {
      throw new Error(`can't find busytime with ID: ${alarm.busytimeId}`);
    }

    var begins = calc.dateFromTransport(busytime.start);
    var distance = prettyDateForCalendar(begins);
    var now = new Date();

    var alarmType = begins > now ?
      'alarm-start-notice' :
      'alarm-started-notice';

    var l10n = navigator.mozL10n;
    var title = l10n.get(alarmType, {
      title: event.remote.title,
      distance: distance
    });

    if (begins > now) {
      if (distance === _('days-until-long', {value: 2})) {
        title = _('alarm-start-notice-two-days', {title: event.remote.title});
      } else if (distance !== _('days-until-long', {value: 1})) {
        title = _('alarm-start-notice-few-days', {
          title: event.remote.title,
          distance: distance
        })
      }
    }

    var starttime = event.remote.startDate;

    var body = event.remote.description || '';
    debug('Will send event notification with title:', title, 'body:', body);
    return notification.sendNotification(
      title,
      body,
      `/alarm-display/${busytime._id}`
    );
  });
}

});

define('object',['require','exports','module'],function(require, exports, module) {


exports.filter = function(obj, fn, thisArg) {
  var results = [];
  exports.forEach(obj, function(key, value) {
    if (fn.call(thisArg, key, value)) {
      results.push(value);
    }
  });

  return results;
};

exports.forEach = function(obj, fn, thisArg) {
  exports.map(obj, fn, thisArg);
};

exports.map = function(obj, fn, thisArg) {
  var results = [];
  Object.keys(obj).forEach((key) => {
    var value = obj[key];
    var result = fn.call(thisArg, key, value);
    results.push(result);
  });

  return results;
};

exports.values = function(obj) {
  return exports.map(obj, (key, value) => {
    return value;
  });
};

});

define('store/alarm',['require','exports','module','./abstract','calc','create_dom_promise','debug','promise','controllers/notifications','object'],function(require, exports, module) {


var Abstract = require('./abstract');
var Calc = require('calc');
var createDOMPromise = require('create_dom_promise');
var debug = require('debug')('store/alarm');
var denodeifyAll = require('promise').denodeifyAll;
var notificationsController = require('controllers/notifications');
var object = require('object');

/**
 * The alarm store can be thought of as a big queue.
 * Over time we add and remove alarm times related to
 * a specific busytime/event instance.
 * (and there could be multiple alarms per busytime/event).
 *
 * When `workQueue` is called records will be removed
 * from the queue (this object store) and added (via mozAlarms).
 */
function Alarm() {
  Abstract.apply(this, arguments);
  this._processQueue = this._processQueue.bind(this);

  denodeifyAll(this, [
    'findAllByBusytimeId',
    'workQueue'
  ]);
}
module.exports = Alarm;

Alarm.prototype = {
  __proto__: Abstract.prototype,

  _store: 'alarms',

  _dependentStores: ['alarms'],

  /**
   * Number of hours ahead of current time to add new alarms.
   *
   * @type Numeric
   */
  _alarmAddThresholdHours: 48,

  /** disable caching */
  _addToCache: function() {},
  _removeFromCache: function() {},

  /**
   * When false will not process queue automatically
   * (that is after each alarm transaction is complete).
   *
   * @type {Boolean}
   */
  autoQueue: false,

  _processQueue: function() {
    this.workQueue();
  },

  _objectData: function(object) {
    var data = Abstract.prototype._objectData.call(this, object);
    if (data.startDate) {
      // ensure the pending trigger is always in sync
      // with the current trigger whenever we update
      // the model.
      data.trigger = data.startDate;
    }

    return data;
  },

  /**
   * Manage the queue when alarms are added.
   */
  _addDependents: function(obj, trans) {
    if (!this.autoQueue) {
      return;
    }

    // by using processQueue even if we added
    // 6000 alarms during a single transaction we only
    // receive the event once as addEventListener discards
    // duplicates.
    trans.addEventListener('complete', this._processQueue);
  },

  /**
   * Move alarms over to the alarm api's database.
   *
   *
   * @param {Date} now date to use as current time.
   *
   * @param {Boolean} requiresAlarm attempts to ensure at
   *                                lest one alarm is added.
   *
   * @param {Function} callback node style callback.
   */
  _moveAlarms: function(now, requiresAlarm, callback) {
    // use transport dates so we can handle timezones & floating time.
    var time = Calc.dateToTransport(now);
    var utc = time.utc;
    // keep adding events until we are beyond this time.
    var minimum = utc + (this._alarmAddThresholdHours * Calc.HOUR);

    var request = this.db
      .transaction('alarms', 'readwrite')
      .objectStore('alarms')
      .index('trigger')
      .openCursor();

    request.onerror = function() {
      callback(new Error('Alarm cursor failed to open.'));
    };

    var past = [];  // Alarms that should be fired immediately.
    var future = [];  // Alarms that should fire in the future.
    request.onsuccess = function(event) {
      var cursor = event.target.result;
      if (!cursor ||
          (cursor.key >= minimum && (!requiresAlarm || future.length))) {
        // We've pulled all (or at least enough) alarms into memory.
        // Now we can send them to the notifications controller
        // or the alarms api.
        return dispatchAlarms(past, future)
        .then(callback)
        .catch(error => debug('Error dispatching alarms:', error));
      }

      var record = cursor.value;
      var date = Calc.dateFromTransport(record.trigger);
      var bucket = date < Date.now() ? past : future;
      bucket.push(record);
      // We need to save the trigger time so that we can send the
      // appropriate time to the alarms api. However, we want to mark
      // that we've handled this alarm so delete the trigger prop.
      record.triggered = record.trigger;
      delete record.trigger;
      cursor.update(record);
      cursor.continue();
    };
  },

  /**
   * Finds single alarm by busytime id.
   *
   * @param {Object} related busytime object.
   * @param {IDBTransaction} [trans] optional transaction.
   * @param {Function} callback node style [err, records].
   */
  findAllByBusytimeId: function(busytimeId, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = null;
    }

    if (!trans) {
      trans = this.db.transaction(this._dependentStores);
    }

    var store = trans.objectStore(this._store);
    var index = store.index('busytimeId');
    var key = IDBKeyRange.only(busytimeId);

    index.mozGetAll(key).onsuccess = function(e) {
      callback(null, e.target.result);
    };
  },

  /**
   * Works queue putting alarms into the alarm api database where needed.
   *
   */
  workQueue: function(now, callback) {
    if (typeof(now) === 'function') {
      callback = now;
      now = null;
    }

    now = now || new Date();
    var alarms = navigator.mozAlarms;

    if (!alarms) {
      if (callback) {
        callback(null);
      }

      return;
    }

    var self = this;
    var requiresAlarm = false;

    /**
     * Why are we getting all alarms here?
     *
     * The alarms are designed to keep the total number
     * of entires (in mozAlarms) down but we should keep at
     * minimum one active at all times. For example if the user
     * has sync turned off and wants notifications we need
     * to have an alarm go off to trigger adding more alarms.
     */
    var req = alarms.getAll();

    //XXX: even with the good reasons above we need
    //     to justify the perf cost here later.
    req.onsuccess = function(e) {
      var data = e.target.result;
      var len = data.length;
      var mozAlarm;

      requiresAlarm = true;

      for (var i = 0; i < len; i++) {
        mozAlarm = data[i].data;
        if (
          mozAlarm &&
          'eventId' in mozAlarm &&
          'trigger' in mozAlarm
        ) {
          requiresAlarm = false;
          break;
        }
      }

      callback = callback || function() {};
      self._moveAlarms(
        now,
        requiresAlarm,
        callback
      );
    };

    req.onerror = function() {
      var msg = 'failed to get alarms';
      console.error('CALENDAR:', msg);

      if (callback) {
        callback(new Error(msg));
      }
    };
  }
};

function dispatchAlarms(past, future) {
  // If the alarm was meant to be triggered in the past,
  // we want to immediately issue a notification.
  // However, in bug 857284 we add the stipulation that
  // we shouldn't issue duplicates, so handle that here also.
  var eventToAlarm = {};
  past.forEach(alarm => {
    var event = alarm.eventId;
    if (!event || event in eventToAlarm) {
      return;
    }

    eventToAlarm[event] = alarm;
  });

  object.forEach(eventToAlarm, (event, alarm) => {
    notificationsController.onAlarm(alarm);
  });

  // If the alarm should be triggered in the future, then we can create an
  // entry in the alarms api to wake us up to issue a notification for it
  // at the appropriate time.
  var alarms = navigator.mozAlarms;
  return Promise.all(future.map(alarm => {
    var timezone = alarm.triggered.tzid === Calc.FLOATING ?
      'ignoreTimezone' :
      'honorTimezone';
    return createDOMPromise(
      alarms.add(
        Calc.dateFromTransport(alarm.triggered),
        timezone,
        alarm
      )
    );
  }));
}

});

define('time_observer',['require','exports','module','timespan'],function(require, exports, module) {


var Timespan = require('timespan');

function TimeObserver() {
  this._timeObservers = [];
}
module.exports = TimeObserver;

TimeObserver.enhance = function(given) {
  var key;
  var proto = TimeObserver.prototype;
  for (key in proto) {
    if (proto.hasOwnProperty(key)) {
      given[key] = proto[key];
    }
  }
};

TimeObserver.prototype = {
 /**
   * Adds observer for timespan.
   *
   * Object example:
   *
   *    object.handleEvent = function(e) {
   *      // e.type
   *      // e.data
   *      // e.time
   *    }
   *
   *    // when given an object
   *    EventStore.observe(timespan, object)
   *
   *
   * Callback example:
   *
   *    EventStore.observe(timespan, function(event) {
   *      // e.type
   *      // e.data
   *      // e.time
   *    });
   *
   * @param {Calendar.Timespan} timespan span to observe.
   * @param {Function|Object} callback function or object follows
   *                                   EventTarget pattern.
   */
  observeTime: function(timespan, callback) {
    if (!(timespan instanceof Timespan)) {
      throw new Error(
        'must pass an instance of Timespan as first argument'
      );
    }
    this._timeObservers.push([timespan, callback]);
  },

  /**
   * Finds index of timespan/object|callback pair.
   *
   * Used internally and in tests has little practical use
   * unless you have the original timespan object.
   *
   * @param {Calendar.Timespan} timespan original (===) timespan used.
   * @param {Function|Object} callback original callback/object.
   * @return {Numeric} -1 when not found otherwise index.
   */
  findTimeObserver: function(timespan, callback) {
    var len = this._timeObservers.length;
    var field;
    var i = 0;

    for (; i < len; i++) {
      field = this._timeObservers[i];

      if (field[0] === timespan &&
          field[1] === callback) {

        return i;
      }
    }

    return -1;
  },

  /**
   * Removes a time observer you
   * must pass the same instance of both
   * the timespan and the callback/object
   *
   *
   * @param {Calendar.Timespan} timespan timespan object.
   * @param {Function|Object} callback original callback/object.
   * @return {Boolean} true when found & removed callback.
   */
  removeTimeObserver: function(timespan, callback) {
    var idx = this.findTimeObserver(timespan, callback);

    if (idx !== -1) {
      this._timeObservers.splice(idx, 1);
      return true;
    } else {
      return false;
    }
  },

  /**
   * Fires a time based event.
   *
   * @param {String} type name of event.
   * @param {Date|Numeric} start start position of time event.
   * @param {Date|Numeric} end end position of time event.
   * @param {Object} data data related to event.
   */
  fireTimeEvent: function(type, start, end, data) {
    var i = 0;
    var len = this._timeObservers.length;
    var observer;
    var event = {
      time: true,
      data: data,
      type: type
    };

    for (; i < len; i++) {
      observer = this._timeObservers[i];
      if (observer[0].overlaps(start, end)) {
        if (typeof(observer[1]) === 'object') {
          observer[1].handleEvent(event);
        } else {
          observer[1](event);
        }
      }
    }
  }
};

});

/**
 * Binary search utilities taken /w permission from :asuth
 */
define('binsearch',['require','exports','module'],function(require, exports) {


exports.find = function(list, seekVal, cmpfunc, aLow, aHigh) {
  var low = ((aLow === undefined) ? 0 : aLow),
      high = ((aHigh === undefined) ? (list.length - 1) : aHigh),
      mid, cmpval;

  while (low <= high) {
    mid = low + Math.floor((high - low) / 2);
    cmpval = cmpfunc(seekVal, list[mid]);
    if (cmpval < 0) {
      high = mid - 1;
    } else if (cmpval > 0) {
      low = mid + 1;
    } else {
      return mid;
    }
  }

  return null;
};

exports.insert = function(list, seekVal, cmpfunc) {
  if (!list.length) {
    return 0;
  }

  var low = 0, high = list.length - 1,
      mid, cmpval;

  while (low <= high) {
    mid = low + Math.floor((high - low) / 2);
    cmpval = cmpfunc(seekVal, list[mid]);

    if (cmpval < 0) {
      high = mid - 1;
    } else if (cmpval > 0) {
      low = mid + 1;
    } else {
      break;
    }
  }

  if (cmpval < 0) {
    return mid; // insertion is displacing, so use mid outright.
  } else if (cmpval > 0) {
    return mid + 1;
  } else {
    return mid;
  }
};

});

define('compare',['require','exports','module'],function(require, exports, module) {


module.exports = function(a, b) {
  if (a > b) {
    return 1;
  }

  if (a < b) {
    return -1;
  }

  return 0;
};

});

define('store/busytime',['require','exports','module','./abstract','calc','time_observer','binsearch','compare','promise'],function(require, exports, module) {


var Abstract = require('./abstract');
var Calc = require('calc');
var TimeObserver = require('time_observer');
var binsearch = require('binsearch');
var compare = require('compare');
var denodeifyAll = require('promise').denodeifyAll;

/**
 * Objects saved in the busytime store:
 *
 *    {
 *      _id: (uuid),
 *      start: Calendar.Calc.dateToTransport(x),
 *      end: Calendar.Calc.dateToTransport(x),
 *      eventId: eventId,
 *      calendarId: calendarId
 *    }
 *
 */
function Busytime() {
  Abstract.apply(this, arguments);
  this._setupCache();

  denodeifyAll(this, [
    'removeEvent',
    'loadSpan'
  ]);
}
module.exports = Busytime;

Busytime.prototype = {
  __proto__: Abstract.prototype,

  _store: 'busytimes',

  _dependentStores: ['alarms', 'busytimes'],

  _setupCache: function() {
    // reset time observers
    TimeObserver.call(this);

    this._byEventId = Object.create(null);
  },

  _createModel: function(input, id) {
    return this.initRecord(input, id);
  },

  initRecord: function(input, id) {
    var _super = Abstract.prototype._createModel;
    var model = _super.apply(this, arguments);
    model.startDate = Calc.dateFromTransport(model.start);
    model.endDate = Calc.dateFromTransport(model.end);
    return model;
  },

  _removeDependents: function(id, trans) {
    this.db.getStore('Alarm').removeByIndex('busytimeId', id, trans);
  },

  removeEvent: function(id, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = undefined;
    }

    if (typeof(trans) === 'undefined') {
      trans = this.db.transaction(
        this._dependentStores,
        'readwrite'
      );
    }

    // build the request using the inherited method
    var req = this.removeByIndex('eventId', id, trans);

    // get the original method which handles the generic bit
    var success = req.onsuccess;

    // override the default .onsuccess to get the ids
    // so we can emit remove events.
    var self = this;
    req.onsuccess = function(e) {
      var cursor = e.target.result;

      if (cursor) {
        var id = cursor.primaryKey;
        self.emit('remove', id);
      }

      success(e);
    };

    this._transactionCallback(trans, callback);
  },

  _startCompare: function(aObj, bObj) {
    var a = aObj.start.utc;
    var b = bObj.start.utc;
    return compare(a, b);
  },

  /**
   * Loads all busytimes in given timespan.
   *
   * @param {Calendar.Timespan} span timespan.
   * @param {Function} callback node style callback
   *                            where first argument is
   *                            an error (or null)
   *                            and the second argument
   *                            is a list of all loaded
   *                            busytimes in the timespan.
   */
  loadSpan: function(span, callback) {
    var trans = this.db.transaction(this._store);
    var store = trans.objectStore(this._store);

    var startPoint = Calc.dateToTransport(new Date(span.start));
    var endPoint = Calc.dateToTransport(new Date(span.end));

    // XXX: we need to implement busytime chunking
    // to make this efficient.
    var keyRange = IDBKeyRange.lowerBound(startPoint.utc);

    var index = store.index('end');
    var self = this;

    index.mozGetAll(keyRange).onsuccess = function(e) {
      var data = e.target.result;

      // sort data
      data = data.sort(self._startCompare);

      // attempt to find a start time that occurs
      // after the end time of the span
      var idx = binsearch.insert(
        data,
        { start: { utc: endPoint.utc + 1 } },
        self._startCompare
      );

      // remove unrelated timespan...
      data = data.slice(0, idx);

      // fire callback
      if (callback) {
        callback(null, data.map(function(item) {
          return self.initRecord(item);
        }));
      }

    };
  },

  /* we don't use id based caching for busytimes */

  _addToCache: function() {},
  _removeFromCache: function() {}

};

});

define('models/calendar',['require','exports','module'],function(require, exports, module) {


function Cal(options) {
  if (typeof(options) === 'undefined') {
    options = {};
  }

  this.remote = {};

  for (var key in options) {
    if (options.hasOwnProperty(key)) {
      this[key] = options[key];
    }
  }
}
module.exports = Cal;

Cal.prototype = {

  /**
   * Local copy of calendars remote state.
   * Taken from a calendar providers .toJSON method.
   *
   * @type {Object}
   */
  remote: null,

  /**
   * The date at which this calendar's events
   * where synchronized.
   *
   * @type {Date}
   */
  firstEventSyncDate: null,

  /**
   * Last sync token used in previous
   * event synchronization.
   *
   * @type {String}
   */
  lastEventSyncToken: '',

  /**
   * Last date of event synchronization.
   * This is not going to be used
   * for any kind of serious operation
   * right now this is just for the UI.
   *
   * @type {Date}
   */
  lastEventSyncDate: '',

  /**
   * Indicates if calendar is displayed
   * locally in the ui.
   *
   * @type {Boolean}
   */
  localDisplayed: true,

  /**
   * Id of account this record
   * is associated with.
   */
  accountId: '',

  /**
   * Updates remote with data from a calendar provider.
   *
   * @param {Calendar.Provider.Calendar.Abstract} provider remote.
   */
  updateRemote: function(provider) {
    var data = provider;
    if ('toJSON' in provider) {
        data = provider.toJSON();
    }

    this.remote = data;
  },

  /**
   * Checks if local and remote state differ
   * via sync tokens. Returns true when
   * local sync token and remote do not match.
   * Does not account for local changes only
   * when the server state has changed
   * and we have not yet synchronized.
   *
   * @return {Boolean} true when sync needed.
   */
  eventSyncNeeded: function() {
    var local = this.lastEventSyncToken;
    var remote = this.remote.syncToken;

    return local != remote;
  },

  set name(name) {
    this.remote.name = name;
    return this.remote.name;
  },

  set color(color) {
    this.remote.color = color;
    return this.remote.color;
  },

  set description(description) {
    this.remote.description = description;
    return this.remote.description;
  },

  get name() {
    return this.remote.name;
  },

  get color() {
    var color = this.remote.color;
    if (color) {
      if (color.substr(0, 1) === '#') {
        return color.substr(0, 7);
      }
    }
    return this.remote.color;
  },

  get description() {
    return this.remote.description;
  },

  toJSON: function() {
    var result = {
      error: this.error,
      remote: this.remote,
      accountId: this.accountId,
      localDisplayed: this.localDisplayed,
      lastEventSyncDate: this.lastEventSyncDate,
      lastEventSyncToken: this.lastEventSyncToken,
      firstEventSyncDate: this.firstEventSyncDate
    };

    if (this._id || this._id === 0) {
      result._id = this._id;
    }

    return result;
  }

};

});

define('store/calendar',['require','exports','module','./abstract','models/calendar','provider/local','promise','probably_parse_int','provider/provider_factory'],function(require, exports, module) {


var Abstract = require('./abstract');
var CalendarModel = require('models/calendar');
var Local = require('provider/local');
var denodeifyAll = require('promise').denodeifyAll;
var probablyParseInt = require('probably_parse_int');
var providerFactory = require('provider/provider_factory');

function Store() {
  Abstract.apply(this, arguments);
  this._usedColors = [];

  denodeifyAll(this, [
    'markWithError',
    'remotesByAccount',
    'sync',
    'providerFor',
    'ownersOf'
  ]);
}
module.exports = Store;

/**
 * Remote calendar colors
 */
Store.REMOTE_COLORS = [
  '#f53d16',
  '#644237',
  '#8c8c8c',
  '#4f6a79',
  '#e82928',
  '#da0051',
  '#af0779',
  '#840fa2',
  '#5026aa',
  '#2f3da7',
  '#2383f2',
  '#1e98f2',
  '#25afca',
  '#1e8675',
  '#47a13a',
  '#7eb832',
  '#c3d51c',
  '#fde61b',
  '#fab200'
];

/**
 * Local calendar color (orange)
 */
Store.LOCAL_COLOR = '#ff8329',

/**
 * List of possible calendar capabilities.
 */
Store.capabilities = {
  createEvent: 'canCreateEvent',
  updateEvent: 'canUpdateEvent',
  deleteEvent: 'canDeleteEvent'
};

Store.prototype = {
  __proto__: Abstract.prototype,

  _store: 'calendars',

  _dependentStores: [
    'calendars', 'events', 'busytimes',
    'alarms', 'icalComponents'
  ],

  _parseId: probablyParseInt,

  _createModel: function(obj, id) {
    if (!(obj instanceof CalendarModel)) {
      obj = new CalendarModel(obj);
    }

    if (typeof(id) !== 'undefined') {
      obj._id = id;
    }

    return obj;
  },

  _removeDependents: function(id, trans) {
    var store = this.db.getStore('Event');
    store.removeByIndex('calendarId', id, trans);
  },

  /**
   * Marks a given calendar with an error.
   *
   * Emits a 'error' event immediately.. This method is typically
   * triggered by an account wide error.
   *
   *
   * @param {Object} calendar model.
   * @param {Calendar.Error} error for given calendar.
   * @param {IDBTransaction} transaction optional.
   * @param {Function} callback fired when model is saved [err, id, model].
   */
  markWithError: function(calendar, error, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = null;
    }

    if (!calendar._id) {
      throw new Error('given calendar must be persisted.');
    }

    calendar.error = {
      name: error.name,
      date: new Date()
    };

    this.persist(calendar, trans, callback);
  },

  persist: function(calendar, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = undefined;
    }

    this._updateCalendarColor(calendar);

    var cb = callback;
    var cached = this._cached[calendar._id];

    if (cached && cached.localDisplayed !== calendar.localDisplayed) {
      cb = function(err, id, model) {
        this.emit('calendarVisibilityChange', id, model);
        callback(err, id, model);
      }.bind(this);
    }

    Abstract.prototype.persist.call(this, calendar, trans, cb);
  },

  remove: function(id, trans, callback) {
    this._removeCalendarColorFromCache(id);
    Abstract.prototype.remove.apply(this, arguments);
  },

  _updateCalendarColor: function(calendar) {
    // we avoid storing multiple colors for same calendar in case of an
    // "update" operation
    this._removeCalendarColorFromCache(calendar._id);
    this._setCalendarColor(calendar);
    // cache is built asynchronously, we need to store the color as soon as
    // possible to avoid adding same color multiple times in a row (eg.
    // account with multiple calendars will call persist multiple times)
    this._usedColors.push(calendar.color);
  },

  _removeCalendarColorFromCache: function(id) {
    // we need to remove the color from index as soon as possible to avoid
    // race conditions (remove is async)
    var color = this.getColorByCalendarId(id);
    var index = this._usedColors.indexOf(color);
    if (index !== -1) {
      this._usedColors.splice(index, 1);
    }
  },

  getColorByCalendarId: function(id) {
    return this._cached[id] && this._cached[id].color;
  },

  _setCalendarColor: function(calendar) {
    // local calendar should always use the same color
    if (calendar._id === Local.calendarId) {
      calendar.color = Store.LOCAL_COLOR;
      return;
    }

    // restore previous color only if it is part of the palette, otherwise we
    // get the next available color (or least used)
    var prevColor = this.getColorByCalendarId(calendar._id);
    if (prevColor && Store.REMOTE_COLORS.indexOf(prevColor) !== -1) {
      calendar.color = prevColor;
    } else {
      calendar.color = this._getNextColor();
    }
  },

  _getNextColor: function() {
    var available = Store.REMOTE_COLORS.filter(function(color) {
      return this._usedColors.indexOf(color) === -1;
    }, this);

    return available.length ? available[0] : this._getLeastUsedColor();
  },

  _getLeastUsedColor: function() {
    var counter = {};
    this._usedColors.forEach(function(color) {
      counter[color] = (counter[color] || 0) + 1;
    });

    var leastUsedColor;
    var leastUsedCount = Infinity;
    for (var color in counter) {
      if (counter[color] < leastUsedCount) {
        leastUsedCount = counter[color];
        leastUsedColor = color;
      }
    }

    return leastUsedColor;
  },

  shouldDisplayCalendar: function(calendarId) {
    var calendar = this._cached[calendarId];
    return calendar && calendar.localDisplayed;
  },

  /**
   * Find calendars in a specific account.
   * Results will be returned in an object where
   * the key is the remote.id and the value is the calendar.
   *
   * @param {String|Numeric} accountId id of account.
   * @param {Function} callback [err, object] see above.
   */
  remotesByAccount: function(accountId, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = null;
    }

    if (!trans) {
      trans = this.db.transaction(this._store);
    }

    var store = trans.objectStore(this._store);

    var reqKey = IDBKeyRange.only(accountId);
    var req = store.index('accountId').mozGetAll(reqKey);

    req.onerror = function remotesError(e) {
      callback(e.target.error);
    };

    var self = this;
    req.onsuccess = function remotesSuccess(e) {
      var result = Object.create(null);
      e.target.result.forEach(function(calendar) {
        result[calendar.remote.id] = self._createModel(
          calendar,
          calendar._id
        );
      });

      callback(null, result);
    };
  },

  /**
   * Sync remote and local events for a calendar.
   *
   * TODO: Deprecate use of this function in favor of a sync methods
   *       inside of providers.
   */
  sync: function(account, calendar, callback) {
    var provider = providerFactory.get(account.providerType);
    provider.syncEvents(account, calendar, callback);
  },

  /**
   * Shortcut to find provider for calendar.
   *
   * @param {Calendar.Models.Calendar} calendar input calendar.
   * @param {Function} callback [err, provider].
   */
  providerFor: function(calendar, callback) {
    this.ownersOf(calendar, function(err, owners) {
      if (err) {
        return callback(err);
      }

      callback(null, providerFactory.get(owners.account.providerType));
    });
  },

  /**
   * Finds calendar/account for a given event.
   *
   * TODO: think about moving this function into its
   * own file as a mixin.
   *
   * @param {Object|String|Numeric} objectOrId must contain .calendarId.
   * @param {Function} callback [err, { ... }].
   */
  ownersOf: function(objectOrId, callback) {
    var result = {};

    var accountStore = this.db.getStore('Account');

    // case 1. given a calendar
    if (objectOrId instanceof CalendarModel) {
      result.calendar = objectOrId;
      accountStore.get(objectOrId.accountId, fetchAccount);
      return;
    }

    // case 2 given a calendar id or object

    if (typeof(objectOrId) === 'object') {
      objectOrId = objectOrId.calendarId;
    }

    // why??? because we use this method in event store too..
    var calendarStore = this.db.getStore('Calendar');
    calendarStore.get(objectOrId, fetchCalendar);

    function fetchCalendar(err, calendar) {
      if (err) {
        return callback(err);
      }

      result.calendar = calendar;
      accountStore.get(calendar.accountId, fetchAccount);
    }

    function fetchAccount(err, account) {
      if (err) {
        return callback(err);
      }

      result.account = account;
      callback(null, result);
    }
  }
};

});

define('store/event',['require','exports','module','./abstract','calc','./calendar','promise','provider/provider_factory'],function(require, exports, module) {


var Abstract = require('./abstract');
var Calc = require('calc');
var Calendar = require('./calendar');
var denodeifyAll = require('promise').denodeifyAll;
var providerFactory = require('provider/provider_factory');

function Events() {
  Abstract.apply(this, arguments);

  denodeifyAll(this, [
    'providerFor',
    'findByIds',
    'ownersOf',
    'eventsForCalendar'
  ]);
}
module.exports = Events;

Events.prototype = {
  __proto__: Abstract.prototype,
  _store: 'events',
  _dependentStores: ['events', 'busytimes', 'alarms', 'icalComponents'],

  /** disable caching */
  _addToCache: function() {},
  _removeFromCache: function() {},

  _createModel: function(input, id) {
    var _super = Abstract.prototype._createModel;
    var model = _super.apply(this, arguments);
    model.remote.startDate = Calc.dateFromTransport(model.remote.start);
    model.remote.endDate = Calc.dateFromTransport(model.remote.end);
    return model;
  },

  /**
   * Link busytime dependants see _addDependents.
   */
  _removeDependents: function(id, trans) {
    this.removeByIndex('parentId', id, trans);

    var busy = this.db.getStore('Busytime');
    busy.removeEvent(id, trans);

    var component = this.db.getStore('IcalComponent');
    component.remove(id, trans);
  },

  /**
   * Generate an id for a newly created record.
   * Based off of remote id (uuid) and calendar id.
   */
  _assignId: function(obj) {
    var id = obj.calendarId + '-' + obj.remote.id;
    obj._id = id;
    return id;
  },

  /**
   * Shortcut finds provider for given event.
   *
   * @param {Object} event full event record from db.
   */
  providerFor: function(event, callback) {
    this.ownersOf(event, function(err, owners) {
      callback(null, providerFactory.get(owners.account.providerType));
    });
  },

  /**
   * Finds a list of events by id.
   *
   * @param {Array} ids list of ids.
   * @param {Function} callback node style second argument
   *                            is an object of _id/event.
   */
  findByIds: function(ids, callback) {
    var results = {};
    var pending = ids.length;
    var self = this;

    if (!pending) {
      callback(null, results);
    }

    function next() {
      if (!(--pending)) {
        // fatal errors should break
        // and so we are not handling them
        // here...
        callback(null, results);
      }
    }

    function success(e) {
      var item = e.target.result;

      if (item) {
        results[item._id] = self._createModel(item);
      }

      next();
    }

    function error() {
      // can't find it or something
      // skip!
      next();
    }

    ids.forEach(function(id) {
      var trans = this.db.transaction('events');
      var store = trans.objectStore('events');
      var req = store.get(id);

      req.onsuccess = success;
      req.onerror = error;
    }, this);
  },

  /**
   * Finds calendar/account for a given event.
   *
   * @param {Object} event must contain .calendarId.
   * @param {Function} callback [err, { ... }].
   */
  ownersOf: Calendar.prototype.ownersOf,
  /**
   * Loads all events for given calendarId
   * and returns results. Does not cache.
   *
   * @param {String} calendarId calendar to find.
   * @param {Function} callback node style [err, array of events].
   */
  eventsForCalendar: function(calendarId, callback) {
    var trans = this.db.transaction('events');
    var store = trans.objectStore('events');
    var index = store.index('calendarId');
    var key = IDBKeyRange.only(calendarId);

    var req = index.mozGetAll(key);

    req.onsuccess = function(e) {
      callback(null, e.target.result);
    };

    req.onerror = function(e) {
      callback(e);
    };
  }
};

});

define('store/ical_component',['require','exports','module','./abstract','calc','promise'],function(require, exports, module) {


var Abstract = require('./abstract');
var Calc = require('calc');
var denodeifyAll = require('promise').denodeifyAll;

function IcalComponent() {
  Abstract.apply(this, arguments);

  denodeifyAll(this, [
    'findRecurrencesBefore'
  ]);
}
module.exports = IcalComponent;

IcalComponent.prototype = {
  __proto__: Abstract.prototype,

  _store: 'icalComponents',

  /** disable caching */
  _addToCache: function() {},
  _removeFromCache: function() {},

  _createModel: function(object) {
    return object;
  },

  _detectPersistType: function(object) {
    // always fire update.
    return 'update';
  },

  /**
   * Finds all components which have recurrences
   * that are not expanded beyond the given date.
   *
   * @param {Date} maxDate max date to find.
   * @param {Function} callback results of search [err, [icalComp, ...]].
   */
  findRecurrencesBefore: function(maxDate, callback) {
    var trans = this.db.transaction(this._store, 'readwrite');

    trans.onerror = function(event) {
      callback(event.target.error.name);
    };

    var time = Calc.dateToTransport(maxDate);
    var utc = time.utc;
    var range = IDBKeyRange.bound(0, utc);
    var store = trans.objectStore(this._store);
    var idx = store.index('lastRecurrenceId');

    var req = idx.mozGetAll(range);

    req.onsuccess = function(event) {
      callback(null, event.target.result);
    };
  }
};

});

define('store/setting',['require','exports','module','./abstract','promise','next_tick'],function(require, exports, module) {


var Abstract = require('./abstract');
var denodeifyAll = require('promise').denodeifyAll;
var nextTick = require('next_tick');

function Setting() {
  Abstract.apply(this, arguments);
  denodeifyAll(this, [
    'getValue',
    'set'
  ]);
}
module.exports = Setting;

Setting.prototype = {
  __proto__: Abstract.prototype,

  _store: 'settings',

  /**
   * Default option values.
   */
  defaults: {
    standardAlarmDefault: -300,
    alldayAlarmDefault: 32400,
    syncFrequency: 15,
    syncAlarm: {
      alarmId: null,
      start: null,
      end: null
    }
  },

  /** disable caching */
  _addToCache: function() {},
  _removeFromCache: function() {},

  /**
   * This method also will use the internal cache to ensure
   * callers are in a consistent state and don't require round
   * trips to the database. When the value does not exist defaults
   * are used where possible...
   *
   *
   *    settings.getValue('syncFrequency', function(err, value) {
   *      // ...
   *    });
   *
   *
   * @param {String} key name of setting.
   * @param {Function} callback usual [err, value] does not include metadata.
   */
  getValue: function(key, callback) {
    var self = this;

    if (key in this._cached) {
      nextTick(function handleCached() {
        callback(null, self._cached[key].value);
      });

      // we have cached value exit...
      return;
    }

    this.get(key, function handleStored(err, value) {
      if (err) {
        return callback(err);
      }

      if (value === undefined) {
        if (self.defaults[key] !== undefined) {
          value = { value: self.defaults[key] };
          self._cached[key] = value;
          return callback(null, value.value);
        } else {
          return callback(new Error('Can not get value with ' + key));
        }
      } else {
        self._cached[key] = value;
        return callback(null, value.value);
      }
    });
  },

  /**
   * Persist a setting change.
   *
   * In addition to updating the value of the setting
   * it will also update the updatedAt & createdAt properties
   * of the record.
   *
   * Calling this function will also emit a 'change' event
   * prior to fully persisting the record to the database.
   *
   * Example:
   *
   *    var settingStore;
   *
   *    settingStore.set('syncFrequency', 15, function() {
   *      // done
   *    });
   *
   *    // somewhere else in the app:
   *
   *    settingStore.on('syncFrequencyChange', function(value) {
   *      // value === 15
   *    });
   *
   * @param {String} key name of setting.
   * @param {Object} value any object that IndexedDb can store.
   * @param {IDBTransaction} [trans] idb transaction optional.
   * @param {Function} [callback] optional callback.
   */
  set: function(key, value, trans, callback) {
    if (typeof(trans) === 'function') {
      callback = trans;
      trans = null;
    }

    var cached = this._cached[key];
    var record;

    if (cached && cached._id) {
      cached.value = value;
      cached.updatedAt = new Date();
      record = cached;
    } else {
      var created = new Date();
      this._cached[key] = record = {
        _id: key,
        createdAt: created,
        updatedAt: created,
        value: value
      };
    }

    this.emit(key + 'Change', value, record);
    this.persist(record, trans, callback);
  }

};

});

define('store/store',['require','exports','module','./abstract','./account','./alarm','./busytime','./calendar','./event','./ical_component','./setting'],function(require, exports) {


exports.Abstract = require('./abstract');
exports.Account = require('./account');
exports.Alarm = require('./alarm');
exports.Busytime = require('./busytime');
exports.Calendar = require('./calendar');
exports.Event = require('./event');
exports.IcalComponent = require('./ical_component');
exports.Setting = require('./setting');

});

/* jshint loopfunc: true */
define('db',['require','exports','module','models/account','presets','provider/local','responder','store/store','debug','next_tick','probably_parse_int','ext/uuid'],function(require, exports, module) {


var Account = require('models/account');
var Presets = require('presets');
var Local = require('provider/local');
var Responder = require('responder');
var Store = require('store/store');
var debug = require('debug')('db');
var nextTick = require('next_tick');
var probablyParseInt = require('probably_parse_int');
var uuid = require('ext/uuid');

var idb = window.indexedDB;

const VERSION = 15;

var store = Object.freeze({
  events: 'events',
  accounts: 'accounts',
  calendars: 'calendars',
  busytimes: 'busytimes',
  settings: 'settings',
  alarms: 'alarms',
  icalComponents: 'icalComponents'
});

function Db(name, app) {
  this.app = app;
  this.name = name;
  this._stores = Object.create(null);
  Responder.call(this);
  this._upgradeOperations = [];
}
module.exports = Db;

Db.prototype = {
  __proto__: Responder.prototype,

  /**
   * Database connection
   */
  connection: null,

  getStore: function(name) {
    if (!(name in this._stores)) {
      try {
        this._stores[name] = new Store[name](this, this.app);
      } catch (e) {
        console.error('Error', e.name, e.message);
        console.error('Failed to load store', name, e.stack);
      }
    }

    return this._stores[name];
  },

  load: function(callback) {
    debug('Will load b2g-calendar db.');

    var self = this;
    function setupDefaults() {
      if (self.oldVersion < 8) {
        self._setupDefaults(callback);
      } else {
        nextTick(callback);
      }
    }

    if (this.isOpen) {
      return setupDefaults();
    }
    this.open(VERSION, setupDefaults);
  },


  /**
   * Opens connection to database.
   *
   * @param {Numeric} [version] version of database to open.
   *                            default to current version.
   *                            Should _only_ be used in testing.
   *
   * @param {Function} [callback] first argument is error, second
   *                            is result of operation or null
   *                            in the error case.
   */
  open: function(version, callback) {
    if (typeof(version) === 'function') {
      callback = version;
      version = VERSION;
    }

    var req = idb.open(this.name, version);
    this.version = version;

    var self = this;

    req.onsuccess = function() {
      self.isOpen = true;
      self.connection = req.result;

      // if we have pending upgrade operations
      if (self._upgradeOperations.length) {
        var pending = self._upgradeOperations.length;

        var operation;
        while ((operation = self._upgradeOperations.shift())) {
          operation.call(self, function next() {
            if (!(--pending)) {
              callback(null, self);
              self.emit('open', self);
            }
          });
        }
      } else {
        callback(null, self);
        self.emit('open', self);
      }
    };

    req.onblocked = function(error) {
      callback(error, null);
      self.emit('error', error);
    };

    req.onupgradeneeded = function(event) {
      self._handleVersionChange(req.result, event);
    };

    req.onerror = function(error) {
      //TODO: steal asuth's error handling...
      callback(error, null);
      self.emit('error', error);
    };
  },

  transaction: function(list, state) {
    var names;
    var self = this;

    if (typeof(list) === 'string') {
      names = [];
      names.push(this.store[list] || list);
    } else {
      names = list.map(function(name) {
        return self.store[name] || name;
      });
    }

    return this.connection.transaction(names, state || 'readonly');
  },

  _handleVersionChange: function(db, event) {
    var newVersion = event.newVersion;
    var curVersion = event.oldVersion;
    var transaction = event.currentTarget.transaction;

    this.hasUpgraded = true;
    this.oldVersion = curVersion;
    this.upgradedVersion = newVersion;

    for (; curVersion < newVersion; curVersion++) {
      // if version is < 7 then it was from pre-production
      // db and we can safely discard its information.
      if (curVersion < 6) {
        // ensure clean state if this was an old db.
        var existingNames = db.objectStoreNames;
        for (var i = 0; i < existingNames.length; i++) {
          db.deleteObjectStore(existingNames[i]);
        }

        // version 0-r are not maintained increment to 6
        curVersion = 6;

        // busytimes has one event, has one calendar
        var busytimes = db.createObjectStore(
          store.busytimes,
          { keyPath: '_id' }
        );

        busytimes.createIndex(
          'end',
          'end.utc',
          { unique: false, multiEntry: false }
        );

        busytimes.createIndex(
          'eventId',
          'eventId',
          { unique: false, multiEntry: false }
        );

        // events -> belongs to calendar
        var events = db.createObjectStore(
          store.events,
          { keyPath: '_id' }
        );

        events.createIndex(
          'calendarId',
          'calendarId',
          { unique: false, multiEntry: false }
        );

        events.createIndex(
          'parentId',
          'parentId',
          { unique: false, multiEntry: false }
        );

        // accounts -> has many calendars
        db.createObjectStore(
          store.accounts, { keyPath: '_id', autoIncrement: true }
        );

        // calendars -> has many events
        db.createObjectStore(
          store.calendars, { keyPath: '_id', autoIncrement: true }
        );

      } else if (curVersion === 7) {
        db.createObjectStore(store.settings, { keyPath: '_id' });
      } else if (curVersion === 8) {
        var alarms = db.createObjectStore(
          store.alarms, { keyPath: '_id', autoIncrement: true }
        );

        alarms.createIndex(
          'trigger',
          'trigger.utc',
          { unique: false, multiEntry: false }
        );

        alarms.createIndex(
          'busytimeId',
          'busytimeId',
          { unique: false, multiEntry: false }
        );
     } else if (curVersion === 12) {
        var icalComponents = db.createObjectStore(
          store.icalComponents, { keyPath: 'eventId', autoIncrement: false }
        );

        icalComponents.createIndex(
          'lastRecurrenceId',
          'lastRecurrenceId.utc',
          { unique: false, multiEntry: false }
        );
      } else if (curVersion === 13) {
        var calendarStore = transaction.objectStore(store.calendars);
        calendarStore.createIndex(
          'accountId', 'accountId', { unique: false, multiEntry: false }
        );
      } else if (curVersion === 14) {
        // Bug 851003 - The database may have some busytimes and/or events
        // which have their calendarId field as a string rather than an int.
        // We need to fix the calendarIds and also remove any of the idb
        // objects that have deleted calendars.
        this.sanitizeEvents(transaction);
      }
    }
  },

  /**
   * 1. Find all events with string calendar ids and index them.
   * 2. Check for each of the events whether the calendar
   *    they reference still exists.
   * 3. Fix the events' calendarIds if the calendar still exists else
   *    delete them.
   * @param {IDBTransaction} trans The active idb transaction during db
   *     upgrade.
   */
  sanitizeEvents: function(trans) {
    /**
     * Map from calendar ids to lists of event ids
     * which we've fixed with that id.
     * @type {Object.<number, Array.<number>>}
     */
    var badCalendarIdToEventIds = {};

    var objectStore = trans.objectStore(store.events);
    objectStore.openCursor().onsuccess = (function(evt) {
      var cursor = evt.target.result;
      if (!cursor) {
        return this._updateXorDeleteEvents(badCalendarIdToEventIds, trans);
      }

      var calendarId = cursor.value.calendarId;
      if (typeof(calendarId) === 'number') {
        // Nothing to do here!
        return cursor.continue();
      }

      // Record the bad reference.
      var eventIds = badCalendarIdToEventIds[calendarId] || [];
      eventIds.push(cursor.key);
      badCalendarIdToEventIds[calendarId] = eventIds;
      cursor.continue();
    }).bind(this);
  },

  /**
   * 1. Check for each of the events whether the calendar
   *    they reference still exists.
   * 2. Fix the events' calendarIds if the calendar still exists else
   *    delete them.
   *
   * @param {Object.<number, Array.<number>>} badCalendarIdToEventIds Map
   *     from calendar ids to lists of event ids which we've fixed with
   *     that id.
   * @param {IDBTransaction} trans The active idb transaction during db
   *     upgrade.
   * @private
   */
  _updateXorDeleteEvents: function(badCalendarIdToEventIds, trans) {
    var calendarIds = Object.keys(badCalendarIdToEventIds);
    calendarIds.forEach(function(calendarId) {
      //Bug 887698 cases for calendarIds of types strings or integers
      calendarId = probablyParseInt(calendarId);
      var eventIds = badCalendarIdToEventIds[calendarId];
      var calendars = trans.objectStore(store.calendars);
      calendars.get(calendarId).onsuccess = (function(evt) {
        var result = evt.target.result;
        if (result) {
          this._updateEvents(eventIds, calendarId, trans);
        } else {
          this._deleteEvents(eventIds, trans);
        }
      }).bind(this);
    }, this);
  },

  /**
   * Update a collection of events and the busytimes that depend on them.
   *
   * @param {Array.<number>>} eventIds An array of event ids for the events.
   * @param {number} calendarId A numerical id to set as calendarId.
   * @param {IDBTransaction} trans The active idb transaction during db
   *     upgrade.
   * @private
   */
  _updateEvents: function(eventIds, calendarId, trans) {
    var eventStore = trans.objectStore(store.events);
    var busytimeStore = trans.objectStore(store.busytimes);
    var busytimeStoreIndexedByEventId = busytimeStore.index('eventId');

    eventIds.forEach(function(eventId) {
      eventStore.get(eventId).onsuccess = function(evt) {
        var result = evt.target.result;
        result.calendarId = calendarId;
        eventStore.put(result);
      };

      busytimeStoreIndexedByEventId.get(eventId).onsuccess = function(evt) {
        var result = evt.target.result;
        result.calendarId = calendarId;
        busytimeStore.put(result);
      };
    });
  },

  /**
   * Delete a collection of events and the busytimes that depend on them.
   *
   * @param {Array.<number>>} eventIds An array of event ids for the events.
   * @param {IDBTransaction} trans The active idb transaction during db
   *     upgrade.
   * @private
   */
  _deleteEvents: function(eventIds, trans) {
    var events = this.getStore('Event');
    eventIds.forEach(function(eventId) {
      events.remove(eventId, trans);
    });
  },

  get store() {
    return store;
  },

  /**
   * Shortcut method for connection.close
   */
  close: function() {
    if (this.connection) {
      this.isOpen = false;
      this.connection.close();
      this.connection = null;
    }
  },

  clearNonCredentials: function(callback) {
    var stores = ['events', 'busytimes'];
    var trans = this.transaction(
      stores,
      'readwrite'
    );

    trans.addEventListener('complete', callback);

    stores.forEach(function(store) {
      store = trans.objectStore(store);
      store.clear();
    });
  },

  /**
   * Setup default values for initial calendar load.
   */
  _setupDefaults: function(callback) {
    debug('Will setup defaults.');
    var calendarStore = this.getStore('Calendar');
    var accountStore = this.getStore('Account');

    var trans = calendarStore.db.transaction(
      ['accounts', 'calendars'],
      'readwrite'
    );

    if (callback) {
      trans.addEventListener('error', function(err) {
        callback(err);
      });

      trans.addEventListener('complete', function() {
        callback();
      });
    }

    var options = Presets.local.options;
    debug('Creating local calendar with options:', options);
    var account = new Account(options);
    account.preset = 'local';
    account._id = uuid();

    var calendar = {
      _id: Local.calendarId,
      accountId: account._id,
      remote: Local.defaultCalendar()
    };

    accountStore.persist(account, trans);
    calendarStore.persist(calendar, trans);
  },

  deleteDatabase: function(callback) {
    var req = idb.deleteDatabase(this.name);

    req.onblocked = function() {
      // improve interface
      callback(new Error('blocked'));
    };

    req.onsuccess = function(event) {
      callback(null, event);
    };

    req.onerror = function(event) {
      callback(event, null);
    };
  }
};

});

define('controllers/error',['require','exports','module','error','error','responder','next_tick','notification'],function(require, exports, module) {


var Authentication = require('error').Authentication;
var InvalidServer = require('error').InvalidServer;
var Responder = require('responder');
var nextTick = require('next_tick');
var notification = require('notification');

/**
 * Global error handler / default handling for errors.
 *
 * @param {Calendar.App} app current application.
 */
function ErrorController(app) {
  Responder.call(this);

  this.app = app;
  this._handlers = Object.create(null);
}
module.exports = ErrorController;

ErrorController.prototype = {
  __proto__: Responder.prototype,

  /**
   * URL in which account errors are dispatched to.
   */
  accountErrorUrl: '/update-account/',

  /**
   * Dispatch an error event.
   *
   * If this type of event has been captured will be dispatched directly to
   * the callback provided. Otherwise the default behaviour will be triggered.
   *
   * @param {Calendar.Error} error to dispatch.
   */
  dispatch: function(error) {
    if (error instanceof Authentication || error instanceof InvalidServer) {
      this.handleAuthenticate(error.detail.account);
    }

    this.emit('error', error);
  },

  /**
   * Default handler for authentication errors.
   *
   * @param {Object} account to notify user about.
   * @param {Function} [callback] optional callback.
   */
  handleAuthenticate: function(account, callback) {
    if (!account) {
      return console.error('attempting to trigger reauth without an account');
    }

    // only trigger notification the first time there is an error.
    if (!account.error || account.error.count !== 1) {
      return nextTick(callback);
    }

    var lock = navigator.requestWakeLock('cpu');

    var l10n = navigator.mozL10n;
    var title = l10n.get('notification-error-sync-title');
    var description = l10n.get('notification-error-sync-description');

    var url = this.accountErrorUrl + account._id;
    notification.sendNotification(title, description, url).then(() => {
      callback && callback();
      lock.unlock();
    });
  }
};

});

define('pending_manager',['require','exports','module'],function(require, exports, module) {


function PendingManager() {
  this.objects = [];
  this.pending = 0;
  this.onstart = this.onstart.bind(this);
  this.onend = this.onend.bind(this);
}
module.exports = PendingManager;

PendingManager.prototype = {
  register: function(object) {
    object.on(object.startEvent, this.onstart);
    object.on(object.completeEvent, this.onend);

    var wasPending = this.isPending();
    this.objects.push(object);
    if (object.pending) {
      this.pending++;

      if (!wasPending) {
        this.onpending && this.onpending();
      }
    }
  },

  /**
   * Unregister an object.
   * Note it is intended that objects that
   * are unregistered are never in a state
   * where we are waiting for their pending
   * status to complete. If an incomplete
   * object is removed it will break .pending.
   */
  unregister: function(object) {
    var idx = this.objects.indexOf(object);
    if (idx !== -1) {
      this.objects.splice(idx, 1);
      return true;
    }

    return false;
  },

  isPending: function() {
    return this.objects.some((object) => {
      return object.pending;
    });
  },

  onstart: function() {
    if (!this.pending) {
      this.onpending && this.onpending();
    }

    this.pending++;
  },

  onend: function() {
    this.pending--;
    if (!this.pending) {
      this.oncomplete && this.oncomplete();
    }
  }
};

});

define('controllers/recurring_events',['require','exports','module','responder','debug','next_tick','provider/provider_factory'],function(require, exports, module) {


var Responder = require('responder');
var debug = require('debug')('controllers/recurring_events');
var nextTick = require('next_tick');
var providerFactory = require('provider/provider_factory');

function RecurringEvents(app) {
  this.app = app;
  this.accounts = app.store('Account');
  Responder.call(this);
}
module.exports = RecurringEvents;

RecurringEvents.prototype = {
  __proto__: Responder.prototype,

  startEvent: 'expandStart',
  completeEvent: 'expandComplete',

  /**
   * Adds N number of days to the window to expand
   * events until. Its very important for this number
   * to be greater then the maximum number of days displayed
   * in the month view (or a view with more days) otherwise
   * the view may be loaded without actually expanding all
   * the visible days.
   *
   * @type Number
   */
  paddingInDays: 85,

  /**
   * Amount of time (in MS) to wait between triggering
   * the recurring event expansions.
   */
  waitBeforeMove: 750,

  /**
   * We need to limit the number of tries on expansions
   * otherwise its possible we never complete during error
   * or long recurring event.
   */
  maximumExpansions: 25,

  /**
   * private timeout (as in setTimeout id) use with waitBeforeMove.
   */
  _moveTimeout: null,

  /**
   * True when queue is running...
   */
  pending: false,

  unobserve: function() {
    this.app.timeController.removeEventListener(
      'monthChange',
      this
    );

    this.app.syncController.removeEventListener(
      'syncComplete',
      this
    );
  },

  observe: function() {
    var time = this.app.timeController;

    // expand initial time this is necessary
    // for cases where user has device off for long periods of time.
    if (time.position) {
      this.queueExpand(time.position);
    }

    // register observers
    time.on('monthChange', this);

    // we must re-expand after sync so events at least
    // expand to the current position....
    this.app.syncController.on('syncComplete', this);
  },

  handleEvent: function(event) {
    switch (event.type) {
      case 'syncComplete':
        this.queueExpand(
          this.app.timeController.position
        );
        break;

      case 'monthChange':
        if (this._moveTimeout !== null) {
          clearTimeout(this._moveTimeout);
          this._moveTimeout = null;
        }

        // trigger the event queue when we move
        this._moveTimeout = setTimeout(
          // data[0] is the new date.
          this.queueExpand.bind(this, event.data[0]),
          this.waitBeforeMove
        );
        break;
    }
  },

  /**
   * Attempts to expand provider until no events require expansion.
   *
   * @param {Date} expandDate expands up to this date.
   * @param {Calendar.Provider.Abstract} provider instance.
   * @param {Function} callback
   *  fired when maximumExpansions is hit or
   *  no more events require expansion.
   *
   */
  _expandProvider: function(expandDate, provider, callback) {
    debug('Will attempt to expand provider until:', expandDate);
    var tries = 0;
    var max = this.maximumExpansions;

    function attemptCompleteExpand() {
      debug('Will try to complete expansion (tries = ' + tries + ')');
      if (tries >= max) {
        return callback(new Error(
          'could not complete expansion after "' + tries + '"'
        ));
      }

      provider.ensureRecurrencesExpanded(expandDate, function(err, didExpand) {
        if (err) {
          return callback(err);
        }

        debug('Expansion attempt did expand:', didExpand);

        if (!didExpand) {
          // successfully expanded and no events need expansion
          // for this date anymore...
          callback();
        } else {
          tries++;
          // attempt another expand without stack.
          nextTick(attemptCompleteExpand);
        }
      });
    }

    attemptCompleteExpand();
  },

  /**
   * Queues an expansion. If the given date is before
   * any dates in the stack it will be discarded.
   */
  queueExpand: function(expandTo) {
    if (this.pending) {
      if (!this._next) {
        this._next = expandTo;
      } else if (expandTo > this._next) {
        this._next = expandTo;
      }
      // don't start the queue if pending...
      return;
    }

    // either way we need to process an event
    // so increment pending for running and non-running cases.
    this.pending = true;
    this.emit('expandStart');

    var self = this;
    function expandNext(date) {
      self.expand(date, function() {
        if (date === self._next) {
          self._next = null;
        }

        var next = self._next;

        // when the queue is empty emit expandComplete
        if (!next) {
          self.pending = false;
          self.emit('expandComplete');
          return;
        }

        expandNext(next);
      });
    }

    expandNext(expandTo);
  },

  /**
   * Ensures we have time converage until the given date.
   * Additional time will be added to the date see .paddingInDays.
   *
   * @param {Date} expandTo date to expand to.
   */
  expand: function(expandTo, callback) {
    debug('expand', expandTo);

    this.accounts.all((err, accounts) => {
      if (err) {
        return callback(err);
      }

      // add minimum padding...
      var expandDate = new Date(expandTo.valueOf());
      expandDate.setDate(expandDate.getDate() + this.paddingInDays);

      var providers = this._getExpandableProviders(accounts);
      var pending = providers.length;

      if (!pending) {
        return nextTick(callback);
      }

      providers.forEach(provider => {
        this._expandProvider(expandDate, provider, () => {
          if (--pending <= 0) {
            callback();
          }
        });
      });
    });
  },

  _getExpandableProviders: function(accounts) {
    var providers = [];
    Object.keys(accounts).forEach(key => {
      var account = accounts[key];
      var provider = providerFactory.get(account.providerType);
      if (provider &&
          provider.canExpandRecurringEvents &&
          providers.indexOf(provider) === -1) {
        providers.push(provider);
      }
    });

    return providers;
  }
};

});

define('worker/manager',['require','exports','module','responder','debug'],function(require, exports, module) {


var Responder = require('responder');
var debug = require('debug')('worker/manager');

const IDLE_CLEANUP_TIME = 5000;

/**
 * Worker manager. Each worker/thread
 * is assigned one or more roles.
 *
 * There may be one or more workers for
 * each role and there is a contract
 * that assumes all roles are stateless
 * requests/streams are routed to workers
 * and will be completed eventually
 * but without order guarantees.
 */
function Manager() {
  this._lastId = 0;

  Responder.call(this);

  this.roles = Object.create(null);
  this.workers = [];
}
module.exports = Manager;

Manager.prototype = {
  // So that we can mock out Worker API in unit tests...
  Worker: Worker,

  __proto__: Responder.prototype,

  _onLog: debug,

  _formatData: function(data) {
    if (data[1] && data[1].stack && data[1].constructorName) {
      var err = data[1];
      var builtErr;

      if (window[err.constructorName]) {
        builtErr = Object.create(window[err.constructorName].prototype);
      } else {
        builtErr = Object.create(Error.prototype);
      }

      var key;

      for (key in err) {
        if (err.hasOwnProperty(key)) {
          builtErr[key] = err[key];
        }
      }

      data[1] = builtErr;
    }

    return data;
  },

  _onWorkerError: function(worker, err) {
    if (/reference to undefined property/.test(err.message)) {
      // This is a warning spewed out by javascript.options.strict,
      // the worker actually didn't crash at all, so ignore it.
      return;
    }

    if (worker.instance) {
      worker.instance.terminate();
      worker.instance = null;
    }
    var pending = worker.pending;
    worker.pending = Object.create(null);
    for (var id in pending) {
      if (pending[id].stream) {
        pending[id].stream.emit('error', err);
      }
      pending[id].callback(err);
    }
  },

  _onWorkerMessage: function(worker, event) {
    var data = this._formatData(event.data);
    var type = data.shift();
    var match = type.match(/^(\d+) (end|stream)$/);

    if (type == 'log') {
      this._onLog.apply(this, data);

    } else if (match) {
      var pending = worker.pending[match[1]];
      if (pending) {
        this._dispatchMessage(worker, pending, match[2], data);
      } else {
        throw new Error('Message arrived for unknown consumer: ' +
                        type + ' ' + JSON.stringify(data));
      }
    } else {
      this.respond([type].concat(data));
    }
  },

  _dispatchMessage: function(worker, pending, type, data) {
    if (type == 'stream') {
      pending.stream.respond(data);
    } else { // 'end'
      pending.callback.apply(null, data);
      delete worker.pending[pending.id];
      // Bail out if there are other pending requests.
      if (Object.keys(worker.pending).length) {
        return;
      }
      // If none are left, schedule cleanup
      this._scheduleCleanup(worker);
    }
  },

  _addPending: function(worker, pending) {
    worker.pending[pending.id] = pending;
    clearTimeout(worker.cleanup);
  },

  _scheduleCleanup: function(worker) {
    clearTimeout(worker.cleanup);
    worker.cleanup = setTimeout(function() {
      // Ensure we don't have a race condition where someone just
      // added a request but the timeout fired anyway.
      if (Object.keys(worker.pending).length) {
        return;
      }
      if (!worker.instance) {
        return;
      }

      worker.instance.terminate();
      worker.instance = null;
    }, IDLE_CLEANUP_TIME);
  },

  /**
   * Adds a worker to the manager.
   * Worker is associated with one or
   * more roles. Workers are assumed
   * stateless.
   *
   *
   * @param {String|Array} role one or more roles.
   * @param {String} worker url.
   */
  add: function(role, workerURL) {
    debug('Will add', role, 'worker at', workerURL);
    var worker = {
      // Actual Worker instance, when active
      instance: null,
      // Handlers that are waiting for a response from this worker
      pending: Object.create(null),
      // Script URL
      url: workerURL,
      // Timeout set to disable the worker when it hasn't been used
      // for a given period of time
      cleanup: null
    };

    this.workers.push(worker);
    [].concat(role).forEach(function(role) {
      if (!(role in this.roles)) {
        this.roles[role] = [worker];
      } else {
        this.roles[role].push(worker);
      }
    }, this);
  },

  _ensureActiveWorker: function(role) {
    if (role in this.roles) {
      var workers = this.roles[role];
      var worker = workers[Math.floor(Math.random() * workers.length)];
      if (worker.instance) {
        return worker;
      } else {
        this._startWorker(worker);
        return worker;
      }
    } else {
      throw new Error('no worker with role "' + role + '" active');
    }
  },

  _startWorker: function(worker) {
    worker.instance = new this.Worker(
      // ?time= is for cache busting in development...
      // there have been cases where nightly would not
      // clear the cache of the worker.
      worker.url + '?time=' + Date.now()
    );

    worker.instance.onerror = this._onWorkerError.bind(this, worker);
    worker.instance.onmessage = this._onWorkerMessage.bind(this, worker);
    this._scheduleCleanup(worker);
  },

  request: function(role /*, args..., callback*/) {
    var args = Array.prototype.slice.call(arguments, 1);
    var callback = args.pop();
    var worker = null;

    try {
      worker = this._ensureActiveWorker(role);
    } catch (e) {
      callback(e);
      return;
    }

    var data = {
      id: this._lastId++,
      role: role,
      payload: args
    };

    this._addPending(worker, {
      id: data.id,
      callback: callback
    });

    worker.instance.postMessage(['_dispatch', data]);
  },

  stream: function(role /*, args...*/) {
    var args = Array.prototype.slice.call(arguments, 1);
    var stream = new Responder();
    var self = this;

    var data = {
      id: this._lastId++,
      role: role,
      payload: args,
      type: 'stream'
    };

    stream.request = function(callback) {
      var worker = null;

      stream.request = function() {
        throw new Error('stream request has been sent');
      };

      try {
        worker = self._ensureActiveWorker(role);
      } catch (e) {
        callback(e);
        return;
      }

      self._addPending(worker, {
        id: data.id,
        stream: stream,
        callback: callback
      });
      worker.instance.postMessage(['_dispatch', data]);
    };
    return stream;
  }
};

});

define('controllers/service',['require','exports','module','worker/manager','debug'],function(require, exports, module) {


var Manager = require('worker/manager');
var debug = require('debug')('controllers/service');

function Service() {
  Manager.call(this);
}
module.exports = Service;

Service.prototype = {
  __proto__: Manager.prototype,

  start: function() {
    debug('Will load and initialize worker...');
    this.add('caldav', '/js/caldav_worker.js');
  }
};

});

define('controllers/sync',['require','exports','module','responder'],function(require, exports, module) {


var Responder = require('responder');

/**
 * Handles all synchronization related
 * tasks. The intent is that this will
 * be the focal point for any view
 * to observe sync events and this
 * controller will decide when to actually
 * tell the stores when to sync.
 */
function Sync(app) {
  this.app = app;
  this.pending = 0;

  Responder.call(this);
}
module.exports = Sync;

Sync.prototype = {
  __proto__: Responder.prototype,

  startEvent: 'syncStart',
  completeEvent: 'syncComplete',

  _incrementPending: function() {
    if (!this.pending) {
      this.emit('syncStart');
    }

    this.pending++;
  },

  _resolvePending: function() {
    if (!(--this.pending)) {
      this.emit('syncComplete');
    }

    if (this.pending < 0) {
      dump('\n\n Error calendar sync .pending is < 0 \n\n');
    }
  },

  /**
   * Sync all accounts, calendars, events.
   * There is no callback for all intentionally.
   *
   * Use:
   *
   *    controller.once('syncComplete', cb);
   *
   */
  all: function(callback) {
    // this is for backwards compatibility... in reality we should remove
    // callbacks from .all.
    if (callback) {
      this.once('syncComplete', callback);
    }

    if (this.app.offline()) {
      this.emit('offline');
      return;
    }

    var account = this.app.store('Account');

    account.all(function(err, list) {

      for (var key in list) {
        this.account(list[key]);
      }

      // If we have nothing to sync
      if (!this.pending) {
        this.emit('syncComplete');
      }

    }.bind(this));
 },

  /**
   * Initiates a sync for a single calendar.
   *
   * @param {Object} account parent of calendar.
   * @param {Object} calendar specific calendar to sync.
   * @param {Function} [callback] optional callback.
   */
  calendar: function(account, calendar, callback) {
    var store = this.app.store('Calendar');
    var self = this;

    this._incrementPending();
    store.sync(account, calendar, err => {
      self._resolvePending();
      this.handleError(err, callback);
    });
  },

  /**
   * Initiates a sync of a single account and all
   * associated calendars (calendars that exist after
   * the full sync of the account itself).
   *
   * The contract is if an callback is given the callback MUST handle the
   * error given. The default behaviour is to bubble up the error up to the
   * error controller.
   *
   * @param {Object} account sync target.
   * @param {Function} [callback] optional callback.
  */
  account: function(account, callback) {
    var accountStore = this.app.store('Account');
    var calendarStore = this.app.store('Calendar');

    var self = this;

    this._incrementPending();
    accountStore.sync(account, err => {
      if (err) {
        self._resolvePending();
        return this.handleError(err, callback);
      }

      var pending = 0;
      function next() {
        if (!(--pending)) {
          self._resolvePending();

          if (callback) {
            callback();
          }
        }
      }

      function fetchCalendars(err, calendars) {
        if (err) {
          self._resolvePending();
          return self.handleError(err, callback);
        }

        for (var key in calendars) {
          pending++;
          self.calendar(account, calendars[key], next);
        }
      }

      // find all calendars
      calendarStore.remotesByAccount(
        account._id,
        fetchCalendars
      );
    });
  },

  /**
   * Private helper for choosing how to dispatch errors.
   * When given a callback the callback will be called otherwise the error
   * controller will be invoked.
   */
  handleError: function(err, callback) {
    if (callback) {
      return callback(err);
    }

    this.app.errorController.dispatch(err);
  }
};

});

define('controllers/time',['require','exports','module','calc','responder'],function(require, exports, module) {


// Controls the selected/displayed dates by all the calendar views.
// ---
// All views that needs to listen date selection/navigation changes, or set
// a new value, should do it through this module (eg. day/week/month views and
// DayObserver)

var isSameDate = require('calc').isSameDate;
var Responder = require('responder');

function Time(app) {
  this.app = app;
  Responder.call(this);

  this._timeCache = Object.create(null);
}
module.exports = Time;

Time.prototype = {
  __proto__: Responder.prototype,

  /**
   * Current position in time.
   * Includes year, month and day.
   *
   * @type {Date}
   */
  _position: null,

  /**
   * Current center point of cached
   * time spans. This is not the last
   * loaded timespan but the last
   * requested timespan.
   *
   * @type {Calendar.Timespan}
   */
  _currentTimespan: null,

  /**
   * Hash that contains
   * the pieces of the current _position.
   * (month, day, year)
   */
  _timeCache: null,

  /**
   * The time 'scale' of the current
   * state of the calendar.
   *
   * Usually one of: ['day', 'month', 'week']
   * @type {String}
   */
  _scale: null,

  /**
   * private state of mostRecentDayType
   */
  _mostRecentDayType: 'day',

  /**
   * When true will lock the cache so no records are
   * purged. This is critical during sync because some
   * records may not yet be in the database.
   */
  cacheLocked: false,

  /**
   * Returns the most recently changed
   * day type either 'day' or 'selectedDay'
   */
  get mostRecentDayType() {
    return this._mostRecentDayType;
  },

  get mostRecentDay() {
    if (this.mostRecentDayType === 'selectedDay') {
      return this.selectedDay;
    } else {
      return this.position;
    }
  },

  get timespan() {
    return this._currentTimespan;
  },

  get scale() {
    return this._scale;
  },

  set scale(value) {
    var oldValue = this._scale;
    if (value !== oldValue) {
      this._scale = value;
      this.emit('scaleChange', value, oldValue);
    }
  },

  get selectedDay() {
    return this._selectedDay;
  },

  set selectedDay(value) {
    var day = this._selectedDay;
    this._mostRecentDayType = 'selectedDay';
    if (!day || !isSameDate(day, value)) {
      this._selectedDay = value;
      this.emit('selectedDayChange', value, day);
    }
  },

  /**
   * Helper function to 'move' state of calendar
   * to the most recently modified day type.
   *
   * (in the case where selectedDay was changed after day)
   */
  moveToMostRecentDay: function() {
    if (this.mostRecentDayType === 'selectedDay') {
      this.move(this.selectedDay);
    }
  },

  _updateCache: function(type, value) {
    var old = this._timeCache[type];

    if (!old || !isSameDate(value, old)) {
      this._timeCache[type] = value;
      this.emit(type + 'Change', value, old);
    }
  },

  get month() {
    return this._timeCache.month;
  },

  get day() {
    return this._timeCache.day;
  },

  get year() {
    return this._timeCache.year;
  },

  get position() {
    return this._position;
  },

  /**
   * Sets position of controller
   * in time.
   *
   * @param {Date} date position to move to.
   */
  move: function(date) {
    var year = date.getFullYear();
    var month = date.getMonth();
    var yearDate = new Date(year, 0, 1);
    var monthDate = new Date(year, month, 1);

    this._position = date;
    this._mostRecentDayType = 'day';

    this._updateCache('year', yearDate);
    this._updateCache('month', monthDate);
    this._updateCache('day', date);
  }
};

});

/*!
 * EventEmitter2
 * https://github.com/hij1nx/EventEmitter2
 *
 * Copyright (c) 2013 hij1nx
 * Licensed under the MIT license.
 */
;!function(undefined) {

  var isArray = Array.isArray ? Array.isArray : function _isArray(obj) {
    return Object.prototype.toString.call(obj) === "[object Array]";
  };
  var defaultMaxListeners = 10;

  function init() {
    this._events = {};
    if (this._conf) {
      configure.call(this, this._conf);
    }
  }

  function configure(conf) {
    if (conf) {

      this._conf = conf;

      conf.delimiter && (this.delimiter = conf.delimiter);
      conf.maxListeners && (this._events.maxListeners = conf.maxListeners);
      conf.wildcard && (this.wildcard = conf.wildcard);
      conf.newListener && (this.newListener = conf.newListener);

      if (this.wildcard) {
        this.listenerTree = {};
      }
    }
  }

  function EventEmitter(conf) {
    this._events = {};
    this.newListener = false;
    configure.call(this, conf);
  }

  //
  // Attention, function return type now is array, always !
  // It has zero elements if no any matches found and one or more
  // elements (leafs) if there are matches
  //
  function searchListenerTree(handlers, type, tree, i) {
    if (!tree) {
      return [];
    }
    var listeners=[], leaf, len, branch, xTree, xxTree, isolatedBranch, endReached,
        typeLength = type.length, currentType = type[i], nextType = type[i+1];
    if (i === typeLength && tree._listeners) {
      //
      // If at the end of the event(s) list and the tree has listeners
      // invoke those listeners.
      //
      if (typeof tree._listeners === 'function') {
        handlers && handlers.push(tree._listeners);
        return [tree];
      } else {
        for (leaf = 0, len = tree._listeners.length; leaf < len; leaf++) {
          handlers && handlers.push(tree._listeners[leaf]);
        }
        return [tree];
      }
    }

    if ((currentType === '*' || currentType === '**') || tree[currentType]) {
      //
      // If the event emitted is '*' at this part
      // or there is a concrete match at this patch
      //
      if (currentType === '*') {
        for (branch in tree) {
          if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
            listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i+1));
          }
        }
        return listeners;
      } else if(currentType === '**') {
        endReached = (i+1 === typeLength || (i+2 === typeLength && nextType === '*'));
        if(endReached && tree._listeners) {
          // The next element has a _listeners, add it to the handlers.
          listeners = listeners.concat(searchListenerTree(handlers, type, tree, typeLength));
        }

        for (branch in tree) {
          if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
            if(branch === '*' || branch === '**') {
              if(tree[branch]._listeners && !endReached) {
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], typeLength));
              }
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
            } else if(branch === nextType) {
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i+2));
            } else {
              // No match on this one, shift into the tree but not in the type array.
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
            }
          }
        }
        return listeners;
      }

      listeners = listeners.concat(searchListenerTree(handlers, type, tree[currentType], i+1));
    }

    xTree = tree['*'];
    if (xTree) {
      //
      // If the listener tree will allow any match for this part,
      // then recursively explore all branches of the tree
      //
      searchListenerTree(handlers, type, xTree, i+1);
    }

    xxTree = tree['**'];
    if(xxTree) {
      if(i < typeLength) {
        if(xxTree._listeners) {
          // If we have a listener on a '**', it will catch all, so add its handler.
          searchListenerTree(handlers, type, xxTree, typeLength);
        }

        // Build arrays of matching next branches and others.
        for(branch in xxTree) {
          if(branch !== '_listeners' && xxTree.hasOwnProperty(branch)) {
            if(branch === nextType) {
              // We know the next element will match, so jump twice.
              searchListenerTree(handlers, type, xxTree[branch], i+2);
            } else if(branch === currentType) {
              // Current node matches, move into the tree.
              searchListenerTree(handlers, type, xxTree[branch], i+1);
            } else {
              isolatedBranch = {};
              isolatedBranch[branch] = xxTree[branch];
              searchListenerTree(handlers, type, { '**': isolatedBranch }, i+1);
            }
          }
        }
      } else if(xxTree._listeners) {
        // We have reached the end and still on a '**'
        searchListenerTree(handlers, type, xxTree, typeLength);
      } else if(xxTree['*'] && xxTree['*']._listeners) {
        searchListenerTree(handlers, type, xxTree['*'], typeLength);
      }
    }

    return listeners;
  }

  function growListenerTree(type, listener) {

    type = typeof type === 'string' ? type.split(this.delimiter) : type.slice();

    //
    // Looks for two consecutive '**', if so, don't add the event at all.
    //
    for(var i = 0, len = type.length; i+1 < len; i++) {
      if(type[i] === '**' && type[i+1] === '**') {
        return;
      }
    }

    var tree = this.listenerTree;
    var name = type.shift();

    while (name) {

      if (!tree[name]) {
        tree[name] = {};
      }

      tree = tree[name];

      if (type.length === 0) {

        if (!tree._listeners) {
          tree._listeners = listener;
        }
        else if(typeof tree._listeners === 'function') {
          tree._listeners = [tree._listeners, listener];
        }
        else if (isArray(tree._listeners)) {

          tree._listeners.push(listener);

          if (!tree._listeners.warned) {

            var m = defaultMaxListeners;

            if (typeof this._events.maxListeners !== 'undefined') {
              m = this._events.maxListeners;
            }

            if (m > 0 && tree._listeners.length > m) {

              tree._listeners.warned = true;
              console.error('(node) warning: possible EventEmitter memory ' +
                            'leak detected. %d listeners added. ' +
                            'Use emitter.setMaxListeners() to increase limit.',
                            tree._listeners.length);
              console.trace();
            }
          }
        }
        return true;
      }
      name = type.shift();
    }
    return true;
  }

  // By default EventEmitters will print a warning if more than
  // 10 listeners are added to it. This is a useful default which
  // helps finding memory leaks.
  //
  // Obviously not all Emitters should be limited to 10. This function allows
  // that to be increased. Set to zero for unlimited.

  EventEmitter.prototype.delimiter = '.';

  EventEmitter.prototype.setMaxListeners = function(n) {
    this._events || init.call(this);
    this._events.maxListeners = n;
    if (!this._conf) this._conf = {};
    this._conf.maxListeners = n;
  };

  EventEmitter.prototype.event = '';

  EventEmitter.prototype.once = function(event, fn) {
    this.many(event, 1, fn);
    return this;
  };

  EventEmitter.prototype.many = function(event, ttl, fn) {
    var self = this;

    if (typeof fn !== 'function') {
      throw new Error('many only accepts instances of Function');
    }

    function listener() {
      if (--ttl === 0) {
        self.off(event, listener);
      }
      fn.apply(this, arguments);
    }

    listener._origin = fn;

    this.on(event, listener);

    return self;
  };

  EventEmitter.prototype.emit = function() {

    this._events || init.call(this);

    var type = arguments[0];

    if (type === 'newListener' && !this.newListener) {
      if (!this._events.newListener) { return false; }
    }

    // Loop through the *_all* functions and invoke them.
    if (this._all) {
      var l = arguments.length;
      var args = new Array(l - 1);
      for (var i = 1; i < l; i++) args[i - 1] = arguments[i];
      for (i = 0, l = this._all.length; i < l; i++) {
        this.event = type;
        this._all[i].apply(this, args);
      }
    }

    // If there is no 'error' event listener then throw.
    if (type === 'error') {

      if (!this._all &&
        !this._events.error &&
        !(this.wildcard && this.listenerTree.error)) {

        if (arguments[1] instanceof Error) {
          throw arguments[1]; // Unhandled 'error' event
        } else {
          throw new Error("Uncaught, unspecified 'error' event.");
        }
        return false;
      }
    }

    var handler;

    if(this.wildcard) {
      handler = [];
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      searchListenerTree.call(this, handler, ns, this.listenerTree, 0);
    }
    else {
      handler = this._events[type];
    }

    if (typeof handler === 'function') {
      this.event = type;
      if (arguments.length === 1) {
        handler.call(this);
      }
      else if (arguments.length > 1)
        switch (arguments.length) {
          case 2:
            handler.call(this, arguments[1]);
            break;
          case 3:
            handler.call(this, arguments[1], arguments[2]);
            break;
          // slower
          default:
            var l = arguments.length;
            var args = new Array(l - 1);
            for (var i = 1; i < l; i++) args[i - 1] = arguments[i];
            handler.apply(this, args);
        }
      return true;
    }
    else if (handler) {
      var l = arguments.length;
      var args = new Array(l - 1);
      for (var i = 1; i < l; i++) args[i - 1] = arguments[i];

      var listeners = handler.slice();
      for (var i = 0, l = listeners.length; i < l; i++) {
        this.event = type;
        listeners[i].apply(this, args);
      }
      return (listeners.length > 0) || !!this._all;
    }
    else {
      return !!this._all;
    }

  };

  EventEmitter.prototype.on = function(type, listener) {

    if (typeof type === 'function') {
      this.onAny(type);
      return this;
    }

    if (typeof listener !== 'function') {
      throw new Error('on only accepts instances of Function');
    }
    this._events || init.call(this);

    // To avoid recursion in the case that type == "newListeners"! Before
    // adding it to the listeners, first emit "newListeners".
    this.emit('newListener', type, listener);

    if(this.wildcard) {
      growListenerTree.call(this, type, listener);
      return this;
    }

    if (!this._events[type]) {
      // Optimize the case of one listener. Don't need the extra array object.
      this._events[type] = listener;
    }
    else if(typeof this._events[type] === 'function') {
      // Adding the second element, need to change to array.
      this._events[type] = [this._events[type], listener];
    }
    else if (isArray(this._events[type])) {
      // If we've already got an array, just append.
      this._events[type].push(listener);

      // Check for listener leak
      if (!this._events[type].warned) {

        var m = defaultMaxListeners;

        if (typeof this._events.maxListeners !== 'undefined') {
          m = this._events.maxListeners;
        }

        if (m > 0 && this._events[type].length > m) {

          this._events[type].warned = true;
          console.error('(node) warning: possible EventEmitter memory ' +
                        'leak detected. %d listeners added. ' +
                        'Use emitter.setMaxListeners() to increase limit.',
                        this._events[type].length);
          console.trace();
        }
      }
    }
    return this;
  };

  EventEmitter.prototype.onAny = function(fn) {

    if (typeof fn !== 'function') {
      throw new Error('onAny only accepts instances of Function');
    }

    if(!this._all) {
      this._all = [];
    }

    // Add the function to the event listener collection.
    this._all.push(fn);
    return this;
  };

  EventEmitter.prototype.addListener = EventEmitter.prototype.on;

  EventEmitter.prototype.off = function(type, listener) {
    if (typeof listener !== 'function') {
      throw new Error('removeListener only takes instances of Function');
    }

    var handlers,leafs=[];

    if(this.wildcard) {
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);
    }
    else {
      // does not use listeners(), so no side effect of creating _events[type]
      if (!this._events[type]) return this;
      handlers = this._events[type];
      leafs.push({_listeners:handlers});
    }

    for (var iLeaf=0; iLeaf<leafs.length; iLeaf++) {
      var leaf = leafs[iLeaf];
      handlers = leaf._listeners;
      if (isArray(handlers)) {

        var position = -1;

        for (var i = 0, length = handlers.length; i < length; i++) {
          if (handlers[i] === listener ||
            (handlers[i].listener && handlers[i].listener === listener) ||
            (handlers[i]._origin && handlers[i]._origin === listener)) {
            position = i;
            break;
          }
        }

        if (position < 0) {
          continue;
        }

        if(this.wildcard) {
          leaf._listeners.splice(position, 1);
        }
        else {
          this._events[type].splice(position, 1);
        }

        if (handlers.length === 0) {
          if(this.wildcard) {
            delete leaf._listeners;
          }
          else {
            delete this._events[type];
          }
        }
        return this;
      }
      else if (handlers === listener ||
        (handlers.listener && handlers.listener === listener) ||
        (handlers._origin && handlers._origin === listener)) {
        if(this.wildcard) {
          delete leaf._listeners;
        }
        else {
          delete this._events[type];
        }
      }
    }

    return this;
  };

  EventEmitter.prototype.offAny = function(fn) {
    var i = 0, l = 0, fns;
    if (fn && this._all && this._all.length > 0) {
      fns = this._all;
      for(i = 0, l = fns.length; i < l; i++) {
        if(fn === fns[i]) {
          fns.splice(i, 1);
          return this;
        }
      }
    } else {
      this._all = [];
    }
    return this;
  };

  EventEmitter.prototype.removeListener = EventEmitter.prototype.off;

  EventEmitter.prototype.removeAllListeners = function(type) {
    if (arguments.length === 0) {
      !this._events || init.call(this);
      return this;
    }

    if(this.wildcard) {
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      var leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);

      for (var iLeaf=0; iLeaf<leafs.length; iLeaf++) {
        var leaf = leafs[iLeaf];
        leaf._listeners = null;
      }
    }
    else {
      if (!this._events[type]) return this;
      this._events[type] = null;
    }
    return this;
  };

  EventEmitter.prototype.listeners = function(type) {
    if(this.wildcard) {
      var handlers = [];
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      searchListenerTree.call(this, handlers, ns, this.listenerTree, 0);
      return handlers;
    }

    this._events || init.call(this);

    if (!this._events[type]) this._events[type] = [];
    if (!isArray(this._events[type])) {
      this._events[type] = [this._events[type]];
    }
    return this._events[type];
  };

  EventEmitter.prototype.listenersAny = function() {

    if(this._all) {
      return this._all;
    }
    else {
      return [];
    }

  };

  if (typeof define === 'function' && define.amd) {
     // AMD. Register as an anonymous module.
    define('ext/eventemitter2',[],function() {
      return EventEmitter;
    });
  } else if (typeof exports === 'object') {
    // CommonJS
    exports.EventEmitter2 = EventEmitter;
  }
  else {
    // Browser global.
    window.EventEmitter2 = EventEmitter;
  }
}();

// these methods are all borrowed from MOUT.js (released under MIT license)
define('utils/mout',['require','exports','module'],function(require, exports) {


// TODO: add MOUT.js as a dependency!!!! this is just a temporary solution

exports.norm = function norm(val, min, max){
  // 0 / 0 === NaN
  if (val === min && min === max) {
    return 1;
  }
  return (val - min) / (max - min);
};

exports.clamp = function clamp(val, min, max) {
  return val < min ? min : (val > max ? max : val);
};

exports.round = function round(value, radix){
  radix = radix || 1; // default round 1
  return Math.round(value / radix) * radix;
};

exports.floor = function floor(val, step) {
  step = Math.abs(step || 1);
  return Math.floor(val / step) * step;
};

exports.ceil = function ceil(val, step) {
  step = Math.abs(step || 1);
  return Math.ceil(val / step) * step;
};

exports.lerp = function lerp(ratio, start, end){
  return start + (end - start) * ratio;
};

exports.debounce = function debounce(fn, threshold, isAsap){
  var timeout, result;
  function debounced(){
    //jshint -W040
    var args = arguments, context = this;
    //jshint +W040
    function delayed(){
      if (! isAsap) {
        result = fn.apply(context, args);
      }
      timeout = null;
    }
    if (timeout) {
      clearTimeout(timeout);
    } else if (isAsap) {
      result = fn.apply(context, args);
    }
    timeout = setTimeout(delayed, threshold);
    return result;
  }
  debounced.cancel = function(){
    clearTimeout(timeout);
  };
  return debounced;
};

exports.throttle = function throttle(fn, delay) {
  var context, timeout, result, args,
    diff,
    prevCall = 0;
  function delayed() {
    prevCall = Date.now();
    timeout = null;
    result = fn.apply(context, args);
  }
  function throttled() {
    //jshint -W040
    context = this;
    //jshint +W040
    args = arguments;
    diff = delay - (Date.now() - prevCall);
    if (diff <= 0) {
      clearTimeout(timeout);
      delayed();
    } else if (!timeout) {
      timeout = setTimeout(delayed, diff);
    }
    return result;
  }
  throttled.cancel = function() {
    clearTimeout(timeout);
  };
  return throttled;
};

});

define('day_observer',['require','exports','module','calc','ext/eventemitter2','binsearch','compare','utils/mout'],function(require, exports) {


// Listen for changes on all busytimes inside a given day
// ---
// This module will listen for changes on any busytime inside a day and
// group/batch multiple add/remove events into a single emit call; making it
// easier to rerender all the busytimes at once and allowing us to use the
// same method to add/remove busytimes from the DOM, drastically simplifying
// the logic required.
// ---
// On the most common scenario there should not be that many busytimes inside
// the same day, and every time we add a new busytime to the DOM we need to
// check for "overlaps", so doing the full rerender will make a lot of sense
// and should not affect performance that much.
// ---
// It's way simpler to use a day-based logic for the whole calendar front-end,
// instead of knowing how to handle months/weeks.

var Calc = require('calc');
var EventEmitter2 = require('ext/eventemitter2');
var binsearch = require('binsearch');
var compare = require('compare');
var daysBetween = Calc.daysBetween;
var debounce = require('utils/mout').debounce;
var getDayId = Calc.getDayId;
var isAllDay = Calc.isAllDay;
var spanOfMonth = Calc.spanOfMonth;

// make sure we only trigger a single emit for multiple consecutive changes
var DISPATCH_DELAY = 25;

// maximum amount of months to keep in the cache
var MAX_CACHED_MONTHS = 5;

// stores busytimes by Id
// {key:id, value:Busytime}
var busytimes = new Map();

// stores events by Id
// {key:id, value:Event}
var events = new Map();

// relationships between event Ids and busytimes Ids
// {key:eventId, value:Array<busytimeId>}
var eventsToBusytimes = new Map();

// cache a reference to all busytime+event data for the days based on the dayId
// {key:dayId, value:DayRecords}
var cache = new Map();

// stores the days IDs that changed since last dispatch
var dayQueue = new Set();

// stores the timespans for all the cached months
var cachedSpans = [];

// we lock the cache pruning during sync
var cacheLocked = false;

// emitter exposed because of unit tests
var emitter = exports.emitter = new EventEmitter2();

// TODO: convert these dependencies into static modules to avoid ugly injection
// injected later to avoid circular dependencies
exports.busytimeStore = null;
exports.calendarStore = null;
exports.eventStore = null;
exports.syncController = null;
exports.timeController = null;

exports.init = function() {
  // both "change" and "add" operations triggers a "persist" event
  this.eventStore.on('persist', (id, event) => cacheEvent(event));
  this.eventStore.on('remove', removeEventById);

  this.busytimeStore.on('persist', (id, busy) => cacheBusytime(busy));
  this.busytimeStore.on('remove', removeBusytimeById);

  this.syncController.on('syncStart', () => {
    cacheLocked = true;
    Toaster.showToast({
      messageL10nId: 'tcl-sync-progress-syncing',
      latency: 2000
    });
  });
  this.syncController.on('syncComplete', () => {
    cacheLocked = false;
    pruneCache();
    dispatch();
    Toaster.showToast({
      messageL10nId: 'tcl-sync-progress-complete',
      latency: 2000
    });
  });

  this.calendarStore.on('calendarVisibilityChange', (id, calendar) => {
    var type = calendar.localDisplayed ? 'add' : 'remove';
    busytimes.forEach((busy, busyId) => {
      if (busy.calendarId === id) {
        registerBusytimeChange(busyId, type);
      }
    });
  });

  this.timeController.on('monthChange', loadMonth);

  // make sure loadMonth is called during setup if 'monthChange' was dispatched
  // before we added the listener
  var month = this.timeController.month;
  if (month) {
    loadMonth(month);
  }
};

exports.on = function(date, callback) {
  var dayId = getDayId(date);
  // important to trigger the callback to avoid getting into weird states when
  // busytimes are loaded while views are not listening for changes
  callback(getDay(dayId, date));
  emitter.on(dayId, callback);
};

function getDay(dayId, date) {
  if (cache.has(dayId)) {
    return cache.get(dayId);
  }

  var day = {
    dayId: dayId,
    date: date,
    amount: 0,
    basic: [],
    allday: []
  };
  cache.set(dayId, day);

  return day;
}

exports.off = function(date, callback) {
  emitter.off(getDayId(date), callback);
};

exports.removeAllListeners = function() {
  emitter.removeAllListeners();
};

exports.getEventsToBusytimes = function(){
  return eventsToBusytimes;
};

// findAssociated is not on the Event/Busytime store so that it can get the
// cache data and show the event details faster on the most common cases
exports.findAssociated = function(busytimeId) {
  return queryBusytime(busytimeId).then(busytime => {
    return queryEvent(busytime.eventId).then(event => {
      return {
        busytime: busytime,
        event: event
      };
    });
  });
};

function queryBusytime(busytimeId) {
  if (busytimes.has(busytimeId)) {
    return Promise.resolve(busytimes.get(busytimeId));
  }
  return exports.busytimeStore.get(busytimeId);
}

function queryEvent(eventId) {
  if (events.has(eventId)) {
    return Promise.resolve(events.get(eventId));
  }
  return exports.eventStore.get(eventId);
}

function cacheBusytime(busy) {
  var {_id, startDate, endDate, eventId} = busy;

  if (outsideSpans(startDate) && outsideSpans(endDate)) {
    // ignore busytimes outside the cached spans
    return;
  }

  busytimes.set(_id, busy);
  eventsToBusytimes.get(eventId).push(_id);
  registerBusytimeChange(_id);
}

function removeBusytimeById(id) {
  var busy = busytimes.get(id);
  if (!busy) {
    // when removing all the data from the calendar the busytime might not be
    // on the cache but still emit a "remove" event
    return;
  }

  var eventId = busy.eventId;
  var ids = eventsToBusytimes.get(eventId).filter(i => i !== id);
  eventsToBusytimes.set(eventId, ids);
  removeEventIfNoBusytimes(ids, eventId);

  registerBusytimeChange(id, 'remove');
  busytimes.delete(id);
}

function cacheEvent(event) {
  var id = event._id;
  events.set(id, event);
  if (!eventsToBusytimes.has(id)) {
    eventsToBusytimes.set(id, []);
  }
}

function removeEventById(id) {
  events.delete(id);
  // busytimeStore will emit a 'remove' event for each busytime, so no need to
  // handle it here; we simply remove the "join table"
  eventsToBusytimes.delete(id);
}

// every time an event/calendar/busytime changes we need to trigger a change
// event and notify all the views that are listening to that specific date
function registerBusytimeChange(id, type) {
  var busy = busytimes.get(id);
  var {startDate, endDate} = busy;

  // subtract 1 millisecond because allday events ends at 00:00:00 of next day,
  // which would include one day more than expected for daysBetween
  var end = new Date(endDate.getTime() - 1);

  // events from hidden calendars should not be displayed
  var isRemove = type === 'remove' ||
    !exports.calendarStore.shouldDisplayCalendar(busy.calendarId);

  daysBetween(startDate, end).forEach(date => {
    if (outsideSpans(date)) {
      // ignore dates outside the cached spans
      return;
    }

    var dayId = getDayId(date);

    if (isRemove && !cache.has(dayId)) {
      return;
    }

    var day = getDay(dayId, date);

    // it should always override the old data
    day.basic = day.basic.filter(r => r.busytime._id !== id);
    day.allday = day.allday.filter(r => r.busytime._id !== id);

    if (!isRemove) {
      var group = isAllDay(date, startDate, endDate) ?
        day.allday :
        day.basic;
      sortedInsert(group, busy);
    }

    day.amount = day.basic.length + day.allday.length;

    dayQueue.add(dayId);
  });

  dispatch();
}

function sortedInsert(group, busy) {
  var index = binsearch.insert(group, busy.startDate, (date, record) => {
    return compare(date, record.busytime.startDate);
  });
  var event = events.get(busy.eventId);
  group.splice(index, 0, {
    event: event,
    busytime: busy,
    color: exports.calendarStore.getColorByCalendarId(event.calendarId)
  });
}

// debounce will make sure that we only trigger one "change" event per day even
// if we have multiple changes happening in a row
var dispatch = debounce(function() {
  dayQueue.forEach(id => {
    // need to remove ID from queue before dispatching event to avoid race
    // conditions (eg. handler triggering a new queue+dispatch)
    dayQueue.delete(id);
    // dispatch is async so there is a small chance data isn't cached anymore
    if (cache.has(id)) {
      emitter.emit(id, cache.get(id));
    }
  });
}, DISPATCH_DELAY);

function loadMonth(newMonth) {
  var span = spanOfMonth(newMonth);

  // ensure we load the minimum amount of busytimes possible
  var toLoad = span;
  cachedSpans.every(cached => toLoad = cached.trimOverlap(toLoad));

  if (!toLoad) {
    // already loaded all the busytimes
    return;
  }

  // cache the whole month instead of `toLoad` because we purge whole months
  cachedSpans.push(span);

  exports.busytimeStore.loadSpan(toLoad, onBusytimeSpanLoad);
}

function onBusytimeSpanLoad(err, busytimes) {
  if (err) {
    console.error('Error loading Busytimes from TimeSpan:', err.toString());
    return;
  }

  // remove duplicates and avoid loading events that are already cached
  var eventIds = Array.from(new Set(
    busytimes.map(b => b.eventId).filter(id => !events.has(id))
  ));

  exports.eventStore.findByIds(eventIds).then(events => {
    // it's very important to cache the events before the busytimes otherwise
    // the records won't contain the event data
    Object.keys(events).forEach(key => cacheEvent(events[key]));
    busytimes.forEach(cacheBusytime);

    pruneCache();
  });
}

function pruneCache() {
  if (cacheLocked) {
    return;
  }

  trimCachedSpans();
  cache.forEach(removeDayFromCacheIfOutsideSpans);
  eventsToBusytimes.forEach(removeEventIfNoBusytimes);
}

function trimCachedSpans() {
  while (cachedSpans.length > MAX_CACHED_MONTHS) {
    // since most changes are sequential, remove the timespans that are further
    // away from the current month
    var baseDate = exports.timeController.month;
    var maxDiff = 0;
    var maxDiffIndex = 0;
    cachedSpans.forEach((span, i) => {
      var diff = Math.abs(span.start - baseDate);
      if (diff > maxDiff) {
        maxDiff = diff;
        maxDiffIndex = i;
      }
    });
    cachedSpans.splice(maxDiffIndex, 1);
  }
}

function removeDayFromCacheIfOutsideSpans(day, id) {
  if (outsideSpans(day.date)) {
    day.basic.forEach(removeRecordIfOutsideSpans);
    day.allday.forEach(removeRecordIfOutsideSpans);
    cache.delete(id);
  }
}

function outsideSpans(date) {
  return !cachedSpans.some(timespan => timespan.contains(date));
}

function removeRecordIfOutsideSpans(record) {
  removeBusytimeIfOutsideSpans(record.busytime);
}

function removeBusytimeIfOutsideSpans(busytime) {
  var {_id, startDate, endDate} = busytime;
  if (outsideSpans(startDate) && outsideSpans(endDate)) {
    removeBusytimeById(_id);
  }
}

function removeEventIfNoBusytimes(ids, eventId) {
  if (!ids.length) {
    removeEventById(eventId);
  }
}

});

/**
 * @fileoverview Period sync controller manages
 *
 *     1. Seeding first sync alarm when app starts.
 *     2. Syncing when a sync alarm fires and issuing a new sync alarm.
 *     3. Invalidating any existing sync alarms and issuing a new one
 *        when the sync interval changes.
 */
define('controllers/periodic_sync',['require','exports','module','responder','debug','message_handler'],function(require, exports) {


var Responder = require('responder');
var debug = require('debug')('controllers/periodic_sync');
var messageHandler = require('message_handler');

/**
 * Cached alarm previously sent to alarms db.
 * @type {Object}
 */
var syncAlarm;

/**
 * Cached sync value (every x minutes) from scheduling.
 * @type {Number}
 */
var prevSyncFrequency;

/**
 * Most recently set sync interval (every x minutes).
 * @type {Number}
 */
var syncFrequency;

/**
 * Cached promise representing pending sync operation.
 * @type {Promise}
 */
var syncing;

/**
 * Cached promise representing pending schedule operation.
 * @type {Promise}
 */
var scheduling;

var events = new Responder();
exports.events = events;

var accounts;
var settings;

// Will be injected...
exports.app = null;

exports.observe = function() {
  debug('Will start periodic sync controller...');
  var app = exports.app;
  accounts = app.store('Account');
  settings = app.store('Setting');
  return Promise.all([
    settings.getValue('syncAlarm'),
    settings.getValue('syncFrequency')
  ])
  .then(values => {
    [syncAlarm, syncFrequency] = values;
    // Trigger whenever there is a change to the accounts collection
    // since we need to re-evaluate whether periodic sync is still necessary.
    accounts.on('persist', onAccountsChange);
    accounts.on('remove', onAccountsChange);
    // Listen to the settings collection for a change to sync frequency so that
    // we can update any alarms we've sent to the alarms api accordingly.
    debug('Will listen for syncFrequencyChange...');
    settings.on('syncFrequencyChange', exports);
    // Listen for sync event from alarms api.
    messageHandler.responder.on('sync', exports);
    return scheduleSync();
  });
};

exports.unobserve = function() {
  syncAlarm = null;
  prevSyncFrequency = null;
  syncFrequency = null;
  syncing = null;
  scheduling = null;
  accounts.off('persist', onAccountsChange);
  accounts.off('remove', onAccountsChange);
  settings.off('syncFrequencyChange', exports);
  messageHandler.responder.off('sync', exports);
};

exports.handleEvent = function(event) {
  switch (event.type) {
    case 'sync':
      // Gets triggered by mozSetMessageHandler alarm event.
      return onSync();
    case 'syncFrequencyChange':
      // Gets triggered by settings db change to sync frequency.
      debug('Received syncFrequencyChange!');
      return onSyncFrequencyChange(event.data[0]);
  }
};

/**
 * 1. Wait until we're done with any previous work.
 * 2. Sync.
 * 3. Schedule the next periodic sync.
 */
function onSync() {
  return sync().then(scheduleSync);
}

/**
 * 1. Wait until we're done with any previous work.
 * 2. Schedule a periodic sync at the new interval.
 */
function onSyncFrequencyChange(value) {
  debug('Sync frequency changed to', value);
  syncFrequency = value;
  return maybeScheduleSync();
}

/**
 * No syncable accounts => revoke any previously scheduled sync alarms.
 * Syncable accounts and no previously scheduled sync => schedule new sync.
 */
function onAccountsChange() {
  debug('Looking up syncable accounts...');
  return accounts.syncableAccounts().then(syncable => {
    if (!syncable || !syncable.length) {
      debug('There are no syncable accounts!');
      revokePreviousAlarm();
      events.emit('pause');
      return;
    }

    debug('There are', syncable.length, 'syncable accounts');

    if (!syncAlarmIssued()) {
      debug('The first syncable account was just added.');
      return scheduleSync();
    }
  });
}

function sync() {
  if (!syncing) {
    syncing = new Promise((resolve, reject) => {
      debug('Will request cpu and wifi wake locks...');
      var cpuLock = navigator.requestWakeLock('cpu');
      var wifiLock = navigator.requestWakeLock('wifi');
      debug('Will start periodic sync...');
      var app = exports.app;
      app.syncController.all(() => {
        debug('Sync complete! Will release cpu and wifi wake locks...');
        cpuLock.unlock();
        wifiLock.unlock();
        events.emit('sync');
        syncing = null;
        resolve();
      });
    });
  }

  return syncing;
}

function scheduleSync() {
  if (scheduling) {
    return scheduling;
  }

  scheduling = accounts.syncableAccounts()
  .then(syncable => {
    if (!syncable || !syncable.length) {
      debug('There seem to be no syncable accounts, will defer scheduling...');
      return Promise.resolve();
    }

    debug('Will schedule periodic sync in:', syncFrequency);
    // Cache the sync interval which we're sending to the alarms api.
    prevSyncFrequency = syncFrequency;
    revokePreviousAlarm();

    return issueSyncAlarm().then(cacheSyncAlarm).then(maybeScheduleSync);
  })
  .then(() => {
    events.emit('schedule');
    scheduling = null;
  })
  .catch(error => {
    debug('Error scheduling sync:', error);
    console.error(error.toString());
    scheduling = null;
  });

  return scheduling;
}

// TODO: When navigator.mozAlarms.remove (one day) returns a DOMRequest,
//     we should make this async...
function revokePreviousAlarm() {
  if (!syncAlarmIssued()) {
    debug('No sync alarms issued, nothing to revoke...');
    return;
  }

  debug('Will revoke alarm', syncAlarm.alarmId);
  var alarms = navigator.mozAlarms;
  alarms.remove(syncAlarm.alarmId);
}

function issueSyncAlarm() {
  if (!prevSyncFrequency) {
    debug('Periodic sync disabled!');
    return Promise.resolve({ alarmId: null, start: null, end: null });
  }

  var start = new Date();
  var end = new Date(
    start.getTime() +
    prevSyncFrequency * 60 * 1000 // minutes to ms
  );

  var alarms = navigator.mozAlarms;
  var request = alarms.add(end, 'ignoreTimezone', { type: 'sync' });

  return new Promise((resolve, reject) => {
    request.onsuccess = function() {
      resolve({ alarmId: this.result, start: start, end: end });
    };

    request.onerror = function() {
      reject(this.error);
    };
  });
}

function cacheSyncAlarm(alarm) {
  debug('Will save alarm:', alarm);
  syncAlarm = alarm;
  return settings.set('syncAlarm', syncAlarm);
}

function maybeScheduleSync() {
  if (syncFrequency === prevSyncFrequency) {
    // Nothing to do!
    return Promise.resolve();
  }

  if (scheduling) {
    return scheduling.then(scheduleSync);
  }

  return scheduleSync();
}

function syncAlarmIssued() {
  return syncAlarm && !!syncAlarm.alarmId;
}

});

define('snake_case',['require','exports','module'],function(require, exports, module) {


module.exports = function(name) {
  return name
    .replace(/^./, chr => chr.toLowerCase())
    .replace(/[A-Z]/g, chr => '_' + chr.toLowerCase());
};

});

define('app',['require','exports','module','calc','date_l10n','db','controllers/error','pending_manager','controllers/recurring_events','router','controllers/service','controllers/sync','controllers/time','day_observer','debug','message_handler','next_tick','controllers/notifications','controllers/periodic_sync','performance','provider/provider_factory','snake_case'],function(require, exports, module) {


var Calc = require('calc');
var DateL10n = require('date_l10n');
var Db = require('db');
var ErrorController = require('controllers/error');
var PendingManager = require('pending_manager');
var RecurringEventsController = require('controllers/recurring_events');
var router = require('router');
var ServiceController = require('controllers/service');
var SyncController = require('controllers/sync');
var TimeController = require('controllers/time');
var Views = {};
var dayObserver = require('day_observer');
var debug = require('debug')('app');
var messageHandler = require('message_handler');
var nextTick = require('next_tick');
var notificationsController = require('controllers/notifications');
var periodicSyncController = require('controllers/periodic_sync');
var performance = require('performance');
var providerFactory = require('provider/provider_factory');
var snakeCase = require('snake_case');

var pendingClass = 'pending-operation';

/**
 * Focal point for state management
 * within calendar application.
 *
 * Contains tools for routing and central
 * location to reference database.
 */
module.exports = {
  startingURL: window.location.href,
  skPanel: null,
  deleteDialogShown: false,
  isShowLunday: false, // Defect216-ting-wang@t2mobile.com-Chinese Calendar requirement just use in China

  /**
   * Entry point for application
   * must be called at least once before
   * using other methods.
   */
  configure: function(db) {
    debug('Configure calendar with db.');
    this.db = db;
    router.app = this;

    providerFactory.app = this;

    this._views = Object.create(null);
    this._routeViewFn = Object.create(null);
    this._pendingManager = new PendingManager();

    var loadedLazyStyles = false;

    this._pendingManager.oncomplete = function onpending() {
      document.body.classList.remove(pendingClass);
      performance.pendingReady();
      // start loading sub-views as soon as possible
      if (!loadedLazyStyles) {
        loadedLazyStyles = true;

        // XXX: not loading the 'lazy_loaded.js' here anymore because for some
        // weird reason curl.js was returning an object instead of
        // a constructor when loading the "views/view_event" when starting the
        // app from a notification; might be related to the fact we bundled
        // multiple modules into the same file, are using the "paths" config to
        // set the location and also using the async require in 2 places and
        // using different module ids for each call.. the important thing is
        // that this should still give a good performance result and works as
        // expected.

        // we need to grab the global `require` because the async require is
        // not part of the AMD spec and is not implemented by all loaders
        window.require(['css!lazy_loaded']);
      }
    };

    this._pendingManager.onpending = function oncomplete() {
      document.body.classList.add(pendingClass);
    };

    messageHandler.app = this;
    messageHandler.start();
    this.timeController = new TimeController(this);
    this.syncController = new SyncController(this);
    this.serviceController = new ServiceController(this);
    this.errorController = new ErrorController(this);
    notificationsController.app = this;
    periodicSyncController.app = this;

    dayObserver.busytimeStore = this.store('Busytime');
    dayObserver.calendarStore = this.store('Calendar');
    dayObserver.eventStore = this.store('Event');
    dayObserver.syncController = this.syncController;
    dayObserver.timeController = this.timeController;

    // observe sync events
    this.observePendingObject(this.syncController);

    // Tell audio channel manager that we want to adjust the notification
    // channel if the user press the volumeup/volumedown buttons in Calendar.
    if (navigator.mozAudioChannelManager) {
      navigator.mozAudioChannelManager.volumeControlChannel = 'notification';
    }
    //Defect216-ting-wang@t2mobile.com-Chinese Calendar requirement just use in China-begin
    navigator.customization.getValue("lunar.day.enable").then((result) => {
      dump("The lunar day state is" + result);
      this.isShowLunday = (result == undefined) ? false : result;

    });
    // Defect216-ting-wang@t2mobile.com-Chinese Calendar requirement just use in China-end
  },

  /**
   * Observes localized events and localizes elements
   * with data-l10n-date-format should be registered
   * after the first localized event.
   *
   *
   * Example:
   *
   *
   *    <span
   *      data-date="Wed Jan 09 2013 19:25:38 GMT+0100 (CET)"
   *      data-l10n-date-format="%x">
   *
   *      2013/9/19
   *
   *    </span>
   *
   */
  observeDateLocalization: function() {
    window.addEventListener('localized', DateL10n.localizeElements);
    window.addEventListener('timeformatchange', () => {
      this.setCurrentTimeFormat();
      DateL10n.changeElementsHourFormat();
    });
  },

  setCurrentTimeFormat: function() {
    document.body.dataset.timeFormat = navigator.mozHour12 ? '12' : '24';
  },

  /**
   * Adds observers to objects capable of being pending.
   *
   * Object must emit some kind of start/complete events
   * and have the following properties:
   *
   *  - startEvent (used to register an observer)
   *  - endEvent ( ditto )
   *  - pending
   *
   * @param {Object} object to observe.
   */
  observePendingObject: function(object) {
    this._pendingManager.register(object);
  },

  isPending: function() {
    return this._pendingManager.isPending();
  },

  _routes: function() {

    /* routes */
    router.state('/week/', 'Week');
    router.state('/day/', 'Day');
    router.state('/month/', ['Month', 'MonthDayAgenda']);
    router.state('/settings/', 'Settings');
    router.state('/advanced-settings/', 'AdvancedSettings');
    router.state('/switchto-date/', 'SwitchtoDate');
    router.state('/search/', 'EventSearch');
    router.state('/alarm-display/:id', 'ViewEvent', { path: false });
    router.state('/show-multi-events/', 'MultiEvents');

    router.state('/event/add/', 'ModifyEvent');
    router.state('/event/edit/:id', 'ModifyEvent');
    router.state('/event/show/:id', 'ViewEvent');

    router.state('/select-preset/', 'CreateAccount');
    router.state('/create-account/:preset', 'ModifyAccount');
    router.state('/update-account/:id', 'ModifyAccount');
    //Task 5317587 Add by ting-wang@t2mobile.com 20170911
    router.state('/lunar-day/', 'LunarDay');

    router.start();

    // at this point the tabs should be interactive and the router ready to
    // handle the path changes (meaning the user can start interacting with
    // the app)
    performance.chromeInteractive();

  },

  _initControllers: function() {
    // controllers can only be initialized after db.load

    // start the workers
    this.serviceController.start(false);

    notificationsController.observe();
    periodicSyncController.observe();

    var recurringEventsController = new RecurringEventsController(this);
    this.observePendingObject(recurringEventsController);
    recurringEventsController.observe();
    this.recurringEventsController = recurringEventsController;

    // turn on the auto queue this means that when
    // alarms are added to the database we manage them
    // transparently. Defaults to off for tests.
    this.store('Alarm').autoQueue = true;
  },

  _initUI: function() {
    this._syncTodayDate();

    this.setCurrentTimeFormat();
    // re-localize dates on screen
    this.observeDateLocalization();

    this.timeController.move(new Date());

    // at this point we remove the .loading class and user will see the main
    // app frame
    performance.domLoaded();

    this._routes();

    nextTick(() => this.view('Errors'));

    window.addEventListener('largetextenabledchanged', (event) => {
      document.body.classList.toggle('large-text', navigator.largeTextEnabled);
    });
    document.body.classList.toggle('large-text', navigator.largeTextEnabled);
  },

  _setPresentDate: function() {
    var id = Calc.getDayId(new Date());
    var presentDate = document.querySelector(
      '#month-view [data-date="' + id + '"]'
    );
    var previousDate = document.querySelector('#month-view .present');

    previousDate.classList.remove('present');
    previousDate.classList.add('past');
    presentDate.classList.add('present');
  },

  _syncTodayDate: function() {
    var now = new Date();
    var midnight = new Date(
      now.getFullYear(), now.getMonth(), now.getDate() + 1,
      0, 0, 0
    );

    var timeout = midnight.getTime() - now.getTime();
    setTimeout(() => {
      this._setPresentDate();
      this._syncTodayDate();
    }, timeout);
  },

  /**
   * Primary code for app can go here.
   */
  init: function() {
    debug('Will initialize calendar app...');

    if (!this.db) {
      this.configure(new Db('b2g-calendar', this));
    }

    this.db.load(() => {
      this._initControllers();
      // it should only start listening for month change after we have the
      // calendars data, otherwise we might display events from calendars that
      // are not visible. this also makes sure we load the calendars as soon as
      // possible
      this.store('Calendar').all(() => dayObserver.init());

      // we init the UI after the db.load to increase perceived performance
      // (will feel like busytimes are displayed faster)
      this._initUI();
      window.addEventListener('routerGoMonth', () => {
        router.go('/month/');
      });
    });
  },

  _initView: function(name) {
    var view = new Views[name]({ app: this });
    this._views[name] = view;
  },

  /**
   * Initializes a view and stores
   * a internal reference so when
   * view is called a second
   * time the same view is used.
   *
   * Makes an asynchronous call to
   * load the script if we do not
   * have the view cached.
   *
   *    // for example if you have
   *    // a calendar view Foo
   *
   *    Calendar.Views.Foo = Klass;
   *
   *    app.view('Foo', function(view) {
   *      (view instanceof Calendar.Views.Foo) === true
   *    });
   *
   * @param {String} name view name.
   * @param {Function} view loaded callback.
   */
  view: function(name, cb) {
    if (name in this._views) {
      debug('Found view named ', name);
      var view = this._views[name];
      return cb && nextTick(() => cb.call(this, view));
    }

    if (name in Views) {
      debug('Must initialize view', name);
      this._initView(name);
      return this.view(name, cb);
    }

    var snake = snakeCase(name);
    debug('Will try to load view', name);
    // we need to grab the global `require` because the async require is not
    // part of the AMD spec and is not implemented by all loaders
    window.require([ 'views/' + snake ], (aView) => {
      debug('Loaded view', name);
      Views[name] = aView;
      return this.view(name, cb);
    });
  },

  /**
   * Pure convenience function for
   * referencing a object store.
   *
   * @param {String} name store name. (e.g events).
   * @return {Calendar.Store.Abstact} store.
   */
  store: function(name) {
    return this.db.getStore(name);
  },

  /**
   * Returns the offline status.
   */
  offline: function() {
    return (navigator && 'onLine' in navigator) ? !navigator.onLine : true;
  },

  createSks: function(actions) {
    var params = {
      header: {
        l10nId: 'options',
        name: 'Options'
      },
      items: actions
    };
    if (this.skPanel) {
      this.skPanel.initSoftKeyPanel(params);
      this.skPanel.show();
    } else {
      this.skPanel = new SoftkeyPanel(params);
      this.skPanel.show();
    }
    if(actions.length == 0) {
      this.hideSkPanel();
    }
  },

  getSkPanel: function() {
    return this.skPanel;
  },

  hideSkPanel: function() {
    if(this.skPanel) {
      this.skPanel.hide();
    }
  },

  showSkPanel: function() {
    if(this.skPanel) {
      this.skPanel.show();
    }
  },

  closeApp: function() {
    window.close();
  }
};

});

(function() {


window.require = window.require || window.curl;

require.config({
  baseUrl: '/js',
  waitSeconds: 60,
  paths: {
    shared: '/shared/js',
    dom: 'curl/plugin/dom',
    css: 'curl/plugin/css'
  },
  shim: {
    'ext/caldav': { exports: 'Caldav' },
    'ext/ical': { exports: 'ICAL' },
    'ext/page': { exports: 'page' },
    'shared/input_parser': { exports: 'InputParser' },
    'shared/notification_helper': { exports: 'NotificationHelper' },
    'shared/performance_testing_helper': { exports: 'PerformanceTestingHelper' }
  }
});

// first require.config call is used by r.js optimizer, so we do this second
// call to list modules that are bundled to avoid duplicate defines
require.config({
  paths: {
    'views/week': 'lazy_loaded',
    'views/advanced_settings': 'lazy_loaded',
    'views/create_account': 'lazy_loaded',
    'views/day': 'lazy_loaded',
    'views/modify_account': 'lazy_loaded',
    'views/modify_event': 'lazy_loaded',
    'views/settings': 'lazy_loaded',
    'views/view_event': 'lazy_loaded',
    'views/event_search': 'lazy_loaded',
    'views/multi_events': 'lazy_loaded',
    'views/switchto_date': 'lazy_loaded',
    'views/lunar_day':'lazy_loaded'
  }
});

require(['app'], app => app.init());

}());

define("main", function(){});

define('view',['require','exports','module'],function(require, exports, module) {


var DEFAULT_ERROR_ID = 'error-default';
const INVALID_CSS = /([^a-zA-Z\-\_0-9])/g;

/**
 * Very simple base class for views.
 * Provides functionality for active/inactive.
 *
 * The first time the view is activated
 * the onactive function/event will fire.
 *
 * The .seen property is added to each object
 * with view in its prototype. .seen can be used
 * to detect if the view has ever been activated.
 *
 * @param {String|Object} options options or a selector for element.
 */
function View(options) {
  if (typeof(options) === 'undefined') {
    options = {};
  }

  if (typeof(options) === 'string') {
    this.selectors = { element: options };
  } else {
    var key;

    if (typeof(options) === 'undefined') {
      options = {};
    }

    for (key in options) {
      if (options.hasOwnProperty(key)) {
        this[key] = options[key];
      }
    }
  }

  this.hideErrors = this.hideErrors.bind(this);
}
module.exports = View;

View.ACTIVE = 'active';

View.prototype = {
  seen: false,
  activeClass: View.ACTIVE,
  errorVisible: false,

  get element() {
    return this._findElement('element');
  },

  get status() {
    return this._findElement('status');
  },

  get errors() {
    return this._findElement('errors');
  },

  /**
   * Creates a string id for a given model.
   *
   *    view.idForModel('foo-', { _id: 1 }); // => foo-1
   *    view.idForModel('foo-', '2'); // => foo-2
   *
   * @param {String} prefix of string.
   * @param {Object|String|Numeric} objectOrString representation of model.
   */
  idForModel: function(prefix, objectOrString) {
    prefix += (typeof(objectOrString) === 'object') ?
      objectOrString._id :
      objectOrString;

    return prefix;
  },

  calendarId: function(input) {
    if (typeof(input) !== 'string') {
      input = input.calendarId;
    }

    input = this.cssClean(input);
    return 'calendar-id-' + input;
  },

  /**
   * Delegate pattern event listener.
   *
   * @param {HTMLElement} element parent element.
   * @param {String} type type of dom event.
   * @param {String} selector css selector element should match
   *                          _note_ there is no magic here this
   *                          is determined from the root of the document.
   * @param {Function|Object} handler event handler.
   *                                  first argument is the raw
   *                                  event second is the element
   *                                  matching the pattern.
   */
  delegate: function(element, type, selector, handler) {
    if (typeof(handler) === 'object') {
      var context = handler;
      handler = function() {
        context.handleEvent.apply(context, arguments);
      };
    }

    element.addEventListener(type, function(e) {
      var target = e.target;
      while (target !== element) {
        if ('mozMatchesSelector' in target &&
            target.mozMatchesSelector(selector)) {
          return handler(e, target);
        }
        target = target.parentNode;
      }
    });
  },

  /**
   * Clean a string for use with css.
   * Converts illegal chars to legal ones.
   */
  cssClean: function(string) {
    if (typeof(string) !== 'string') {
      return string;
    }

    //TODO: I am worried about the performance
    //of using this all over the place =/
    //consider sanitizing all keys to ensure
    //they don't blow up when used as a selector?
    return string.replace(INVALID_CSS, '-');
  },

  /**
   * Finds a caches a element defined
   * by selectors
   *
   * @param {String} selector name as defined in selectors.
   * @param {Boolean} all true when to find all elements. (default false).
   */
  _findElement: function(name, all, element) {
    if (typeof(all) === 'object') {
      element = all;
      all = false;
    }

    element = element || document;

    var cacheName;
    var selector;

    if (typeof(all) === 'undefined') {
      all = false;
    }

    if (name in this.selectors) {
      cacheName = '_' + name + 'Element';
      selector = this.selectors[name];

      if (!this[cacheName]) {
        if (all) {
          this[cacheName] = element.querySelectorAll(selector);
        } else {
          this[cacheName] = element.querySelector(selector);
        }
      }

      return this[cacheName];
    }

    return null;
  },

 /**
   * Displays a list of errors
   *
   * @param {Array} list error list
   *  (see Event.validaitonErrors) or Error object.
   */
  showErrors: function(list) {
    var _ = navigator.mozL10n.get;
    var errors = '';

    // We can pass Error objects or
    // Array of {name: foo} objects
    if (!Array.isArray(list)) {
        list = [list];
    }

    var i = 0;
    var len = list.length;

    for (; i < len; i++) {
      var name = list[i].l10nID || list[i].name;
      errors += _('error-' + name) || _(DEFAULT_ERROR_ID);
    }

    Toaster.showToast({
      message: errors
    });
  },

  hideErrors: function() {
    this.status.classList.remove(this.activeClass);
    this.status.removeEventListener('animationend', this.hideErrors);
    this.errorVisible = false;
  },

  stripItem: function(params,tarName) {
    for(var i in params) {
      if(params[i].name === tarName) {
        params.splice(i,1);
      }
    }
    return params;
  },

  onactive: function() {
    if (this.errorVisible) {
      this.hideErrors();
    }

    // seen can be set to anything other than false to override this behaviour
    if (this.seen === false) {
      this.onfirstseen();
    }

    // intentionally using 'in'
    if ('dispatch' in this) {
      this.dispatch.apply(this, arguments);
    }

    this.seen = true;
    if (this.element) {
      this.element.classList.add(this.activeClass);
    }
  },

  oninactive: function() {
    if (this.element) {
      this.element.classList.remove(this.activeClass);
    }
  },

  onfirstseen: function() {},

  showWarningDialog: function(isLocalCalendar, type) {
    var offline =  (navigator && 'onLine' in navigator) ? !navigator.onLine : true;
    var shouldShowWarningDialog = offline && !isLocalCalendar;
    if (shouldShowWarningDialog) {
      var textId = type === 'delete' ?
          'error-delete-online-event' : 'error-edit-online-event';
      var dialogConfig = {
        title: {id: 'tcl-confirmation-title', args: {}},
        body: {id: textId, args: {}},
        desc: {id: '', args: {}},
        accept: {
          name: 'Ok',
          l10nId: 'ok',
          priority: 2,
          callback: function() {}
        }
      }
      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('confirm-dialog-container'));
    }
    return shouldShowWarningDialog;
  }
};

});

define('dom',['require','exports','module'],function(require, exports) {


exports.load = function(id, require, onLoad, config) {
  if (config.isBuild) {
    return onLoad();
  }

  var node = document.getElementById(id);
  if (!node) {
    onLoad.error('can\'t find element with id #' + id);
    return;
  }

  LazyLoader.load(node, function() {
    onLoad(node);
  });
};

});

define('views/errors',['require','exports','module','view','dom!errors'],function(require, exports, module) {


var View = require('view');

require('dom!errors');

function Errors() {
  View.apply(this, arguments);
  this.app.syncController.on('offline', this);
}
module.exports = Errors;

Errors.prototype = {
  __proto__: View.prototype,

  selectors: {
    status: '*[role="application"] > section[role="status"]',
    errors: '*[role="application"] > section > .errors'
  },

  handleEvent: function(event) {
    switch (event.type) {
      case 'offline':
        this.showErrors([{name: 'offline'}]);
        break;
    }
  }
};

});

define('views/month_day',['require','exports','module','calc','day_observer'],function(require, exports, module) {


var Calc = require('calc');
var dayObserver = require('day_observer');
var l10n = navigator.mozL10n;

// MonthDay represents a single day inside the Month view grid.
function MonthDay(options) {
  this.container = options.container;
  this.date = options.date;
  this.month = options.month;
  this._updateBusyCount = this._updateBusyCount.bind(this);
}
module.exports = MonthDay;

MonthDay.prototype = {

  container: null,
  date: null,
  element: null,
  month: null,

  create: function() {
    var dayId = Calc.getDayId(this.date);
    var id = 'month-view-day-' + dayId;
    var state = Calc.relativeState(this.date, this.month);
    var l10nStateId = state.replace(/\s/g, '-');
    var date = this.date.getDate();
    var week = this.date.getDay();

    if (l10nStateId === 'present') {
      l10nStateId = 'present-description';
    } else {
      l10nStateId = 'day-description';
    }

    var el = document.createElement('li');
    el.setAttribute('role', 'button');
    el.id = id;
    el.tabindex = 0;
    var describeInfo = navigator.mozL10n.get(l10nStateId, {
      weekday:navigator.mozL10n.get('weekday-' + week),
      date: date
    });
    el.dataset.date = dayId;
    el.className = state;
    el.classList.add('month-day');
    el.classList.add('focusable');
    el.innerHTML = `<span class="day" role="button">${date}</span>
      <div id="${id}-busy-indicator" aria-hidden="true" class="busy-indicator"></div>
      <span id="${id}-description" aria-hidden="true" class="day-describe"
        aria-label="${describeInfo}">
      </span>`;

    this.element = el;
    this._updateAriaLabel();
    this.container.appendChild(el);
  },

  _updateAriaLabel: function() {
    var day = this.element.querySelector('.day');
    var busyIndicator = this.element.querySelector('.busy-indicator');
    var dayDescribe = this.element.querySelector('.day-describe');
    var busyLabel = busyIndicator.getAttribute('aria-label');
    var dayDescribeLabel = dayDescribe.getAttribute('aria-label');
    var dayLabel = '';
    if (dayDescribeLabel) {
      dayLabel = dayDescribeLabel;
    }
    if (busyLabel) {
      dayLabel = dayLabel + ' ' + busyLabel;
    }
    day.setAttribute('aria-label', dayLabel);
  },

  activate: function() {
    dayObserver.on(this.date, this._updateBusyCount);
  },

  deactivate: function() {
    dayObserver.off(this.date, this._updateBusyCount);
  },

  destroy: function() {
    this.deactivate();
    this.container = this.element = null;
  },

  _updateBusyCount: function(data) {
    var count = Math.min(3, data.amount);
    var holder = this.element.querySelector('.busy-indicator');

    if (count >= 0) {
      holder.setAttribute('aria-label', navigator.mozL10n.get('busy', {
        n: count
      }));
      this._updateAriaLabel();
    } else {
      holder.removeAttribute('aria-label');
    }

    var diff = count - holder.childNodes.length;
    if (diff === 0) {
      return;
    }

    if (diff > 0) {
      var dot;
      var arr = data.allday.concat(data.basic);
      while (diff--) {
        dot = document.createElement('div');
        dot.className = 'gaia-icon icon-calendar-dot';
        dot.style['background-color'] = arr[diff].color;
        holder.appendChild(dot);
      }
      return;
    }

    // difference < 0
    while (diff++) {
      holder.removeChild(holder.firstChild);
    }
  }
};

});

define('views/single_month',['require','exports','module','calc','./month_day','view'],function(require, exports, module) {


var Calc = require('calc');
var MonthDay = require('./month_day');
var View = require('view');
var daysBetween = Calc.daysBetween;
var daysInWeek = Calc.daysInWeek;
var getDayId = Calc.getDayId;
var spanOfMonth = Calc.spanOfMonth;

var SELECTED = 'selected';

// SingleMonth contains all the logic required to build the Month view grid and
// groups all the MonthDay instances for that given month.
function SingleMonth() {
  View.apply(this, arguments);
  this.days = [];
  this.timespan = spanOfMonth(this.date);
  this.timeController = this.app.timeController;
  this.weeklys = null;
  this.limit_row = 2;
}

module.exports = SingleMonth;

SingleMonth.prototype = {
  __proto__: View.prototype,

  active: false,
  container: null,
  date: null,
  days: null,
  element: null,

  create: function() {
    var element = document.createElement('section');
    element.className = 'month';
    element.setAttribute('role', 'grid');

    element.setAttribute('aria-readonly', true);
    element.innerHTML = this._renderDayHeaders();
    element.dataset.date = getDayId(this.date);
    this.element = element;
    this._buildWeeks();
  },

  _renderDayHeaders: function() {
    // startDay might change during the 'localized' event
    var startDay = Calc.startDay;
    var days = [];
    var i;
    for (i = startDay; i < 7; i++) {
      days.push(this._dayHeader(i));
    }

    if (startDay > 0) {
      for (i = 0; i < startDay; i++) {
        days.push(this._dayHeader(i));
      }
    }

    var html = `<header id="month-days" role="presentation">
      <ol role="row">${days.join('')}</ol>
    </header>`;
    return html;
  },

  _dayHeader: function(dayIndex) {
    return `<li data-l10n-id="weekday-${dayIndex}-single-char"
      role="columnheader" class="h4"></li>`;
  },

  _buildWeeks: function() {
    var weekLength = daysInWeek();
    var week;
    daysBetween(this.timespan).forEach((date, i) => {
      if (i % weekLength === 0) {
        week = document.createElement('ol');
        week.setAttribute('role', 'row');
        this.element.appendChild(week);
      }
      var day = new MonthDay({
        date: date,
        month: this.date,
        container: week
      });
      day.create();
      this.days.push(day);
    });
  },

  activate: function() {
    if (this.active) {
      return;
    }
    this.active = true;

    this.onactive();
    this.days.forEach(day => day.activate());
    this._onSelectedDayChange(this.timeController.selectedDay);
    this.timeController.on('selectedDayChange', this);
  },

  deactivate: function() {
    if (!this.active) {
      return;
    }
    this.active = false;

    this.oninactive();
    this.days.forEach(day => day.deactivate());
    this.timeController.off('selectedDayChange', this);
  },

  destroy: function() {
    this.deactivate();
    this._detach();
    this.days.forEach(day => day.destroy());
    this.days = [];
  },

  append: function() {
    this.container.appendChild(this.element);
  },

  _detach: function() {
    if (this.element && this.element.parentNode) {
      this.element.parentNode.removeChild(this.element);
    }
  },

  handleEvent: function(e) {
    if (e.type === 'selectedDayChange') {
      this._onSelectedDayChange(e.data[0]);
    }
  },

  _clearSelectedDay: function() {
    var day = this.element.querySelector(`li.${SELECTED}`);
    if (day) {
      day.classList.remove(SELECTED);
    }
  },
  _isPortraitMode: function(){
    return screen.orientation.type.startsWith('portrait');
  },

  _onSelectedDayChange: function(date) {
    this._clearSelectedDay();

    if (!date || !this.timespan.contains(date)) {
      return;
    }

    var el = this.element.querySelector(`li[data-date="${getDayId(date)}"]`);
    el.classList.add(SELECTED);

    if (!this._isPortraitMode() && window.location.pathname === '/switchto-date/' && !el.classList.contains('present')) {
      this._handlerExhibition(el);
    }
  },

    /** In mode landscape is visible only 3 weekly row's,
     * this method is responsible for handler exhibition of rows. */
    _handlerExhibition: function(dayActive) {
      if (!this.weeklys) {
        var nodes = document.querySelectorAll('#view-switchto-month .month.active ol');
        this.weeklys = Array.prototype.slice.call(nodes);
        this.weeklys.shift();
      }

      var currentRowIndex = this.weeklys.indexOf(dayActive.parentNode);
      if (currentRowIndex > this.limit_row) {
        this._toggleExhibitionRow(this.weeklys[currentRowIndex], true);

        for (let i = 0; i <= (currentRowIndex - 3); i++) {
          this._toggleExhibitionRow(this.weeklys[i], false);
        }
      } else {
        this._toggleExhibitionRow(this.weeklys[currentRowIndex], true);
        this._toggleExhibitionRow(this.weeklys[currentRowIndex + 3], false);
      }
    },
    _toggleExhibitionRow: function(element, active) {
      if (active) {
        element.classList.add('row-visible');
        element.classList.remove('row-hidden');
      } else {
        element.classList.add('row-hidden');
        element.classList.remove('row-visible');
      }
    }
};

});

define('views/month',['require','exports','module','calc','./single_month','view','router','day_observer'],function(require, exports, module) {


var Calc = require('calc');
var SingleMonth = require('./single_month');
var View = require('view');
var dateFromId = Calc.dateFromId;
var monthStart = Calc.monthStart;
var router = require('router');
var dayObserver = require('day_observer');

// minimum difference between X and Y axis to be considered an horizontal swipe
var XSWIPE_OFFSET = window.innerWidth / 10;

var skAddEvent = {
  name: 'Add Event',
  l10nId: 'add-event',
  priority: 1,
  method: function() {
    router.go('/event/add/');
  }
};

var skWeeklyView = {
  name: 'Week View',
  l10nId: 'week-view',
  priority: 5,
  method: function () {
    console.log('Weekly View');
    window.history.backfrom = 'month-view';
    router.go('/week/');
  }
};

var skDayView = {
  name: 'Day View',
  l10nId: 'day-view',
  priority: 5,
  method: function () {
    console.log('Day View');
    router.go('/day/');
  }
};

var skCurrentDate = {
  name: 'Today',
  l10nId: 'today',
  priority: 5,
  method: function () {
    console.log('Current Date');
    var date = new Date();
    var controller = self.app.timeController;
    self.controller.move(date);
    self.controller.selectedDay = date;
    var evt = new CustomEvent('tcl-date-changed', {
      detail: {
        toDate: date
      },
      bubbles: true,
      cancelable: false
    });

    document.dispatchEvent(evt);
  }
};

var skGoToDate = {
  name: 'Go to Date',
  l10nId: 'go',
  priority: 5,
  method: function () {
    console.log('Go To Date');
    setTimeout(function() {
      router.go('/switchto-date/');
    }, 300);
  }
};

var skSearch = {
  name: 'Search',
  l10nId: 'search',
  priority: 5,
  method: function () {
    console.log('Search');
    router.go('/search/');
  }
};

var skCalendarsToDisplay = {
  name: 'Calendars to Display',
  l10nId: 'calendar-to-display',
  priority: 5,
  method: function () {
    console.log('Calendars to Display');
    router.go('/settings/');
  }
};

var skSync = {
  name: 'Sync calendar',
  l10nId: 'sync-calendar',
  priority: 5,
  method: function () {
    self.app.syncController.all();
  }
};

var skSettings = {
  name: 'Settings',
  l10nId: 'settings',
  priority: 5,
  method: function () {
    console.log('Settings');
    router.go('/advanced-settings/');
  }
};

var skDefaultCSK = {
  name: 'select',
  l10nId: 'select',
  priority: 2,
  method: function() {}
};
//Task 5317587 Add by ting-wang@t2mobile.com 20170911 begin
var skLunar = {
  name: 'Lunar Calendar',
  l10nId: 'lunar-calendar',
  priority: 5,
  method: function () {
    router.go('/lunar-day/');
  }
};

// Defect216-ting-wang@t2mobile.com-Chinese Calendar requirement just use in China-begin
var monthUiAddEventWithOptionActions = [
    skAddEvent, skDefaultCSK, skWeeklyView, skDayView,
    skCurrentDate, skGoToDate, skSearch,
    skCalendarsToDisplay, skSync, skSettings];

function Month() {
  View.apply(this, arguments);
  this.frames = new Map();
  window.addEventListener('localized', this);
}
module.exports = Month;

Month.prototype = {
  __proto__: View.prototype,

  SCALE: 'month',

  // boolean to control the inversed keydown
  isRtl: false,

  selectors: {
    element: '#month-view',
    selectedDay:'.month.active ol li.focus',
    currentTime: '#time-views #launch-view'
  },

  date: null,

  /** @type {SingleMonth} */
  currentFrame: null,

  /** @type {DOMElement} used to detect if dbltap happened on same date */
  _lastTarget: null,

  self:null,

  /**
   * store current, previous and next months
   * we load them beforehand and keep on the cache to speed up swipes
   * @type {Array<SingleMonth>}
   */
  frames: null,
  records: null,

  actionIfy:"",

  onactive: function() {
    View.prototype.onactive.apply(this, arguments);
    self = this;
    this._findElement('currentTime').classList.remove('active');
    if (router.last && /^\/(week|day|switchto-date)/.test(router.last.path)) {
      var controller = self.app.timeController;
      window.history.state.monthselectedDay = controller.selectedDay;
      controller.moveToMostRecentDay();
    } else {
      delete window.history.state.monthselectedDay;
    }
    this.app.timeController.scale = this.SCALE;
    if (this.currentFrame) {
      this.currentFrame.activate();
    }
    self.actionIfy="";
    // Defect216-ting-wang@t2mobile.com-Chinese Calendar requirement just use in China-begin
    if (this.app.isShowLunday && !monthUiAddEventWithOptionActions.includes(skLunar)){
        monthUiAddEventWithOptionActions.push(skLunar);
    }
    this.updateSKs(monthUiAddEventWithOptionActions);
    // Defect216-ting-wang@t2mobile.com-Chinese Calendar requirement just use in China-end
    window.addEventListener('keydown', this._keyDownEvent, false);
    window.addEventListener('index-changed', this);
  },

  _initEvents: function() {
    this.controller = this.app.timeController;
    this.controller.on('monthChange', this);
  },

  _postMonthChanged: function(dalta) {
    var item = NavigationMap.getCurItem();
    var current_date = dateFromId(item.dataset.date);
    var next_date  = self._get_day(current_date, dalta);

    self.controller.move(next_date);
    var evt = new CustomEvent('tcl-date-changed', {
      detail: {
        toDate: next_date
      },
      bubbles: true,
      cancelable: false
    });

    document.dispatchEvent(evt);
  },

  _get_day:function(date , count){
      var yesterday_all_milliseconds = date.getTime() - count * 1000 * 60 * 60 * 24;
      var today=new Date();
      today.setTime(yesterday_all_milliseconds);
      var temp_hour = today.getHours();
      var temp_date = today.getDate();
      if(temp_hour >= 12)
      {
        today.setDate(temp_date+1);
        today.setHours(0);
      }
      return today;
  },

  _keyDownEvent: function(e) {
    this.isRtl = document.documentElement.dir === 'rtl';
    var leftRightKeys = this.isRtl ? ['ArrowRight', 'ArrowLeft'] : ['ArrowLeft', 'ArrowRight'];
    if(!self.app.skPanel.menuVisible) {
      switch(e.key) {
        case 'Enter':
        case 'Accept':
          if (self.records.amount === 0) {
            //the CSK needn't to show and nothing to do
          } else if (self.records.amount === 1) {
            if (self.records.allday.length === 1) {
              router.show('/event/show/' + self.records.allday[0].busytime._id);
            } else {
              router.show('/event/show/' + self.records.basic[0].busytime._id);
            }
          } else {
            var param = {date: self.controller.selectedDay};
            router.go('/show-multi-events/', param);
          }
          break;
        case 'Backspace':
        case 'BrowserBack':
          // Do nothing;
          break;
        case leftRightKeys[0]:
          if (e.target.style.getPropertyValue('--pre-month-1')) {
            self._postMonthChanged(1);
          }
          break;
        case leftRightKeys[1]:
          if (e.target.style.getPropertyValue('--next-month-1')) {
            self._postMonthChanged(-1);
          }
          break;
        case 'ArrowDown':
          if (e.target.style.getPropertyValue('--next-month-7')) {
            self._postMonthChanged(-7);
          }
          break;
        case 'ArrowUp':
          if (e.target.style.getPropertyValue('--pre-month-7')) {
            self._postMonthChanged(7);
          }
          break;
      }
    }
  },
  updateSKs:function(actions){
    self.app.createSks(actions);
  },

  handleEvent: function(e, target) {
    switch (e.type) {
      case 'monthChange':
        this.changeDate(e.data[0]);
        break;
      case 'localized':
        this.reconstruct();
        break;
      case 'index-changed':
        var item = e.detail.focusedItem;
        var currentDate = dateFromId(item.dataset.date);
        self.controller.selectedDay = currentDate;
        if(item.classList.contains('other-month')) {
          self.controller.move(currentDate);
          var evt = new CustomEvent('tcl-date-changed', {
            detail: {
              toDate: currentDate
            },
            bubbles: true,
            cancelable: false
          });

          document.dispatchEvent(evt);
        }
        dayObserver.on(self.controller.selectedDay, this._saveRecordsData.bind(this));
        this._createSks();
        break;

    }
    this._lastTarget = target;
  },

  _goToAddEvent: function(date) {
    // slight delay to avoid tapping the elements inside the add event screen
    setTimeout(() => {
      // don't need to set the date since the first tap triggers a click that
      // sets the  timeController.selectedDay
      router.go('/event/add/');
    }, 50);
  },

  changeDate: function(time) {
    this.date = monthStart(time);

    if (this.currentFrame) {
      this.currentFrame.deactivate();
    }

    this.currentFrame = this._getFrame(this.date);

    this._trimFrames();
    this._appendFrames();

    this.currentFrame.activate();
  },

  _getFrame: function(date) {
    var id = date.getTime();
    var frame = this.frames.get(id);
    if (!frame) {
      frame = new SingleMonth({
        app: this.app,
        date: date,
        container: this.element
      });
      frame.create();
      this.frames.set(id, frame);
    }
    return frame;
  },

  _trimFrames: function() {
    if (this.frames.size <= 3) {
      return;
    }

    // full month (we always keep previous/next months)
    var delta = 31 * 24 * 60 * 60 * 1000;

    this.frames.forEach((frame, ts) => {
      var base = Number(this.date);
      if (Math.abs(base - ts) > delta) {
        frame.destroy();
        this.frames.delete(ts);
      }
    });
  },

  _appendFrames: function() {
    // sort elements by timestamp (key = timestamp) so DOM makes more sense
    Array.from(this.frames.keys())
    .sort((a, b) => a - b)
    .forEach(key => this.frames.get(key).append());
  },

  oninactive: function() {
    View.prototype.oninactive.call(this);
    if (this.currentFrame) {
      this.currentFrame.deactivate();
    }
    window.removeEventListener('keydown', this._keyDownEvent, false);
    window.removeEventListener('index-changed', this);
    self.actionIfy="";
  },

   _createSks: function() {
    var actions = [].concat(monthUiAddEventWithOptionActions);
    var selectedDay = document.querySelector(self.selectors.selectedDay);
    var isToday = false;
    if (!selectedDay && typeof(selectedDay) !== 'undefined') {
      isToday = true;
    } else {
      isToday = Calc.isToday(dateFromId(selectedDay.dataset.date));
    }

    if (isToday) {
      actions = self.stripItem(actions, 'Today');
    }
    if (self.records.amount === 0) {
      actions = self.stripItem(actions, 'select');
    }

    if (JSON.stringify(actions) !== self.actionIfy) {
      this.updateSKs(actions);
      self.actionIfy = JSON.stringify(actions);
    }
  },

  onfirstseen: function() {
    this.app.view('TimeHeader', (header) => header.render());
    this._initEvents();
    this.changeDate(this.controller.month);
  },

  destroy: function() {
    this.frames.forEach((frame, key) => {
      this.frames.delete(key);
      frame.destroy();
    });
  },

  reconstruct: function() {
    // Watch for changed value from transition of a locale to another
    this.destroy();
    this.changeDate(this.controller.month);
  },

  _saveRecordsData: function(records) {
    self.records = records;
  }

};

});

define('template',['require','exports','module'],function(require, exports, module) {


var POSSIBLE_HTML = /[&<>"'`]/;

var span = document.createElement('span');

function create(templates) {
  var key, result = {};

  for (key in templates) {
    if (templates.hasOwnProperty(key)) {
      result[key] = new Template(templates[key]);
    }
  }

  return result;
}

function Template(fn) {
  this.template = fn;
}
module.exports = Template;

Template.prototype = {
  arg: function(key) {
    if (typeof(this.data) === 'undefined') {
      return '';
    } else if (typeof(this.data) !== 'object') {
      return this.data;
    }

    return this.data[key];
  },

  h: function(a) {
    var arg = this.arg(a);
    // accept anything that can be converted into a string and we make sure
    // the only falsy values that are converted into empty strings are
    // null/undefined to avoid mistakes
    arg = arg == null ? '' : String(arg);

    //only escape bad looking stuff saves
    //a ton of time
    if (POSSIBLE_HTML.test(arg)) {
      span.textContent = arg;
      return span.innerHTML.replace(/"/g, '&quot;').replace(/'/g, '&#x27;');
    } else {
      return arg;
    }
  },

  s: function(a) {
    var arg = this.arg(a);
    return String((arg || ''));
  },

  bool: function(key, onTrue) {
    if (this.data[key]) {
      return onTrue;
    } else {
      return '';
    }
  },

  l10n: function(key, prefix) {
    var value = this.arg(key);

    if (prefix) {
      value = prefix + value;
    }
    return navigator.mozL10n.get(value);
  },

  l10nId: function(a) {
    return this.s(a).replace(/\s/g, '-');
  },

  /**
   * Renders template with given slots.
   *
   * @param {Object} object key, value pairs for template.
   */
  render: function(data) {
    this.data = data;
    return this.template();
  },

  /**
   * Renders template multiple times
   *
   * @param {Array} objects object details to render.
   * @param {String} [join] optional join argument will join the array.
   * @return {String|Array} String if join argument is given array otherwise.
   */
  renderEach: function(objects, join) {
    var i = 0, len = objects.length,
        result = [];

    for (; i < len; i++) {
      result.push(this.render(objects[i]));
    }

    if (typeof(join) !== 'undefined') {
      return result.join(join);
    }

    return result;
  }
};

Template.create = create;

});

define('templates/date_span',['require','exports','module','calc','template','date_format'],function(require, exports, module) {


var Calc = require('calc');
var create = require('template').create;
var dateFormat = require('date_format');

var l10n = navigator.mozL10n;

module.exports = create({
  time: function() {
    var time = this.arg('time');
    var format = Calc.getTimeL10nLabel(this.h('format'));
    var displayTime = dateFormat.localeFormat(time, l10n.get(format));

    return `<span class="p-pri" data-l10n-date-format="${format}"
                  data-date="${time}">${displayTime}</span>`;
  },

  hour: function() {
    var hour = this.h('hour');
    var format = Calc.getTimeL10nLabel(this.h('format'));
    var className = this.h('className');
    // 0ms since epoch as base date to avoid issues with daylight saving time
    var date = new Date(0);
    date.setHours(hour, 0, 0, 0);

    var displayDate = new Date(0);
    var h = hour === '23' ? hour :
        hour === '11' && navigator.mozHour12 ? '0' : parseInt(hour)  + 1;
    displayDate.setHours(h, 0, 0, 0);

    var l10nLabel = l10n.get(format);
    if (this.arg('addAmPmClass')) {
      l10nLabel = l10nLabel.replace(
        /\s*%p\s*/,
        '<span class="ampm" aria-hidden="true">%p</span>'
      );
    }

    var displayHour = dateFormat.localeFormat(displayDate, l10nLabel);
    // remove leading zero
    displayHour = displayHour.replace(/^0/, '');
    if (hour === '23') {
      var index = displayHour.indexOf('<');
      displayHour = index > 0 ? displayHour.substring(index) : '';
    }
    var l10nAttr = (hour === Calc.ALLDAY) ?
      'data-l10n-id="hour-allday"' :
      `data-l10n-date-format="${format}"`;
    return `<span class="${className}" data-date="${date}" ${l10nAttr}>
              ${displayHour}
            </span>`;
  }
});

});

define('templates/month_day_agenda',['require','exports','module','./date_span','template'],function(require, exports, module) {


var DateSpan = require('./date_span');
var create = require('template').create;

var MonthDayAgenda = create({
  event: function() {
    var busytimeId = this.h('busytimeId');
    var color = this.h('color');

    var eventTime;
    if (this.arg('isAllDay')) {
      eventTime = '<div class="all-day" data-l10n-id="hour-allday"></div>';
    } else {
      var startTime = formatTime(this.arg('startTime'));
      var endTime = formatTime(this.arg('endTime'));
      eventTime = `<div class="start-time">${startTime}</div>
        <div class="end-time">${endTime}</div>`;
    }

    var title = this.h('title');
    var eventDetails = `<h5 role="presentation">${title}</h5>`;
    var location = this.h('location');
    if (location && location.length > 0) {
      eventDetails += `<span class="details">
        <span class="location">${location}</span>
      </span>`;
    }

    var alarmClass = this.arg('hasAlarms') ? 'has-alarms' : '';

    return `<a href="/event/show/${busytimeId}/" class="event ${alarmClass}"
      role="option" aria-describedby="${busytimeId}-icon-calendar-alarm">
      <div class="container">
      <div class="gaia-icon icon-calendar-dot" style="color:${color}"
          aria-hidden="true"></div>
        <div class="event-time">${eventTime}</div>
        <div class="event-details" dir="auto">${eventDetails}</div>
        <div id="${busytimeId}-icon-calendar-alarm" aria-hidden="true"
          class="gaia-icon icon-calendar-alarm" style="color:${color}"
          data-l10n-id="icon-calendar-alarm"></div>
      </div>
      </a>`;
  },

  eventDescription: function() {
    var description = this.h('eventDescription');

    if (_isPortraitMode()) {
      return `<div class="event-details" dir="auto"><h5 role="presentation" class="p-pri">${description}</h5></div>`;
    }

    return getDescriptionModeLandscape(description);
  }
});

module.exports = MonthDayAgenda;

function formatTime(time) {
  return DateSpan.time.render({
    time: time,
    format: 'shortTimeFormat'
  });
}


function _isPortraitMode() {
  return screen.orientation.type.startsWith('portrait');
}

function getDescriptionModeLandscape(label) {
  var _separator = " ";
  var partials = label.split(_separator);
  partials[1] = partials[1].toLocaleLowerCase();
  var busy = partials.join(_separator);
  var busyEvent = '';

  if (parseInt(partials[0])) {
    busy = partials[0];
    busyEvent = partials[1];
  }

  return `<div class="event-details" dir="auto"><h5 role="presentation" class="p-pri busy ${busyEvent ? 'events' : ''}">${busy}</h5><span class="busy_event">${busyEvent}</span></div>`;

}

});

define('views/month_day_agenda',['require','exports','module','view','calc','date_format','day_observer','calc','templates/month_day_agenda'],function(require, exports, module) {


var Parent = require('view');
var createDay = require('calc').createDay;
var dateFormat = require('date_format');
var dayObserver = require('day_observer');
var isAllDay = require('calc').isAllDay;
var template = require('templates/month_day_agenda');

function MonthDayAgenda() {
  Parent.apply(this, arguments);
  this._render = this._render.bind(this);
  this.controller = this.app.timeController;
}
module.exports = MonthDayAgenda;

MonthDayAgenda.prototype = {
  __proto__: Parent.prototype,

  date: null,

  selectors: {
    element: '#month-day-agenda',
    events: '.day-events',
    currentDate: '#event-list-date'
  },

  get element() {
    return this._findElement('element');
  },

  get events() {
    return this._findElement('events');
  },

  get currentDate() {
    return this._findElement('currentDate');
  },

  onactive: function() {
    Parent.prototype.onactive.call(this);
    this.controller.on('selectedDayChange', this);
    this.changeDate(this.controller.selectedDay);
  },

  oninactive: function() {
    Parent.prototype.oninactive.call(this);
    if (this.date) {
      dayObserver.off(this.date, this._render);
    }
    this.controller.removeEventListener('selectedDayChange', this);
    this.date = null;
  },

  _isPortraitMode: function(){
    return screen.orientation.type.startsWith('portrait');
  },  
  changeDate: function(date) {
    // first time view is active the `selectedDay` is null
    date = date || createDay(new Date());

    if (this.date) {
      dayObserver.off(this.date, this._render);
    }
    this.date = date;
    dayObserver.on(this.date, this._render);

    var formatId = this._isPortraitMode() ? 'months-day-view-header-format' : 'months-day-view-header-format-small';
    this.currentDate.innerHTML = dateFormat.localeFormat(
      date,
      navigator.mozL10n.get(formatId)
    );
    // we need to set the [data-date] and [data-l10n-date-format] because
    // locale might change while the app is still open
    this.currentDate.dataset.date = date;
    this.currentDate.dataset.l10nDateFormat = formatId;
  },

  _render: function(records) {
    // we should always render allday events at the top
    var description = null;
    if (records.amount === 0) {
      description = navigator.mozL10n.get('no-events');
    } else if(records.amount === 1) {
      description = records.amount + " " + navigator.mozL10n.get('tcl-single-event');
    } else if(records.amount > 1) {
      description = records.amount + " " + navigator.mozL10n.get('tcl-event-count');
    }
    this.events.innerHTML = template.eventDescription.render({
      eventDescription: description
    });
      
    this.element.classList.toggle('no-event', records.amount === 0);

  },

  _renderEvent: function(record) {
    var {event, busytime, color} = record;
    var {startDate, endDate} = busytime;

    return template.event.render({
      hasAlarms: !!(event.remote.alarms && event.remote.alarms.length),
      busytimeId: busytime._id,
      color: color,
      title: event.remote.title,
      location: event.remote.location,
      startTime: startDate,
      endTime: endDate,
      isAllDay: isAllDay(this.date, startDate, endDate)
    });
  },

  handleEvent: function(e) {
    switch (e.type) {
      case 'selectedDayChange':
        this.changeDate(e.data[0]);
        break;
    }
  }
};

});

define('views/time_header',['require','exports','module','view','date_format','router','calc'],function(require, exports, module) {


var View = require('view');
var dateFormat = require('date_format');
var router = require('router');
var Calc = require('calc');

var SETTINGS = /settings/;

function TimeHeader() {
  View.apply(this, arguments);
  this.controller = this.app.timeController;
  this.controller.on('scaleChange', this);

  this.element.addEventListener('action', (e) => {
    e.stopPropagation();
    var path = window.location.pathname;
    if (SETTINGS.test(path)) {
      router.resetState();
    } else {
      router.show('/settings/');
    }
  });
}
module.exports = TimeHeader;

TimeHeader.prototype = {
  __proto__: View.prototype,

  selectors: {
    element: '#time-header',
    title: '#time-header h1'
  },

  scales: {
    month: 'multi-month-view-header-format',
    day: 'day-view-header-format',
    // when week starts in one month and ends
    // in another, we need both of them
    // in the header
    multiMonth: 'multi-month-view-header-format'
  },

  handleEvent: function(e) {
    // Week view will update title by self.
    if (window.location.pathname == '/week/') {
      return;
    }
    // respond to all events here but
    // we add/remove listeners to reduce
    // calls
    switch (e.type) {
      case 'yearChange':
      case 'monthChange':
      case 'dayChange':
      case 'weekChange':
        this._updateTitle();
        break;
      case 'scaleChange':
        this._updateScale.apply(this, e.data);
        break;
    }
  },

  get title() {
    return this._findElement('title');
  },

  _scaleEvent: function(event) {
    switch (event) {
      case 'month':
        return 'monthChange';
      case 'year':
        return 'yearChange';
      case 'week':
        return 'weekChange';
    }

    return 'dayChange';
  },

  _updateScale: function(newScale, oldScale) {
    if (oldScale) {
      this.controller.removeEventListener(
        this._scaleEvent(oldScale),
        this
      );
    }

    this.controller.addEventListener(
      this._scaleEvent(newScale),
      this
    );

    this._updateTitle();
  },

  getScale: function(type) {
    var position = this.controller.position;
    if (type === 'week') {
      var l10nLabel = navigator.mozL10n.get('week-view-header-format', {
        value: Calc.getWeeksOfYear(position)
      });

      return dateFormat.localeFormat(position, l10nLabel);
    }
    return this._localeFormat(position, type || 'month');
  },

  _getLastWeekday: function(){
    // we display 5 days at a time, controller.position is always the day on
    // the left of the view
    var position = this.controller.position;
    return new Date(
      position.getFullYear(),
      position.getMonth(),
      position.getDate() + 6
    );
  },

  _localeFormat: function(date, scale) {
    return dateFormat.localeFormat(
      date,
      navigator.mozL10n.get(this.scales[scale])
    );
  },

  _updateTitle: function() {
    var con = this.app.timeController;
    var title = this.title;

    title.dataset.l10nDateFormat =
      this.scales[con.scale] || this.scales.month;

    title.dataset.date = con.position.toString();

    title.textContent = this.getScale(
      con.scale
    );
  },

  render: function() {
    this._updateScale(this.controller.scale);
  }
};

});

define("bundle", function(){});
