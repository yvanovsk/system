'use strict';
/* global CarrierInfoNotifier */
/* global MobileOperator */
/* global BaseModule */

const GSM_CMAS_LOWER_BOUND = 4370;  //0x1112
const GSM_CMAS_UPPER_BOUND = 4400;  //0x1130
const CDMA_CMAS_LOWER_BOUND = 4096; //0x1000
const CDMA_CMAS_UPPER_BOUND = 4351; //0x10FF
const OPERATOR_CHANNELS = [911,919,112];// Task5642356-chengyanzhang@t2mobile.com-for add operator alert-add

(function(exports) {
  /**
   * CellBroadcastSystem
   * @class CellBroadcastSystem
   * @requires CarrierInfoNotifier
   * @requires MobileOperator
   */
  var CellBroadcastSystem = function() {}

  CellBroadcastSystem.SETTINGS = [
    'ril.normal-cellbroadcast.enabled',
    'ril.cellbroadcast.disabled'
  ];

  BaseModule.create(CellBroadcastSystem, {
    name: 'CellBroadcastSystem',

    EVENT_PREFIX: 'cellbroadcast',

    DEBUG: true,

    '_observe_ril.cellbroadcast.disabled': function(value) {
      if ('undefined' === typeof(this._settings['ril.normal-cellbroadcast.enabled'])) {
        return;
      }
      this.toggleCellbroadcast();
    },

    '_observe_ril.normal-cellbroadcast.enabled': function(value) {
      if ('undefined' === typeof(this._settings['ril.cellbroadcast.disabled'])) {
        return;
      }
      this.toggleCellbroadcast();
    },

    toggleCellbroadcast: function() {
      if (this._hasNormalCBSDisabled()) {
        this.debug('disabled!');
        navigator.mozCellBroadcast.onreceived = null;
      } else {
        this.debug('enabled!');
        navigator.mozCellBroadcast.onreceived = this.show.bind(this);
      }
      this.publish('msgchanged');
    },

    isEmergencyAlert: function (message) {
      if (message.cdmaServiceCategory) {
        return (message.cdmaServiceCategory >= CDMA_CMAS_LOWER_BOUND &&
          message.cdmaServiceCategory <= CDMA_CMAS_UPPER_BOUND);
      } else {
        return (message.messageId >= GSM_CMAS_LOWER_BOUND &&
          message.messageId < GSM_CMAS_UPPER_BOUND);
      }
    },

    //Task5642356-chengyanzhang@t2mobile.com-for add operator alert-begin
    isOperatorChannels(messageId){
      var id = parseInt(messageId);
      if(OPERATOR_CHANNELS.indexOf(id)!==-1){
        return true;
      }
      return false;
    },
    //Task5642356-chengyanzhang@t2mobile.com-for add operator alert-end

    /**
     * Shows the cell broadcast notification.
     * @memberof CellBroadcastSystem.prototype
     */
    show: function cbs_show(event) {
      this.debug('will show cb if it is not emergency');
      var msg = event.message;
      var serviceId = msg.serviceId || 0;
      var conn = window.navigator.mozMobileConnections[serviceId];
      var id = msg.messageId;

      // Task5108059-chengyanzhang@t2mobile.com-for disable cb in settings-begin
      navigator.customization.getValue('def.enable.cellbroadcast').then((isEnableCellBroadcast) => {
            dump('system app isEnableCellBroadcast===>' +isEnableCellBroadcast);
            if (isEnableCellBroadcast!==undefined && !isEnableCellBroadcast) {
                return;
            }
      });
      // Task5108059-chengyanzhang@t2mobile.com-for disable cb in settings-end

      // Early return CMAS messsage and let network alert app handle it. Please
      // ref http://www.etsi.org/deliver/etsi_ts/123000_123099/123041/
      // 11.06.00_60/ts_123041v110600p.pdf, chapter 9.4.1.2.2 GSM　Message
      // identifier Message id from range 4370 to 4399(1112 hex to 112f hex),
      // and CDMA　Message identifier Message id from range 4096 to 4351(0x1000
      // hex ro 0x10ff hex) should be CMAS message and network alert will
      // display detail information.

      if (this.isEmergencyAlert(msg)) {
        return;
      }

      //Task5642356-chengyanzhang@t2mobile.com-for add operator alert-begin
      if(this.isOperatorChannels(id)){
        return;
      }
      //Task5642356-chengyanzhang@t2mobile.com-for add operator alert-end

      if (conn &&
          conn.voice.network.mcc === MobileOperator.BRAZIL_MCC &&
          id === MobileOperator.BRAZIL_CELLBROADCAST_CHANNEL) {
        var evt = new CustomEvent('cellbroadcastmsgchanged',
          { detail: msg.body });
        window.dispatchEvent(evt);
        return;
      }



      if (this._hasNormalCBSDisabled()) {
        return;
      }

      var body = msg.body;

      // XXX: 'undefined' test until bug-1021177 lands
      if (msg.etws && (!body || (body == 'undefined'))) {
        body = navigator.mozL10n.get('cb-etws-warningType-' +
          (msg.etws.warningType ? msg.etws.warningType : 'other'));
      }

      CarrierInfoNotifier.show(body,
        navigator.mozL10n.get('cb-channel', { channel: id }));
    },

    /**
     * To make sure there is any CBS pref is disabled
     * @memberof CellBroadcastSystem.prototype
     */
    _hasNormalCBSDisabled: function() {
      if (!this._settings) {
        return true;
      }
      var enabled =
        this._settings['ril.cellbroadcast.disabled'].some(disabled => (disabled === false));
      return !this._settings['ril.normal-cellbroadcast.enabled'] && enabled;
    }
  });
}(window));
