'use strict';(function(exports){var _id=0;function Card(config){if(config){for(var key in config){this[key]=config[key];}}
this.instanceID=_id++;return this;}
Card.prototype=Object.create(BaseUI.prototype);Card.prototype.constructor=Card;Card.prototype.EVENT_PREFIX='card-';Card.prototype.containerElement=null;Card.prototype.CLASS_NAME='Card';Card.prototype.element=null;Card.prototype.closeButtonVisibility='hidden';Card.prototype.favoriteButtonVisibility='hidden';Card.prototype.toString=function(){return'['+this.CLASS_NAME+' '+
this.position+':'+this.title+']';};Card.prototype.getScreenshotPreviewsSetting=function(){return this.manager.useAppScreenshotPreviews;};Card.prototype.template=function(){return Tagged.escapeHTML`<div class="titles">
     <h1 id="${this.titleId}" dir="auto" class="title">${this.title}</h1>
     <p class="subtitle">
      <span class="subtitle-url">${this.subTitle}</span>
     </p>
    </div>

    <div class="screenshotView bb-button" data-l10n-id="openCard"
      role="link"></div>
    <div class="privateOverlay"></div>
    <div class="appIconView" style="background-image:${this.iconValue}"></div>

    <footer class="card-tray">
     <button class="appIcon" data-l10n-id="openCard"
       data-button-action="select" aria-hidden="true"></button>
     <menu class="buttonbar">
       <button class="close-button bb-button" data-l10n-id="closeCard"
         data-button-action="close" role="button"
         style="visibility: ${this.closeButtonVisibility}"></button>
      <button class="favorite-button bb-button"
        data-button-action="favorite" role="button"
        style="visibility: ${this.favoriteButtonVisibility}"></button>
     </menu>
    </footer>`;};Card.prototype.view=function c_view(){return this.template();};Card.prototype._populateViewData=function(){var app=this.app;this.title=(app.isBrowser()&&app.title)?app.title:app.shortName||app.name;this.sslState=app.getSSLState();this.subTitle='';this.iconValue='';this.closeButtonVisibility='visible';this.viewClassList=['card','appIconPreview'];this.titleId='card-title-'+this.instanceID;if(app.isPrivate){this.viewClassList.push('private');}
var iconURI=CardsHelper.getIconURIForApp(this.app);if(iconURI){this.iconValue='url('+iconURI+')';}
var origin=app.origin;var popupFrame;var frameForScreenshot=app.getFrameForScreenshot();var displayUrl='';if(app.isBrowser()){displayUrl=app.config.url||origin;if(displayUrl.startsWith('app://')){displayUrl=false;}}else if(frameForScreenshot&&CardsHelper.getOffOrigin(frameForScreenshot.src,origin)){displayUrl=CardsHelper.getOffOrigin(frameForScreenshot.src,origin);}
if(displayUrl){this.subTitle=this.getDisplayURLString(displayUrl);this.viewClassList.push('show-subtitle');}
if(TrustedUIManager.hasTrustedUI(app.origin)){popupFrame=TrustedUIManager.getDialogFromOrigin(app.origin);this.title=CardsHelper.escapeHTML(popupFrame.name||'',true);this.viewClassList.push('trustedui');}else if(!this.app.killable()){this.closeButtonVisibility='hidden';}};Card.prototype.move=function(deltaX,deltaY){deltaX=deltaX||0;deltaY=deltaY||0;var windowWidth=this.manager.windowWidth||window.innerWidth;var offset=this.position-this.manager.position;var positionX=deltaX+offset*(windowWidth*0.55);this.element.dataset.positionX=positionX;var style={transform:''};if(deltaX||offset){style.transform='translateX('+positionX+'px)';}
if(deltaY){style.transform='translateY('+deltaY+'px)';}
this.applyStyle(style);};Card.prototype.render=function(){this.publish('willrender');var elem=this.element||(this.element=document.createElement('li'));elem.dataset.position=this.position;elem.dataset.appInstanceId=this.app.instanceID;elem.dataset.origin=this.app.origin;this._populateViewData();elem.innerHTML=this.view();elem.setAttribute('aria-labelledby',this.titleId);elem.setAttribute('role','group');if(this.sslState){elem.dataset.ssl=this.sslState;}
elem.setAttribute('aria-labelledby',this.titleId);this.viewClassList.forEach(function(cls){elem.classList.add(cls);});if(this.containerElement){this.containerElement.appendChild(elem);}
this._fetchElements();this._updateDisplay();this.publish('rendered');return elem;};Card.prototype.applyStyle=function(nameValues){var style=this.element.style;for(var property in nameValues){if(undefined===nameValues[property]){delete style[[property]];}else{style[property]=nameValues[property];}}};Card.prototype.setVisibleForScreenReader=function(visible){this.element.setAttribute('aria-hidden',!visible);};Card.prototype.killApp=function(){this.app.kill();};Card.prototype.destroy=function(){this.publish('willdestroy');var element=this.element;if(element&&element.parentNode){element.parentNode.removeChild(element);}
this.element=this.manager=this.app=null;this.publish('destroyed');};Card.prototype._updateDisplay=function c_updateDisplay(){var elem=this.element;var app=this.app;if(app.isBrowser()){elem.classList.add('browser');}
var screenshotView=this.screenshotView;var isIconPreview=!this.getScreenshotPreviewsSetting();if(isIconPreview){elem.classList.add('appIconPreview');}else{elem.classList.remove('appIconPreview');if(screenshotView.style.backgroundImage){return;}}
if(this.iconValue){this.iconButton.style.backgroundImage=this.iconValue;}
if(isIconPreview){return;}
var cachedLayer;if(app.isActive()){cachedLayer=app.requestScreenshotURL();screenshotView.classList.toggle('fullscreen',app.isFullScreen());if(app.appChrome){screenshotView.classList.toggle('maximized',app.appChrome.isMaximized());}}
screenshotView.style.backgroundImage=(cachedLayer?'url('+cachedLayer+')':'none')+','+'-moz-element(#'+this.app.instanceID+')';};Card.prototype._fetchElements=function c__fetchElements(){this.screenshotView=this.element.querySelector('.screenshotView');this.titleNode=this.element.querySelector('h1.title');this.iconButton=this.element.querySelector('.appIcon');};Card.prototype.getDisplayURLString=function(url){var anURL;try{anURL=this.app?new URL(url,this.app.origin):new URL(url);}catch(e){return url;}
var displayString=url.substring(url.indexOf(anURL.host));return displayString;};return(exports.Card=Card);})(window);