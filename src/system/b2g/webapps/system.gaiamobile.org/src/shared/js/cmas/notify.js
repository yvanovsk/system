/* global
  AudioContext
 */

(function(exports) {
  'use strict';

  const SETTINGS = {
    notificationVolume: 'audio.volume.notification',
    vibration: 'vibration.enabled'
  };

  //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-begin
  //const ATTENTION_PATTERN = [4, 1, 2, 1, 2, 1, 4, 1, 2, 1, 2];
  //ATTENTION_PATTERN.push(1);
  const ATTENTION_PATTERN = [4, 1, 2, 1, 2, 1];
  //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-end
  ATTENTION_PATTERN.push(...ATTENTION_PATTERN);

  const ATTENTION_SOUND_VOLUME = 0.3;

  function getSetting(key) {
    if (!navigator.mozSettings) {
      return Promise.reject(new Error('The mozSettings API is not available.'));
    }

    return navigator.mozSettings.createLock().get(key).then(
      (result) => result[key]
    );
  }

  function getSettings(settings) {
    return Promise.all(
      settings.map(getSetting)
    ).catch((e) => {
      // catch and log errors
      console.error('Error while retrieving settings', e.message, e);
      return settings.map(() => null);
    }).then((results) => {
      return settings.reduce((result, setting, i) => {
        result[setting] = results[i];
        return result;
      }, {});
    });
  }

  // Converts from the ATTENTION_PATTERN (suitable for the Vibration API) to a
  // Float32Array suitable for the Audio API.
  //
  // The Float32Array will be interpolated so we just need to have the changes.
  // Each value will last a "unit" of time.
  //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-begin
  //function getAttentionCurveWave() {
  function getAttentionCurveWave(dur) {
    var result = [];
    var currentValue = ATTENTION_SOUND_VOLUME;

    var patternArray=getAttentionPattern(dur);
    //ATTENTION_PATTERN.forEach(duration => {
    patternArray.forEach(duration => {
    //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-end
      for (var i = 0; i < duration; i++) {
        result.push(currentValue);
      }

      currentValue = ATTENTION_SOUND_VOLUME - currentValue;
    });

    return new Float32Array(result);
  }

  //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-begin
  // function ringtone() {
  function ringtone(dur) {
    debug("notify ringtone playTime = " +dur );
    var playTime =11;
    if(dur!==undefined) {
      playTime = dur;
    }
    //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-end
    var audioChannel = 'notification';
    var audioCtx = new AudioContext(audioChannel);

    var o1 = audioCtx.createOscillator();
    var o2 = audioCtx.createOscillator();
    var gain = audioCtx.createGain();

    var time = audioCtx.currentTime;
    o1.type = o2.type = 'sine';
    o1.frequency.value = 853;
    o2.frequency.value = 960;

    o1.start();
    o2.start();
    // Eventually stop the oscillator to allow garbage collecting.
    //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-begin
    //o1.stop(time + 11);
    //o2.stop(time + 11);
    o1.stop(time + playTime);
    o2.stop(time + playTime);

    var wave = getAttentionCurveWave(playTime);
    gain.gain.setValueCurveAtTime(wave, time, playTime);
    //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-end

    o1.connect(gain);
    o2.connect(gain);
    gain.connect(audioCtx.destination);

    // Task5066737-chengyanzhang@t2mobile.com-for customization-begin
    window.addEventListener('control_ring_tone', function controlRingTone(event) {
      debug('ringtone::control_ring_tone -> ' + JSON.stringify(event.detail));
      if(!event.detail.enable) {
        if(audioCtx.state && audioCtx.state === 'running') {
          audioCtx.suspend().then(function () {
            debug('Resume context');
          });
        } else {
          gain.disconnect(audioCtx.destination);
        }
      } else {
        window.removeEventListener('control_ring_tone', controlRingTone);
        if(audioCtx.state && audioCtx.state === 'suspended') {
          audioCtx.resume().then(function () {
            debug('Suspend context');
          });
        } else {
          gain.connect(audioCtx.destination);
        }
      }
    });
    // Task5066737-chengyanzhang@t2mobile.com-for customization-end
  }

  //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-begin
  // function vibrate() {
  function vibrate(dur) {
    debug("notify vibrate vibrateTime = " + dur );
    var patternArray=ATTENTION_PATTERN;
    if(dur!==undefined){
      patternArray =getAttentionPattern(dur);
    }
    // var pattern = ATTENTION_PATTERN.map((value) => value * 500);
    var pattern = patternArray.map((value) => value * 500);
    //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-begin
    // vibration only works when App is in the foreground
    if (document.hidden) {
      window.addEventListener('visibilitychange', function waitOn() {
        window.removeEventListener('visibilitychange', waitOn);
        navigator.vibrate(pattern);
      });
    } else {
      navigator.vibrate(pattern);
    }
  }

  //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-begin
  function getAttentionPattern(dur){
    var pattern =ATTENTION_PATTERN;
    switch(dur) {
      case 30:
        pattern =[4, 1, 2, 1, 2, 1, 4, 1, 2, 1, 2, 1, 4, 1, 2, 1, 2, 1, 4, 1, 2, 1, 2, 1, 4, 1, 2, 1, 2, 1, 4, 1, 1];
        break;
      case 10.5:
        pattern= [4, 1, 2, 1, 2, 1, 4, 1, 2, 1, 2];
        break;
      case 5:
        pattern= [4, 1, 2, 1, 2];
        break;
     }
    return pattern;
  }
  //Task5066737-chengyanzhang@t2mobile.com-for custom vibrate and sound-end

  var Notify = {
    notify: function notification_ringtone() {
      return getSettings(
        [SETTINGS.notificationVolume, SETTINGS.vibration]
      ).then((settings) => {
        if (settings[SETTINGS.notificationVolume] && !navigator.mozAudioChannelManager.headphones) {
          ringtone();
        }

        if (settings[SETTINGS.vibration]) {
          vibrate();
        }
      });
    },

    //Task5066737-chengyanzhang@t2mobile.com-for customization-begin
    ringtone: ringtone,
    vibrate: vibrate
    //Task5066737-chengyanzhang@t2mobile.com-for customization-end
  };

  exports.Notify = Notify;
}(this));
