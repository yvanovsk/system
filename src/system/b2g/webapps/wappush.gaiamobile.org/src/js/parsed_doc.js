
(function(exports){'use strict';var CONTENT_TYPE='application/xml';var AUTHTYPE_MAPPING={'PAP':'1','CHAP':'2'};var TYPE_MAPPING={'w2':'default','25':'default','110':'default','143':'default','w4':'mms','ap0004':'supl'};function ParsedProvisioningDoc(provisioningDoc){this._provisioningDoc=provisioningDoc;this._applicationNodes=null;this._napDefNodes=null;this._pxLogicalNodes=null;this._pxPhysicalNodes=null;this._apns=[],this._proxies=[];this._apnsReady=false;}
ParsedProvisioningDoc.prototype={getPxPhysicalNodes:function pd_getPxPhysicalNodes(toProxyNode){var toProxy=toProxyNode.getAttribute('value');var pxLogicalNode=null;for(var i=0;i<this._pxLogicalNodes.length;i++){var selector='parm[name="PROXY-ID"][value="'+toProxy+'"]';pxLogicalNode=this._pxLogicalNodes[i].querySelector(selector);if(pxLogicalNode){break;}}
if(!pxLogicalNode){return null;}
return this._pxLogicalNodes[i].querySelectorAll('characteristic[type="PXPHYSICAL"]');},getNapDefNode:function pd_getNapDefNode(toNapId){for(var i=0;i<this._napDefNodes.length;i++){var selector='parm[name="NAPID"][value="'+toNapId+'"]';var napDefNode=this._napDefNodes[i].querySelector(selector);if(napDefNode){return this._napDefNodes[i];}}
return null;},parse:function pd_parse(){if(!this._provisioningDoc){return;}
function parsePxPhysicalNode(pxPhysicalNode,isMmsProxy){var obj={};var proxyKey=null,portKey=null;proxyKey=isMmsProxy?'mmsproxy':'proxy';portKey=isMmsProxy?'mmsport':'port';var pxAddrNode=pxPhysicalNode.querySelector('parm[name="PXADDR"]');if(pxAddrNode){obj[proxyKey]=pxAddrNode.getAttribute('value');}
obj['TO-NAPID']=[];var toNapIdNodes=pxPhysicalNode.querySelectorAll('parm[name="TO-NAPID"]');if(toNapIdNodes){for(var i=0;i<toNapIdNodes.length;i++){var toNapIdNode=toNapIdNodes[i];if(toNapIdNode){obj['TO-NAPID'].push(toNapIdNode.getAttribute('value'));}}}
var portNodes=pxPhysicalNode.querySelectorAll('characteristic[type="PORT"]');for(var j=0;j<portNodes.length;j++){var portNode=portNodes[j].querySelector('parm[name="PORTNBR"]');obj[portKey]=portNode.getAttribute('value');}
var proxyIdNode=pxPhysicalNode.querySelector('parm[name="PHYSICAL-PROXY-ID"]');if(proxyIdNode){obj.pxPhId=proxyIdNode.getAttribute('value');}
var pxPhDomainNode=pxPhysicalNode.querySelector('parm[name="DOMAIN"]');if(pxPhDomainNode){obj.pxPhDomain=pxPhDomainNode.getAttribute('value');}
return obj;}
function parseNapDefNode(napDefNode){function parseValidityNode(validityNode){var obj={};var countryNode=validityNode.querySelector('parm[name="COUNTRY"]');if(countryNode){obj.countrycode=countryNode.getAttribute('value');}
var networkNode=validityNode.querySelector('parm[name="NETWORK"]');if(networkNode){obj.networkcode=networkNode.getAttribute('value');}
var systemIdNode=validityNode.querySelector('parm[name="SID"]');if(systemIdNode){obj.sid=systemIdNode.getAttribute('value');}
var socNode=validityNode.querySelector('parm[name="SOC"]');if(socNode){obj.soc=socNode.getAttribute('value');}
var valduntilNode=validityNode.querySelector('parm[name="VALIDUNTIL"]');if(valduntilNode){obj.validuntil=valduntilNode.getAttribute('value');}
return obj;}
var obj={};var napIdNode=napDefNode.querySelector('parm[name="NAPID"]');if(napIdNode){obj.NAPID=napIdNode.getAttribute('value');}
var nameNode=napDefNode.querySelector('parm[name="NAME"]');if(nameNode){obj.carrier=nameNode.getAttribute('value');}
var apnNode=napDefNode.querySelector('parm[name="NAP-ADDRESS"]');if(apnNode){obj.apn=apnNode.getAttribute('value');}
var bearerNode=napDefNode.querySelector('parm[name="BEARER"]');if(bearerNode){obj.bearer=bearerNode.getAttribute('value');}
var apnFormatNode=napDefNode.querySelector('parm[name="NAP-ADDRTYPE"]');if(apnFormatNode){obj.apnFormat=apnFormatNode.getAttribute('value');}
var napAuthInfoNode=napDefNode.querySelector('characteristic[type="NAPAUTHINFO"]');if(napAuthInfoNode){var authTypeNode=napAuthInfoNode.querySelector('parm[name="AUTHTYPE"]');if(authTypeNode){var authType=AUTHTYPE_MAPPING[authTypeNode.getAttribute('value')];if(authType){obj.authtype=authType;}}
var authNameNode=napAuthInfoNode.querySelector('parm[name="AUTHNAME"]');if(authNameNode){obj.user=authNameNode.getAttribute('value');}
var authSecretNode=napAuthInfoNode.querySelector('parm[name="AUTHSECRET"]');if(authSecretNode){obj.password=authSecretNode.getAttribute('value');}
var authentryNode=napAuthInfoNode.querySelector('parm[name="AUTH-ENTITY"]');if(authentryNode){obj.authEnty=authentryNode.getAttribute('value');}
var authSpiNode=napAuthInfoNode.querySelector('parm[name="SPI"]');if(authSpiNode){obj.spi=authSpiNode.getAttribute('value');}}
var validityNode=napDefNode.querySelector('characteristic[type="VALIDITY"]');if(validityNode){obj.validity=parseValidityNode(validityNode);}
return obj;}
var parser=new DOMParser();var domDocument=parser.parseFromString(this._provisioningDoc,CONTENT_TYPE);this._applicationNodes=domDocument.querySelectorAll('characteristic[type="APPLICATION"]');this._napDefNodes=domDocument.querySelectorAll('characteristic[type="NAPDEF"]');this._pxLogicalNodes=domDocument.querySelectorAll('characteristic[type="PXLOGICAL"]');this._pxPhysicalNodes=domDocument.querySelectorAll('characteristic[type="PXPHYSICAL"]');var napDefNode=null;var apn=null;var type=[];var proxy=null;if(!this._applicationNodes.length){for(var i=0;i<this._napDefNodes.length;i++){napDefNode=this._napDefNodes[i];apn=parseNapDefNode(napDefNode);var defapn={};for(var key in apn){defapn[key]=apn[key];}
type=[];type.push('default');defapn.types=type;this._apns.push(defapn);var suplapn={};for(var key in apn){suplapn[key]=apn[key];}
type=[];type.push('supl');suplapn.types=type;this._apns.push(suplapn);}
for(var i=0;i<this._pxPhysicalNodes.length;i++){var pxPhysicalNode=this._pxPhysicalNodes[i];proxy=parsePxPhysicalNode(pxPhysicalNode,false);this._proxies.push(proxy);}
return;}
var applicationNode=null;for(var j=0;j<this._applicationNodes.length;j++){applicationNode=this._applicationNodes[j];var appIdNode=null,appId=null;appIdNode=applicationNode.querySelector('parm[name="APPID"]');appId=appIdNode.getAttribute('value');var toProxyNodes=null;toProxyNodes=applicationNode.querySelectorAll('parm[name="TO-PROXY"]');var toNapIdNodes=null;toNapIdNodes=applicationNode.querySelectorAll('parm[name="TO-NAPID"]');var addrNode=null,addr=null;addrNode=applicationNode.querySelector('parm[name="ADDR"]');if(addrNode){addr=addrNode.getAttribute('value');}
if(appId&&(appId!=='w2')&&(appId!=='w4')){continue;}
if(!toNapIdNodes&&!toProxyNodes){continue;}
type=[];type.push(TYPE_MAPPING[appId]);if(toProxyNodes){for(var l=0;l<toProxyNodes.length;l++){var pxPhysicalNodes=this.getPxPhysicalNodes(toProxyNodes[l]);if(!pxPhysicalNodes){continue;}
for(var m=0;m<pxPhysicalNodes.length;m++){proxy=parsePxPhysicalNode(pxPhysicalNodes[m],(TYPE_MAPPING[appId]==='mms'));this._proxies.push(proxy);for(var n=0;n<proxy['TO-NAPID'].length;n++){napDefNode=this.getNapDefNode(proxy['TO-NAPID'][n]);apn=parseNapDefNode(napDefNode);apn.types=type;if((TYPE_MAPPING[appId]==='mms')&&addr){apn.mmsc=addr;}
this._apns.push(apn);}}}}
if(toNapIdNodes){for(var o=0;o<toNapIdNodes.length;o++){var toNapId=toNapIdNodes[o].getAttribute('value');if(!toNapId){continue;}
napDefNode=this.getNapDefNode(toNapId);apn=parseNapDefNode(napDefNode);apn.types=type;if((TYPE_MAPPING[appId]==='mms')&&addr){apn.mmsc=addr;}
this._apns.push(apn);}}}},getApns:function ppd_getApns(){function addProperties(src,dst){for(var key in src){dst[key]=src[key];}}
if(this._apnsReady){return this._apns;}
for(var i=0;i<this._proxies.length;i++){var proxy=this._proxies[i];for(var j=0;j<proxy['TO-NAPID'].length;j++){var TO_NAPID=proxy['TO-NAPID'][j];for(var k=0;k<this._apns.length;k++){var apn=this._apns[k];if(TO_NAPID===apn.NAPID){addProperties(proxy,apn);}}}}
this._apnsReady=true;return this._apns;}};ParsedProvisioningDoc.from=function ppd_from(provisioningDoc){var obj=new ParsedProvisioningDoc(provisioningDoc);obj.parse();return obj;};exports.ParsedProvisioningDoc=ParsedProvisioningDoc;})(window);