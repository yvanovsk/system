/**
 * Asks the user to manually configure their account.
 */
/*jshint nonew: false */
/*global MozActivity */

define(['require','cards','confirm_dialog','form_navigation','tmpl!./tng/plain_socket_warning.html','./base','template!./setup_manual_config.html','./setup_account_error_mixin'],function(require) {

var cards = require('cards'),
    ConfirmDialog = require('confirm_dialog'),
    FormNavigation = require('form_navigation'),
    plainSocketWarningNode = require('tmpl!./tng/plain_socket_warning.html');

var self;
var dataChanged;

return [
  require('./base')(require('template!./setup_manual_config.html')),
  require('./setup_account_error_mixin'),
  {
    onArgs: function(args) {
      this.formItems = {
        common: {
          displayName: this._fromClass('sup-info-name'),
          emailAddress: this._fromClass('sup-info-email'),
          password: this._fromClass('sup-info-password'),
          passwordWrapper: this._fromClass('sup-manual-password-wrapper'),
          activeSyncShowpassword:
              this._fromClass('sup-manual-activesync-showpassword')
        },
        composite: {
          hostname: this._fromClass('sup-manual-composite-hostname'),
          port: this._fromClass('sup-manual-composite-port'),
          socket: this._fromClass('sup-manual-composite-socket'),
          username: this._fromClass('sup-manual-composite-username'),
          password: this._fromClass('sup-manual-composite-password')
        },
        smtp: {
          hostname: this._fromClass('sup-manual-smtp-hostname'),
          port: this._fromClass('sup-manual-smtp-port'),
          socket: this._fromClass('sup-manual-smtp-socket'),
          username: this._fromClass('sup-manual-smtp-username'),
          password: this._fromClass('sup-manual-smtp-password')
        },
        activeSync: {
          hostname: this._fromClass('sup-manual-activesync-hostname'),
          username: this._fromClass('sup-manual-activesync-username')
        }
      };

      this.compositeLiElements = Array.prototype.slice.call(
          this.compositeSection.querySelectorAll('li'));
      this.activeSyncLiElements = Array.prototype.slice.call(
          this.activeSyncSection.querySelectorAll('li'));

      var password = password || '';

      var common = this.formItems.common;
      common.displayName.value = args.displayName;
      common.emailAddress.value = args.emailAddress;
      common.password.value = password;

      var composite = this.formItems.composite;
      composite.username.value = args.emailAddress;
      composite.password.value = password;


      var smtp = this.formItems.smtp;
      smtp.username.value = args.emailAddress;
      smtp.password.value = password;

      this.changeIfSame(common.emailAddress,
                        [composite.username,
                         smtp.username]);
      this.changeIfSame(composite.username,
                        [smtp.username]);
      this.changeIfSame(composite.password,
                        [smtp.password,
                         common.password]);
      this.changeIfSame(smtp.password,
                        [composite.password,
                         common.password]);

      for (var type in this.formItems) {
        for (var field in this.formItems[type]) {
          if (this.formItems[type][field].tagName === 'INPUT') {
            this.formItems[type][field].addEventListener(
              'input', this.onInfoInput.bind(this));
          }
        }
      }

      this.requireFields('composite', true);
      this.requireFields('smtp', true);
      this.requireFields('activeSync', false);

      this.compositeSocketValue = composite.socket.value;
      this.smtpSocketValue = smtp.socket.value;
      this.socketChooseType = null;

      composite.socket.addEventListener('change',
                                       this.onChangeCompositeSocket.bind(this));
      smtp.socket.addEventListener('change',
                                   this.onChangeSmtpSocket.bind(this));

      this.onChangeAccountType({ target: this.accountTypeNode });

      this._formNavigation = new FormNavigation({
        formElem: this.formNode,
        onLast: this.onNext.bind(this)
      });

      self = this;
      dataChanged = false;
    },

    handleKeyDown: function(e) {
      if (!document.getElementById('Symbols')) {
        switch (e.key) {
          case 'Accept':
          case 'Enter':
            e.preventDefault();
            var selectEl =
                document.querySelector('.sup-manual-form .focus select');
            if (selectEl) {
              selectEl.focus();
            }
            break;
          case 'Backspace':
            e.preventDefault();
            if (document.querySelector('#confirm-dialog-container ' +
                'gaia-confirm')) {
              cards._endKeyClicked = false;
              self.resetSocketValue(self.socketChooseType);
            } else {
              self.onBack();
            }
            break;
        }
      }
    },

    endkeyHandler: function(e) {
      var cb = e.detail.callback;
      if (document.getElementById('Symbols')) {
        var evt = new CustomEvent('back-accepted');
        window.dispatchEvent(evt);
      }

      if (dataChanged) {
        var dialogConfig = {
          title: {
            id: 'confirm-dialog-title',
            args: {}
          },
          body: {
            id: 'leave-this-page-warning-message',
            args: {}
          },
          desc: {
            id: '',
            args: {}
          },
          cancel: {
            l10nId: 'cancel',
            priority: 1,
            callback: function(){
              cards._endKeyClicked = false;
            }
          },
          confirm: {
            l10nId: 'discard',
            priority: 3,
            callback: function() {
              cb();
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));
      } else {
        cb();
      }
    },

    onCardVisible: function(navDirection) {
      const CARD_NAME = this.localName;
      const QUERY_CHILD = CARD_NAME + ' ' + 'li:not(.collapsed)';
      const CONTROL_ID = CARD_NAME + ' ' + QUERY_CHILD;

      var form = this.formNode;
      var formLi = document.querySelectorAll('.sup-manual-form li');
      for (var i = 0; i < formLi.length; i++) {
        formLi[i].addEventListener('focus', function(e) {
          var inputEl = form.querySelector('.focus input');
          if (inputEl) {
            setTimeout( () => {
              inputEl.focus();
              inputEl.setSelectionRange(inputEl.value.length,
                  inputEl.value.length);
            }, 100);
          }
        });
      }

      console.log(this.localName + '.onCardVisible, navDirection=' +
                  navDirection);
      // forward: new card pushed
      if (navDirection === 'forward') {
        NavigationMap.navSetup(CARD_NAME, QUERY_CHILD);
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('first');
        this.updateSK(this.formNode.checkValidity());
      }
      // back: hidden card is restored
      else if (navDirection === 'back') {
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('restore');
        this.updateSK(this.formNode.checkValidity());
      }

      window.addEventListener('keydown', this.handleKeyDown);
      window.addEventListener('email-endkey', self.endkeyHandler);
      self.showpasswordImapInputNode.addEventListener('change', function() {
        document.querySelector('.sup-manual-composite-password').type =
            this.checked ? 'text' : 'password';
        self.updateSK(self.formNode.checkValidity());
      });
      self.showpasswordSmtpInputNode.addEventListener('change', function() {
        document.querySelector('.sup-manual-smtp-password').type =
            this.checked ? 'text' : 'password';
        self.updateSK(self.formNode.checkValidity());
      });
      self.showpasswordActiveSyncInputNode.addEventListener('change', function() {
        document.querySelector('.sup-manual-activesync-password').type =
            this.checked ? 'text' : 'password';
        self.updateSK(self.formNode.checkValidity());
      });
    },

    _fromClass: function(className) {
      return this.getElementsByClassName(className)[0];
    },

    onBack: function(event) {
      if (dataChanged) {
        var dialogConfig = {
          title: {
            id: 'confirm-dialog-title',
            args: {}
          },
          body: {
            id: 'leave-this-page-warning-message',
            args: {}
          },
          desc: {
            id: '',
            args: {}
          },
          cancel: {
            l10nId: 'cancel',
            priority: 1,
            callback: function() {}
          },
          confirm: {
            l10nId: 'discard',
            priority: 3,
            callback: function() {
              cards.removeCardAndSuccessors(self, 'animate', 1);
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));
      } else {
        cards.removeCardAndSuccessors(self, 'animate', 1);
      }
    },

    onNext: function(event) {
      //event.preventDefault(); // Prevent FormNavigation from taking over.
      var config = { type: this.accountTypeNode.value };

      if (config.type === 'imap+smtp' || config.type === 'pop3+smtp') {
        config.incoming = {
          hostname: this.formItems.composite.hostname.value,
          port: this.formItems.composite.port.value,
          socketType: this.formItems.composite.socket.value,
          username: this.formItems.composite.username.value,
          password: this.formItems.composite.password.value,
          authentication: 'password-cleartext'
        };
        config.outgoing = {
          hostname: this.formItems.smtp.hostname.value,
          port: this.formItems.smtp.port.value,
          socketType: this.formItems.smtp.socket.value,
          username: this.formItems.smtp.username.value,
          password: this.formItems.smtp.password.value,
          authentication: 'password-cleartext'
        };
      }
      else { // config.type === 'activesync'
        config.incoming = {
          server: 'https://' + this.formItems.activeSync.hostname.value,
          username: this.formItems.activeSync.username.value
        };
      }

      this.pushSetupCard(config);
    },

    pushSetupCard: function(config) {
      // For composite accounts where they've elected to have separate
      // passwords, use the composite password field. For everything
      // else, there's MasterCard. Uh, I mean, the common password.
      var password;
      if (this.accountTypeNode.value === 'activesync') {
        password = this.formItems.common.password.value;
      } else {
        password = this.formItems.composite.password.value;
      }
      // The progress card is the dude that actually tries to create the
      // account.
      cards.pushCard(
        'setup_progress', 'animate',
        {
          displayName: this.formItems.common.displayName.value,
          emailAddress: this.formItems.common.emailAddress.value,
          password: password,
          outgoingPassword: config.outgoing && config.outgoing.password,
          manualSetup: true,
          configInfo: config,
          callingCard: this
        },
        'right');
    },

    updateSK: function(validity) {
      var optCancel = {
        name: 'Cancel',
        l10nId: 'cancel',
        priority: 1,
        method: function() {
          self.onBack();
        }
      };
      var optSelect = {
        name: 'Select',
        l10nId: 'select',
        priority: 2
      };
      var optDeSelect = {
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2
      };
      var optNext = {
        name: 'Next',
        l10nId: 'setup-manual-next',
        priority: 3,
        method: function() {
          self.onNext();
        }
      };

      var optCsk = optSelect;
      var focusedItem = this.formNode.querySelector('.focus');
      if (focusedItem && focusedItem.childElementCount > 0) {
        if (focusedItem.children[0].classList
                .contains('setup-account-checkbox')) {
          var checked = focusedItem.children[0].children[0].checked;
          if (checked) {
            optCsk = optDeSelect;
          }
        }
      }
      var menuOptions = [optCancel, optCsk];
      var menuOptionsWithNext = [optCancel, optCsk, optNext];

      var focusedItem = this.formNode.querySelector('.focus'),
      inputText = focusedItem.querySelector(
          '[type="text"], [type="email"], [type="password"]'
      );

      if (validity) {
        if (focusedItem && inputText) {
          menuOptionsWithNext.splice(1, 1);
        }
        NavigationMap.setSoftKeyBar(menuOptionsWithNext);
      } else {
        if (focusedItem && inputText) {
          menuOptions.splice(1, 1);
        }
        NavigationMap.setSoftKeyBar(menuOptions);
      }
    },

    onFocusChanged: function() {
      this.updateSK(this.formNode.checkValidity());
    },

    onInfoInput: function(ignoredEvent) {
      this.updateSK(this.formNode.checkValidity());
      dataChanged = true;
    },

    /**
     * When sourceField changes, change every field in destFields to
     * match, if and only if destField previously matched sourceField.
     */
    changeIfSame: function(sourceField, destFields) {
      sourceField._previousValue = sourceField.value;
      sourceField.addEventListener('input', function(e) {
        for (var i = 0; i < destFields.length; i++) {
          var destField = destFields[i];
          if (destField.value === e.target._previousValue) {
            destField.value = destField._previousValue = e.target.value;
          }
        }
        sourceField._previousValue = e.target.value;
        this.onInfoInput(); // run validation
      }.bind(this));
    },

    updateLiCollapsed: function (isComposite) {
      if (isComposite) {
        this.compositeLiElements.forEach(function(item) {
          item.classList.remove('collapsed');
        });
        this.activeSyncLiElements.forEach(function(item) {
          item.classList.add('collapsed');
        });
      } else{
        this.compositeLiElements.forEach(function(item) {
          item.classList.add('collapsed');
        });
        this.activeSyncLiElements.forEach(function(item) {
          item.classList.remove('collapsed');
        });
      }
    },

    moveAccountTypeNodeIntoView: function(isComposite) {
      var evt = isComposite ? { key: 'ArrowUp' } : { key: 'ArrowDown' };
      var accountTypeNode =
          document.querySelector('.sup-manual-account-type-container.focus');
      if (accountTypeNode) {
        NavigationMap.scrollToElement(accountTypeNode, evt);
      }
    },

    onChangeAccountType: function(event) {
      var isComposite = (event.target.value === 'imap+smtp' ||
                         event.target.value === 'pop3+smtp');
      var isImap = event.target.value === 'imap+smtp';

      if (isComposite) {
        this.compositeSection.classList.remove('collapsed');
        this.activeSyncSection.classList.add('collapsed');
        this.manualImapTitle.classList.toggle('collapsed', !isImap);
        this.manualPop3Title.classList.toggle('collapsed', isImap);
        this.updateLiCollapsed(true);
      }
      else {
        this.compositeSection.classList.add('collapsed');
        this.activeSyncSection.classList.remove('collapsed');
        this.updateLiCollapsed(false);
      }

      this.formItems.common.passwordWrapper.classList.toggle(
        'collapsed', isComposite);
      this.formItems.common.activeSyncShowpassword.classList.toggle(
        'collapsed', isComposite);
      this.requireFields('composite', isComposite);
      this.requireFields('smtp', isComposite);
      this.requireFields('activeSync', !isComposite);
      this.onChangeCompositeSocket({ target: this.formItems.composite.socket });
      NavigationMap.navSetup('cards-setup-manual-config',
          'cards-setup-manual-config li:not(.collapsed)');
      this.moveAccountTypeNodeIntoView(isComposite);
    },

    // If the user selects a different socket type, autofill the most likely
    // port.
    onChangeCompositeSocket: function(event) {
      var isImap = this.accountTypeNode.value === 'imap+smtp';
      var SSL_VALUE = (isImap ? '993' : '995');
      var STARTTLS_VALUE = (isImap ? '143' : '110');

      var socketType = event.target.value;
      if (socketType === 'PLAIN') {
        this.showPlainSocketWarning('composite');
      } else {
        this.compositeSocketValue = socketType;
      }

      var portField = this.formItems.composite.port;
      if (socketType === 'SSL') {
        portField.value = SSL_VALUE;
      } else if (socketType === 'STARTTLS') {
        portField.value = STARTTLS_VALUE;
      }
    },

    onChangeSmtpSocket: function(event) {
      const SSL_VALUE = '465';
      const STARTTLS_VALUE = '587';
      const PLAIN_VALUE = '25';
      var socketType = event.target.value;
      var portField = this.formItems.smtp.port;

      if (socketType === 'PLAIN') {
        this.showPlainSocketWarning('smtp');
      } else {
        this.smtpSocketValue = socketType;
      }

      // Switch portField values to match defaults for the socketType, but only
      // if the existing value for portField is one of the other defaults, and
      // not a user-supplied value.
      if (socketType === 'SSL' &&
          (portField.value === STARTTLS_VALUE ||
           portField.value === PLAIN_VALUE)) {
        portField.value = SSL_VALUE;
      } else if (socketType === 'STARTTLS' &&
          (portField.value === SSL_VALUE ||
           portField.value === PLAIN_VALUE)) {
        portField.value = STARTTLS_VALUE;
      } else if (socketType === 'PLAIN' &&
          (portField.value === SSL_VALUE ||
           portField.value === STARTTLS_VALUE)) {
        portField.value = PLAIN_VALUE;
      }
    },

    requireFields: function(type, required) {
      for (var field in this.formItems[type]) {
        var item = this.formItems[type][field];
        if (!item.hasAttribute('data-maybe-required')) {
          continue;
        }

        if (required) {
          item.setAttribute('required', '');
        } else {
          item.removeAttribute('required');
        }
      }
    },

    resetSocketValue: function(type) {
      const SSL_VALUE = '465';
      const STARTTLS_VALUE = '587';
      const PLAIN_VALUE = '25';
      switch (type){
        case 'composite':
          self.formItems.composite.socket.value = self.compositeSocketValue;
          break;
        case 'smtp':
          self.formItems.smtp.socket.value = self.smtpSocketValue;
          switch (self.smtpSocketValue) {
            case 'SSL':
              self.formItems.smtp.port.value = SSL_VALUE;
              break;
            case 'STARTTLS':
              self.formItems.smtp.port.value = STARTTLS_VALUE;
              break;
            case 'PLAIN':
              self.formItems.smtp.port.value = PLAIN_VALUE;
              break;
          }
          break;
      }
    },

    showPlainSocketWarning: function(type) {
      this.socketChooseType = type;
      var dialogConfig = {
        title: {
          id: 'tng-plain-socket-warning-title',
          args: {}
        },
        body: {
          id: 'tng-plain-socket-warning-message',
          args: {}
        },
        desc: {
          id: 'tng-plain-socket-warning-message-options',
          args: {}
        },
        cancel: {
          l10nId: 'settings-account-delete-cancel',
          priority: 1,
          callback: function() {
            self.resetSocketValue(type);
          }
        },
        confirm: {
          l10nId: 'tng-plain-socket-warning-ok',
          priority: 3,
          callback: function() {}
        }
      };

      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('confirm-dialog-container'));
    },

    die: function() {
      this._formNavigation = null;
      window.removeEventListener('keydown', this.handleKeyDown);
      window.removeEventListener('email-endkey', self.endkeyHandler);
    }
  }
];
});
