'use strict';
(function(exports) {
  var editParamSelect = {
    header: { l10nId:'editSelect' },
    items: [
      {
        name: 'Select All',
        l10nId: 'calllog-all',
        priority: 1,
        method: function() {
          CallLog.selectAll();
          OptionHelper.show('edit-deselect');
        }
      }, {
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2,
        method: function() {
        }
      }, {
        l10nId: 'delete',
        name: 'Delete',
        priority: 3,
        method: function() {
          CallLog.deleteLogGroups();
        }
      },
    ]
  };

  var editParamNoSelectFocus = {
    header: { l10nId:'editSelect' },
    items: [
      {
        name: 'Select All',
        l10nId: 'calllog-all',
        priority: 1,
        method: function() {
          CallLog.selectAll();
          OptionHelper.show('edit-deselect');
        }
      }, {
        name: 'Select',
        l10nId: 'select',
        priority: 2,
        method: function() {
        }
      }, {
        l10nId: 'delete',
        name: 'Delete',
        priority: 3,
        method: function() {
          CallLog.deleteLogGroups();
        }
      },
    ]
  };

  var editParamSelectNone = {
    header: { l10nId: 'editSelect' },
    items: [
      {
        name: 'Select All',
        l10nId: 'calllog-all',
        priority: 1,
        method: function() {
          CallLog.selectAll();
          OptionHelper.show('edit-deselect');
        }
      }, {
        name: 'Select',
        l10nId: 'select',
        priority: 2,
        method: function() {
        }
      },
    ]
  };

  var editParamDeselect = {
    header: { l10nId:'editDeselect' },
    items: [
      {
        name: 'Deselect All',
        l10nId: 'calllog-none',
        priority: 1,
        method: function() {
          CallLog.deselectAll();
          OptionHelper.show('edit-select-none');
        }
      }, {
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2,
        method: function() {
        }
      }, {
        l10nId: 'delete',
        name: 'Delete',
        priority: 3,
        method: function() {
          CallLog.deleteLogGroups();
          OptionHelper.show('confirmation');
        }
      },
    ]
  };
  var confirmation = {
    header: { l10nId:'confirmation' },
    items: [
      {
        name: 'Cancel',
        l10nId: 'cancel',
        priority: 1,
        method: function() {
          var keyDown = new CustomEvent('keydown');
          keyDown.key = 'Backspace';
          window.dispatchEvent(keyDown);
        }
      },
      {
        l10nId: 'delete',
        name: 'Delete',
        priority: 3,
        method: function() {
          document.querySelector('#confirmation-message button[data-l10n-id="delete"]').click();
        }
      },
    ]
  };
  var callInfoParam = {
  header: { l10nId:'CallInfo' },
    items: [
      {
        name: 'Cancel',
        l10nId: 'tcl-cancel',
        priority: 1,
        method: function() {
          var keyDown = new CustomEvent('keydown');
          keyDown.key = 'BrowserBack';
          window.dispatchEvent(keyDown);
        }
      },
    ]
  };
  var emptyCallParam = {
  header: { l10nId:'EmptyCall' },
    items: [
      {
        name: '',
        l10nId: '',
        priority: 1,
        method: function() {
        }
      }
    ]
  };
  OptionHelper.optionParams["call-info"] = callInfoParam;
  OptionHelper.optionParams["edit-select"] = editParamSelect;
  OptionHelper.optionParams["edit-no-select-focus"] = editParamNoSelectFocus;
  OptionHelper.optionParams['edit-select-none'] = editParamSelectNone;
  OptionHelper.optionParams["edit-deselect"] = editParamDeselect;
  OptionHelper.optionParams["confirmation"] = confirmation;
  OptionHelper.optionParams["empty-call"] = emptyCallParam;
})(window);
