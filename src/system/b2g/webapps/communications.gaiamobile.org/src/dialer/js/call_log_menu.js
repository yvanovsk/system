'use strict';
(function(exports) {
  //console.log("call log menu");

  LazyLoader.load([
    'js/selected_item_menu_params.js',
    'js/group_menu_params.js'
  ], function loaded() {
    document.addEventListener("focusChanged", function(e) {
      if (OptionHelper.option.menuVisible) {
        return;
      }
      if (e.detail != undefined) {
        if (e.detail.focusedElement == undefined) {
          delete OptionHelper.option.phoneNumber;
          delete OptionHelper.option.date;
          delete OptionHelper.option.type;
          delete OptionHelper.option.status;
          delete OptionHelper.option.id;
          return;
        } else {
          if (OptionHelper.option.header &&
              OptionHelper.option.header.l10nId != 'editSelect' &&
              OptionHelper.option.header.l10nId != 'editDeselect' &&
              !OptionHelper.option.menuVisible &&
              !OptionHelper.CallInfoVisible) {
            OptionHelper.show(NavigationMap.latestClassName);

          }
        }
      }
      if (e.detail.focusedElement.classList.contains('log-item')) {
        OptionHelper.option.phoneNumber = e.detail.focusedElement.dataset.phoneNumber;
        OptionHelper.option.date = e.detail.focusedElement.dataset.timestamp;
        OptionHelper.option.type = e.detail.focusedElement.dataset.type;
        OptionHelper.option.status = e.detail.focusedElement.dataset.status;
        OptionHelper.option.id = e.detail.focusedElement.id;
      }
    });
  });
})(window);
