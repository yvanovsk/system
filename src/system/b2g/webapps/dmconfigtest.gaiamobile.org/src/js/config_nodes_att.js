'use strict';

var omaNodes_ATT =(function(){
	
	var NODE_IDS = [];
	var SERVER_IDS = [];
	//"./DevInfo/Ext"
	NODE_IDS =[
		{name:"AppID", value:1},
		{name:"ServerID", value:2},
		{name:"Name", value:3},
		{name:"Addr", value:4},
		{name:"PortNbr", value:5},
		{name:"clientAAuthLevel", value:6},
		{name:"clientAAuthType", value:7},
		{name:"clientAAuthName", value:8},
		{name:"clientAAuthSecret", value:9},
		{name:"clientAAuthData", value:10},
		{name:"serverAAuthLevel", value:11},
		{name:"serverAAuthType", value:12},
		{name:"serverAAuthName", value:13},
		{name:"serverAAuthSecret", value:14},
		{name:"serverAAuthData", value:15},
		{name:"TransportType", value:17}
	];
	
	SERVER_IDS =[
		{name:"master", value:1},
		{name:"Cingular", value:2},
		{name:"ATTLabA", value:3},
		{name:"mform", value:4},
		{name:"scat", value:5},
		{name:"bls", value:6}
	];

	function getNodeIds() {
		return NODE_IDS;
	};

	function getServerIds() {
		return SERVER_IDS;
	};
	
	return {
		getNodeIds: getNodeIds,
		getServerIds: getServerIds
	};

})();


